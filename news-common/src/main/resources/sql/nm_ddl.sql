DROP TABLE "NMTEST"."AUTHOR" CASCADE CONSTRAINTS;
DROP TABLE "NMTEST"."COMMENTS" CASCADE CONSTRAINTS;
DROP TABLE "NMTEST"."NEWS" CASCADE CONSTRAINTS;
DROP TABLE "NMTEST"."NEWS_AUTHOR" CASCADE CONSTRAINTS;
DROP TABLE "NMTEST"."NEWS_TAG" CASCADE CONSTRAINTS;
DROP TABLE "NMTEST"."TAG" CASCADE CONSTRAINTS;
DROP SEQUENCE "NMTEST"."AUTHOR_ID_SEQ";
DROP SEQUENCE "NMTEST"."COMMENT_ID_SEQ";
DROP SEQUENCE "NMTEST"."NEWS_AUTHOR_ID_SEQ";
DROP SEQUENCE "NMTEST"."NEWS_ID_SEQ";
DROP SEQUENCE "NMTEST"."NEWS_TAG_ID_SEQ";
DROP SEQUENCE "NMTEST"."TAG_ID_SEQ";
--------------------------------------------------------
--  DDL for Sequence AUTHOR_ID_SEQ
--------------------------------------------------------

CREATE SEQUENCE "NMTEST"."AUTHOR_ID_SEQ" MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER NOCYCLE;
--------------------------------------------------------
--  DDL for Sequence COMMENT_ID_SEQ
--------------------------------------------------------

CREATE SEQUENCE "NMTEST"."COMMENT_ID_SEQ" MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER NOCYCLE;
--------------------------------------------------------
--  DDL for Sequence NEWS_AUTHOR_ID_SEQ
--------------------------------------------------------

CREATE SEQUENCE "NMTEST"."NEWS_AUTHOR_ID_SEQ" MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER NOCYCLE;
--------------------------------------------------------
--  DDL for Sequence NEWS_ID_SEQ
--------------------------------------------------------

CREATE SEQUENCE "NMTEST"."NEWS_ID_SEQ" MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER NOCYCLE;
--------------------------------------------------------
--  DDL for Sequence NEWS_TAG_ID_SEQ
--------------------------------------------------------

CREATE SEQUENCE "NMTEST"."NEWS_TAG_ID_SEQ" MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER NOCYCLE;
--------------------------------------------------------
--  DDL for Sequence TAG_ID_SEQ
--------------------------------------------------------

CREATE SEQUENCE "NMTEST"."TAG_ID_SEQ" MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER NOCYCLE;
--------------------------------------------------------
--  DDL for Table AUTHOR
--------------------------------------------------------

CREATE TABLE "NMTEST"."AUTHOR"
(
  "AUTHOR_ID" NUMBER(20, 0),
  "NAME"            VARCHAR2(30 BYTE),
  "EXPIRATION_DATE" DATE
)
TABLESPACE "TBM_NM_ADMIN";
--------------------------------------------------------
--  DDL for Table COMMENTS
--------------------------------------------------------

CREATE TABLE "NMTEST"."COMMENTS"
(
  "COMMENT_ID"    NUMBER(20, 0),
  "COMMENT_TEXT"  VARCHAR2(100 BYTE),
  "CREATION_DATE" TIMESTAMP(6),
  "NEWS_ID"       NUMBER(20, 0)
)
TABLESPACE "TBM_NM_ADMIN";
--------------------------------------------------------
--  DDL for Table NEWS
--------------------------------------------------------

CREATE TABLE "NMTEST"."NEWS"
(
  "NEWS_ID"           NUMBER(20, 0),
  "SHORT_TEXT"        VARCHAR2(100 BYTE),
  "FULL_TEXT"         VARCHAR2(2000 BYTE),
  "TITLE"             VARCHAR2(30 BYTE),
  "CREATION_DATE"     TIMESTAMP(6),
  "MODIFICATION_DATE" DATE
  "VERSION"           NUMBER(20, 0)		
)
TABLESPACE "TBM_NM_ADMIN";
--------------------------------------------------------
--  DDL for Table NEWS_AUTHOR
--------------------------------------------------------

CREATE TABLE "NMTEST"."NEWS_AUTHOR"
(
  "NEWS_AUTHOR_ID" NUMBER(20, 0),
  "NEWS_ID"        NUMBER(20, 0),
  "AUTHOR_ID"      NUMBER(20, 0)
)
TABLESPACE "TBM_NM_ADMIN";
--------------------------------------------------------
--  DDL for Table NEWS_TAG
--------------------------------------------------------

CREATE TABLE "NMTEST"."NEWS_TAG"
(
  "NEWS_TAG_ID" NUMBER(20, 0),
  "NEWS_ID"     NUMBER(20, 0),
  "TAG_ID"      NUMBER(20, 0)
)
TABLESPACE "TBM_NM_ADMIN";
--------------------------------------------------------
--  DDL for Table TAG
--------------------------------------------------------

CREATE TABLE "NMTEST"."TAG"
(
  "TAG_ID"   NUMBER(20, 0),
  "TAG_NAME" VARCHAR2(30 BYTE)
)
TABLESPACE "TBM_NM_ADMIN";
--------------------------------------------------------
--  DDL for Index NEWS_AUTHOR_PK
--------------------------------------------------------

CREATE UNIQUE INDEX "NMTEST"."NEWS_AUTHOR_PK" ON "NMTEST"."NEWS_AUTHOR" ("NEWS_AUTHOR_ID")
TABLESPACE "TBM_NM_ADMIN";
--------------------------------------------------------
--  DDL for Index TAG_PK
--------------------------------------------------------

CREATE UNIQUE INDEX "NMTEST"."TAG_PK" ON "NMTEST"."TAG" ("TAG_ID")
TABLESPACE "TBM_NM_ADMIN";
--------------------------------------------------------
--  DDL for Index COMMENTS_PK
--------------------------------------------------------

CREATE UNIQUE INDEX "NMTEST"."COMMENTS_PK" ON "NMTEST"."COMMENTS" ("COMMENT_ID")
TABLESPACE "TBM_NM_ADMIN";
--------------------------------------------------------
--  DDL for Index AUTHOR_PK
--------------------------------------------------------

CREATE UNIQUE INDEX "NMTEST"."AUTHOR_PK" ON "NMTEST"."AUTHOR" ("AUTHOR_ID")
TABLESPACE "TBM_NM_ADMIN";
--------------------------------------------------------
--  DDL for Index NEWS_TAG_PK
--------------------------------------------------------

CREATE UNIQUE INDEX "NMTEST"."NEWS_TAG_PK" ON "NMTEST"."NEWS_TAG" ("NEWS_TAG_ID")
TABLESPACE "TBM_NM_ADMIN";
--------------------------------------------------------
--  DDL for Index NEWS_PK
--------------------------------------------------------

CREATE UNIQUE INDEX "NMTEST"."NEWS_PK" ON "NMTEST"."NEWS" ("NEWS_ID")
TABLESPACE "TBM_NM_ADMIN";
--------------------------------------------------------
--  Constraints for Table TAG
--------------------------------------------------------

ALTER TABLE "NMTEST"."TAG" ADD CONSTRAINT "TAG_PK" PRIMARY KEY ("TAG_ID");
ALTER TABLE "NMTEST"."TAG" MODIFY ("TAG_NAME" NOT NULL ENABLE);
ALTER TABLE "NMTEST"."TAG" MODIFY ("TAG_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table AUTHOR
--------------------------------------------------------

ALTER TABLE "NMTEST"."AUTHOR" ADD CONSTRAINT "AUTHOR_PK" PRIMARY KEY ("AUTHOR_ID");
ALTER TABLE "NMTEST"."AUTHOR" MODIFY ("NAME" NOT NULL ENABLE);
ALTER TABLE "NMTEST"."AUTHOR" MODIFY ("AUTHOR_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table NEWS
--------------------------------------------------------

ALTER TABLE "NMTEST"."NEWS" ADD CONSTRAINT "NEWS_PK" PRIMARY KEY ("NEWS_ID");
ALTER TABLE "NMTEST"."NEWS" MODIFY ("MODIFICATION_DATE" NOT NULL ENABLE);
ALTER TABLE "NMTEST"."NEWS" MODIFY ("CREATION_DATE" NOT NULL ENABLE);
ALTER TABLE "NMTEST"."NEWS" MODIFY ("TITLE" NOT NULL ENABLE);
ALTER TABLE "NMTEST"."NEWS" MODIFY ("FULL_TEXT" NOT NULL ENABLE);
ALTER TABLE "NMTEST"."NEWS" MODIFY ("SHORT_TEXT" NOT NULL ENABLE);
ALTER TABLE "NMTEST"."NEWS" MODIFY ("NEWS_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table COMMENTS
--------------------------------------------------------

ALTER TABLE "NMTEST"."COMMENTS" ADD CONSTRAINT "COMMENTS_PK" PRIMARY KEY ("COMMENT_ID");
ALTER TABLE "NMTEST"."COMMENTS" MODIFY ("CREATION_DATE" NOT NULL ENABLE);
ALTER TABLE "NMTEST"."COMMENTS" MODIFY ("COMMENT_TEXT" NOT NULL ENABLE);
ALTER TABLE "NMTEST"."COMMENTS" MODIFY ("COMMENT_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table NEWS_AUTHOR
--------------------------------------------------------

ALTER TABLE "NMTEST"."NEWS_AUTHOR" ADD CONSTRAINT "NEWS_AUTHOR_PK" PRIMARY KEY ("NEWS_AUTHOR_ID");
ALTER TABLE "NMTEST"."NEWS_AUTHOR" MODIFY ("AUTHOR_ID" NOT NULL ENABLE);
ALTER TABLE "NMTEST"."NEWS_AUTHOR" MODIFY ("NEWS_ID" NOT NULL ENABLE);
ALTER TABLE "NMTEST"."NEWS_AUTHOR" MODIFY ("NEWS_AUTHOR_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table NEWS_TAG
--------------------------------------------------------

ALTER TABLE "NMTEST"."NEWS_TAG" ADD CONSTRAINT "NEWS_TAG_PK" PRIMARY KEY ("NEWS_TAG_ID");
ALTER TABLE "NMTEST"."NEWS_TAG" MODIFY ("TAG_ID" NOT NULL ENABLE);
ALTER TABLE "NMTEST"."NEWS_TAG" MODIFY ("NEWS_ID" NOT NULL ENABLE);
ALTER TABLE "NMTEST"."NEWS_TAG" MODIFY ("NEWS_TAG_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Ref Constraints for Table COMMENTS
--------------------------------------------------------

ALTER TABLE "NMTEST"."COMMENTS" ADD CONSTRAINT "FK_NEWS_COMMENTS" FOREIGN KEY ("NEWS_ID")
REFERENCES "NMTEST"."NEWS" ("NEWS_ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table NEWS_AUTHOR
--------------------------------------------------------

ALTER TABLE "NMTEST"."NEWS_AUTHOR" ADD CONSTRAINT "FK_AUTHOR_NEWS_AUTHOR" FOREIGN KEY ("AUTHOR_ID")
REFERENCES "NMTEST"."AUTHOR" ("AUTHOR_ID") ENABLE;
ALTER TABLE "NMTEST"."NEWS_AUTHOR" ADD CONSTRAINT "FK_NEWS_NEWS_AUTHOR" FOREIGN KEY ("NEWS_ID")
REFERENCES "NMTEST"."NEWS" ("NEWS_ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table NEWS_TAG
--------------------------------------------------------

ALTER TABLE "NMTEST"."NEWS_TAG" ADD CONSTRAINT "FK_NEWS_NEWS_TAG" FOREIGN KEY ("NEWS_ID")
REFERENCES "NMTEST"."NEWS" ("NEWS_ID") ENABLE;
ALTER TABLE "NMTEST"."NEWS_TAG" ADD CONSTRAINT "FK_TAG_NEWS_TAG" FOREIGN KEY ("TAG_ID")
REFERENCES "NMTEST"."TAG" ("TAG_ID") ENABLE;
--------------------------------------------------------
--  DDL for Trigger TRG_NEWS_AUTHOR_ID
--------------------------------------------------------

CREATE OR REPLACE TRIGGER "NMTEST"."TRG_NEWS_AUTHOR_ID"
BEFORE INSERT ON news_author
FOR EACH ROW
  BEGIN
    SELECT news_author_id_seq.nextval
    INTO :new.news_author_id
    FROM dual;
  END;
/
ALTER TRIGGER "NMTEST"."TRG_NEWS_AUTHOR_ID" ENABLE;

--------------------------------------------------------
--  DDL for Trigger TRG_NEWS_TAG_ID
--------------------------------------------------------

CREATE OR REPLACE TRIGGER "NMTEST"."TRG_NEWS_TAG_ID"
BEFORE INSERT ON news_tag
FOR EACH ROW
  BEGIN
    SELECT news_tag_id_seq.nextval
    INTO :new.news_tag_id
    FROM dual;
  END;
/
ALTER TRIGGER "NMTEST"."TRG_NEWS_TAG_ID" ENABLE;

--------------------------------------------------------
-- test purposes
--------------------------------------------------------

CREATE OR REPLACE TRIGGER TRG_AUTHOR_ID
BEFORE INSERT ON author
FOR EACH ROW
  BEGIN
    IF :new.author_id IS NULL
    THEN
      SELECT author_id_seq.nextval
      INTO :new.author_id
      FROM dual;
    END IF;
  END;
/