package com.epam.newsmanagement.dao;


import com.epam.newsmanagement.model.Author;
import com.epam.newsmanagement.model.News;

import java.util.List;

/**
 * IAuthorDAO provides DAO interface for database AUTHOR table.
 *
 * @author Siarhei_Yeharetski
 */
public interface AuthorDAO {
    /**
     * Get all Authors from the database.
     *
     * @return list of all authors.
     */
    List<Author> findAll();

    /**
     * Get Author by id from the database.
     *
     * @param id the id of Author.
     * @return Authors from the database.
     */
    Author findById(long id);

    /**
     * Adds Author to the database.
     *
     * @param author the Author.
     * @return id of the added Author.
     */
    long create(Author author);

    /**
     * Deletes Author by id.
     *
     * @param id the Author id to be deleted.
     * @return true, if successfully deleted.
     */
    boolean delete(long id);

    /**
     * Updates Author.
     *
     * @param author the Author to be updated.
     * @return true, if successfully updated.
     */
    boolean update(Author author);

    /**
     * Adds news author link.
     *
     * @param news   the News VO.
     * @param author the Author VO.
     * @return affected row id.
     */
    long createNewsAuthor(News news, Author author);
    
    /**
     * 
     * @param id
     * @return
     */
    boolean deleteNewsAuthor(long id);
    
    /**
     * Find Author by News id
     * 
     * @param id
     * @return Requested author
     */
    Author findByNewsId(long id);
    
    /**
     * Expires Author
     * 
     * @param id
     * @return true, if successfully expired.
     */
    boolean expire(long id);
    
    /**
     * Selects all not expired Authors from database
     * 
     * @return Authors collection.
     */
    List<Author> findAllNotExpired();
}
