package com.epam.newsmanagement.dao.jpa;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.UserDAO;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.model.Roles;
import com.epam.newsmanagement.model.User;

public class JpaUserDAO implements UserDAO {

	private static final String SELECT_USER_ROLES = "SELECT r FROM Roles r WHERE r.userId IN (SELECT u.id FROM User u WHERE u.login = :login)";

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	@Transactional
	public User userIsExist(String login) {
		try {
			Query query = entityManager.createNamedQuery("User.findByLogin");
			query.setParameter("login", login);
			if (query.getResultList().isEmpty()) {
				return null;
			} else {
				return (User) query.getResultList().get(0);
			}
		} catch (PersistenceException e) {
			throw new DAOException(
					"Something went wrong in database layer when finding User by username.",
					e);
		}
	}

	@Override
	@Transactional
	public long addUser(User user) {
		try {
			entityManager.persist(user);
			entityManager.flush();
			return user.getId();
		} catch (PersistenceException e) {
			throw new DAOException(
					"Something went wrong in database layer when creating new User.",
					e);
		}
	}

	@Override
	@Transactional
	public boolean deleteUser(long id) {
		try {
			User user = entityManager.find(User.class, id);
			entityManager.remove(user);
			return true;
		} catch (PersistenceException e) {
			throw new DAOException(
					"Something went wrong in database layer when deleting User.",
					e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<String> getRolesOfUser(String login) {
		try {
			List<String> userRoles = new ArrayList<String>();
			Query query = entityManager.createQuery(SELECT_USER_ROLES,
					Roles.class);
			query.setParameter("login", login);
			List<Roles> rolesList = query.getResultList();
			for (Roles role : rolesList) {
				userRoles.add(role.getRoleName());
			}
			return userRoles;
		} catch (PersistenceException e) {
			throw new DAOException(
					"Something went wrong in database layer when finding User roles.",
					e);
		}
	}

}
