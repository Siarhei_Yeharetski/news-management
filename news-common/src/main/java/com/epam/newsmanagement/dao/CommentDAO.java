package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.model.Comment;
import com.epam.newsmanagement.model.News;

import java.util.List;

/**
 * ICommentDAO provides DAO interface for database COMMENT table.
 *
 * @author Siarhei_Yeharetski
 */
public interface CommentDAO {
    /**
     * Get all Comments from the database.
     *
     * @return list of all comments.
     */
    List<Comment> findAll();

    /**
     * Get Comment by id from the database.
     *
     * @param id the id of Comment.
     * @return Comment from the database.
     */
    Comment findById(long id);

    /**
     * Adds Comment to the database
     *
     * @param comment the Comment.
     * @return id of the added Comment.
     */
    long create(Comment comment);

    /**
     * Adds Comment for News message to the database.
     *
     * @param news    the News VO.
     * @param comment the Comment.
     * @return id of the added Comment.
     */
    long create(News news, Comment comment);

    /**
     * Deletes Comment by id.
     *
     * @param id the Comment id to be deleted.
     * @return true, if successfully deleted.
     */
    boolean delete(long id);

    /**
     * Updates Comment.
     *
     * @param comment the Comment to be updated.
     * @return true, if successfully updated.
     */
    boolean update(Comment comment);
    
    /**
     * Find all Comments by news id.
     * 
     * @param id
     * @return Comments collection.
     */
    List<Comment> findByNewsId(long id);
}
