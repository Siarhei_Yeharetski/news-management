package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.model.User;

public interface UserDAO {
	
	/**
	 * Checks if User is presented in DB.
	 * 
	 * @param login
	 * @return User, if found.
	 */
    User userIsExist(String login);
    
    /**
     * Add User to DB.
     * 
     * @param user
     * @return user id.
     */
    long addUser(User user);
    
    /**
     * Delete user by its id.
     * 
     * @param id
     * @return true, if successfully deleted.
     */
    boolean deleteUser(long id);
    
    /**
     * Get all roles granted to User.
     * 
     * @param login
     * @return String collection of Roles.
     */
    List<String> getRolesOfUser(String login);
}
