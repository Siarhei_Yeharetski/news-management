package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.model.News;
import com.epam.newsmanagement.model.Tag;

import java.util.List;

/**
 * ITagDAO provides DAO interface for database TAG table.
 *
 * @author Siarhei_Yeharetski
 */
public interface TagDAO {
    /**
     * Get all Tags from the database.
     *
     * @return list of all tags.
     */
    List<Tag> findAll();

    /**
     * Get Tag by id from the database.
     *
     * @param id the id of Tag.
     * @return Tag from the database.
     */
    Tag findById(long id);

    /**
     * Adds Tag to the database.
     *
     * @param tag the Tag.
     * @return id of the added Tag.
     */
    long create(Tag tag);

    /**
     * Deletes Tag by id.
     *
     * @param id the Tag id to be deleted.
     * @return true, if successfully deleted.
     */
    boolean delete(long id);

    /**
     * Updates Tag.
     *
     * @param tag the Tag to be updated.
     * @return true, if successfully updated.
     */
    boolean update(Tag tag);

    /**
     * Adds news tag link.
     *
     * @param news the News VO.
     * @param tag  the Tag VO.
     * @return affected row id.
     */
    long createNewsTag(News news, Tag tag);
    
    /**
     * 
     * @param id
     * @return
     */
    boolean deleteNewsTag(long id);
    
    /**
     * Find all Tags by News id.
     * 
     * @param id
     * @return Tag collection.
     */
    List<Tag> findByNewsId(long id);
}
