package com.epam.newsmanagement.dao.jdbc;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.model.Comment;
import com.epam.newsmanagement.model.News;
import com.epam.newsmanagement.utils.DateUtils;
import com.epam.newsmanagement.utils.DbUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

/**
 * CommentDAOImpl class implements ICommentDAO interface and provides access to
 * COMMENT database entity.
 *
 * @author Siarhei_Yeharetski
 */
@Repository
public class JdbcCommentDAO implements CommentDAO {

	private static final String COMMENT_ID_COLUMN = "COMMENT_ID";
	private static final String COMMENT_TEXT_COLUMN = "COMMENT_TEXT";
	private static final String COMMENT_CREATION_DATE_COLUMN = "CREATION_DATE";
	private static final String COMMENT_NEWS_ID_COLUMN = "NEWS_ID";

	private static final String SELECT_ALL = "SELECT COMMENT_ID, COMMENT_TEXT, CREATION_DATE, NEWS_ID FROM COMMENTS";
	private static final String INSERT = "INSERT INTO COMMENTS(COMMENT_ID, COMMENT_TEXT, CREATION_DATE) VALUES (?, ?, ?)";
	private static final String INSERT_NO_ID = "INSERT INTO COMMENTS(COMMENT_ID, COMMENT_TEXT, CREATION_DATE) VALUES (comment_id_seq.NEXTVAL, ?, ?)";
	private static final String FULL_INSERT = "INSERT INTO COMMENTS(COMMENT_ID, COMMENT_TEXT, CREATION_DATE, NEWS_ID) VALUES (?, ?, ?, ?)";
	private static final String FULL_INSERT_NO_ID = "INSERT INTO COMMENTS(COMMENT_ID, COMMENT_TEXT, CREATION_DATE, NEWS_ID) VALUES (comment_id_seq.NEXTVAL, ?, ?, ?)";
	private static final String FIND_BY_ID = "SELECT COMMENT_ID, COMMENT_TEXT, CREATION_DATE, NEWS_ID FROM COMMENTS WHERE COMMENT_ID = ?";
	private static final String FIND_BY_NEWS_ID = "SELECT COMMENT_ID, COMMENT_TEXT, CREATION_DATE, NEWS_ID FROM COMMENTS WHERE NEWS_ID = ? ORDER BY CREATION_DATE desc";
	private static final String DELETE_BY_ID = "DELETE FROM COMMENTS WHERE COMMENT_ID = ?";
	private static final String UPDATE = "UPDATE COMMENTS SET COMMENT_TEXT = ?, CREATION_DATE = ?, NEWS_ID = ? WHERE COMMENT_ID = ?";

	@Autowired
	private DataSource dataSource;

	public JdbcCommentDAO() {
	}

	@Override
	public List<Comment> findAll() {
		List<Comment> commentList = new ArrayList<>();
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			statement = connection.createStatement();
			resultSet = statement.executeQuery(SELECT_ALL);

			while (resultSet.next()) {
				Comment comment = new Comment();
				comment.setId(resultSet.getLong(COMMENT_ID_COLUMN));
				comment.setCommentText(resultSet.getString(COMMENT_TEXT_COLUMN));
				comment.setCreationDate(resultSet
						.getTimestamp(COMMENT_CREATION_DATE_COLUMN));
				comment.setNewsId(resultSet.getLong(COMMENT_NEWS_ID_COLUMN));
				commentList.add(comment);
			}
		} catch (SQLException e) {
			throw new DAOException(
					"Something went wrong in database layer when finding all Comments.",
					e);
		} finally {
			DbUtils.close(resultSet, statement, connection);
		}
		return commentList;
	}

	@Override
	public Comment findById(long id) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(FIND_BY_ID);

			preparedStatement.setLong(1, id);

			resultSet = preparedStatement.executeQuery();

			if (resultSet.isBeforeFirst()) {
				Comment comment = new Comment();

				while (resultSet.next()) {
					comment.setId(resultSet.getLong(COMMENT_ID_COLUMN));
					comment.setCommentText(resultSet
							.getString(COMMENT_TEXT_COLUMN));
					comment.setCreationDate(resultSet
							.getTimestamp(COMMENT_CREATION_DATE_COLUMN));
					comment.setNewsId(resultSet.getLong(COMMENT_NEWS_ID_COLUMN));
				}

				return comment;
			} else {
				return null;
			}
		} catch (SQLException e) {
			throw new DAOException(
					"Something went wrong in database layer when finding Comment by id.",
					e);
		} finally {
			DbUtils.close(resultSet, preparedStatement, connection);
		}
	}

	@Override
	public long create(Comment comment) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);

			if (comment.getId() == null) {
				preparedStatement = connection.prepareStatement(INSERT_NO_ID,
						new String[] { COMMENT_ID_COLUMN });

				preparedStatement.setString(1, comment.getCommentText());
				preparedStatement.setTimestamp(2, DateUtils
						.convertUtilDateToSqlTimestamp(comment
								.getCreationDate()));
			} else {
				preparedStatement = connection.prepareStatement(INSERT,
						new String[] { COMMENT_ID_COLUMN });

				preparedStatement.setLong(1, comment.getId());
				preparedStatement.setString(2, comment.getCommentText());
				preparedStatement.setTimestamp(3, DateUtils
						.convertUtilDateToSqlTimestamp(comment
								.getCreationDate()));
			}

			preparedStatement.executeUpdate();

			resultSet = preparedStatement.getGeneratedKeys();

			if (resultSet.next()) {
				return resultSet.getLong(1);
			} else {
				throw new DAOException(
						"Could not retrieve COMMENT_ID value from DB.");
			}
		} catch (SQLException e) {
			throw new DAOException(
					"Something went wrong in database layer when creating new Comment.",
					e);
		} finally {
			DbUtils.close(preparedStatement, connection);
		}
	}

	@Override
	public long create(News news, Comment comment) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);

			if (comment.getId() == null) {
				preparedStatement = connection.prepareStatement(
						FULL_INSERT_NO_ID, new String[] { COMMENT_ID_COLUMN });

				preparedStatement.setString(1, comment.getCommentText());
				preparedStatement.setTimestamp(2, DateUtils
						.convertUtilDateToSqlTimestamp(comment
								.getCreationDate()));
				preparedStatement.setLong(3, news.getId());
			} else {
				preparedStatement = connection.prepareStatement(FULL_INSERT,
						new String[] { COMMENT_ID_COLUMN });

				preparedStatement.setLong(1, comment.getId());
				preparedStatement.setString(2, comment.getCommentText());
				preparedStatement.setTimestamp(3, DateUtils
						.convertUtilDateToSqlTimestamp(comment
								.getCreationDate()));
				preparedStatement.setLong(4, news.getId());
			}

			preparedStatement.executeUpdate();

			resultSet = preparedStatement.getGeneratedKeys();

			if (resultSet.next()) {
				return resultSet.getLong(1);
			} else {
				throw new DAOException(
						"Could not retrieve COMMENT_ID value from DB.");
			}
		} catch (SQLException e) {
			throw new DAOException(
					"Something went wrong in database layer when creating new Comment.",
					e);
		} finally {
			DbUtils.close(preparedStatement, connection);
		}
	}

	@Override
	public boolean delete(long id) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(DELETE_BY_ID);

			preparedStatement.setLong(1, id);

			return preparedStatement.executeUpdate() > 0;
		} catch (SQLException e) {
			throw new DAOException(
					"Something went wrong in database layer when deleting Comment.",
					e);
		} finally {
			DbUtils.close(preparedStatement, connection);
		}
	}

	@Override
	public boolean update(Comment comment) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(UPDATE);

			preparedStatement.setLong(4, comment.getId());
			preparedStatement.setString(1, comment.getCommentText());
			preparedStatement.setTimestamp(2, DateUtils
					.convertUtilDateToSqlTimestamp(comment.getCreationDate()));
			preparedStatement.setLong(3, comment.getNewsId());

			return preparedStatement.executeUpdate() > 0;
		} catch (SQLException e) {
			throw new DAOException(
					"Something went wrong in database layer when updating Comment.",
					e);
		} finally {
			DbUtils.close(preparedStatement, connection);
		}
	}

	@Override
	public List<Comment> findByNewsId(long id) {
		List<Comment> commentList = new ArrayList<>();
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(FIND_BY_NEWS_ID);

			preparedStatement.setLong(1, id);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				Comment comment = new Comment();
				comment.setId(resultSet.getLong(COMMENT_ID_COLUMN));
				comment.setCommentText(resultSet.getString(COMMENT_TEXT_COLUMN));
				comment.setCreationDate(resultSet
						.getTimestamp(COMMENT_CREATION_DATE_COLUMN));
				comment.setNewsId(resultSet.getLong(COMMENT_NEWS_ID_COLUMN));
				commentList.add(comment);
			}
		} catch (SQLException e) {
			throw new DAOException(
					"Something went wrong in database layer when finding Comment by News id.",
					e);
		} finally {
			DbUtils.close(resultSet, preparedStatement, connection);
		}
		return commentList;
	}
}
