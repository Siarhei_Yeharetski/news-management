package com.epam.newsmanagement.dao.jpa;

import java.util.Calendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.model.Author;
import com.epam.newsmanagement.model.News;
import com.epam.newsmanagement.model.NewsAuthor;

public class JpaAuthorDAO implements AuthorDAO {

	private static final String FIND_BY_NEWS_ID = "SELECT a FROM Author a WHERE a.id IN (SELECT n.authorId FROM NewsAuthor n WHERE n.newsId = :newsId)";
	private static final String SELECT_NOT_EXPIRED = "SELECT a FROM Author a WHERE a.expirationDate IS NOT NULL";

	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<Author> findAll() {
		try {
			Query query = entityManager.createNamedQuery("Author.findAll");
			return query.getResultList();
		} catch (PersistenceException e) {
			throw new DAOException(
					"Something went wrong in database layer when finding all Authors.",
					e);
		}
	}

	@Override
	@Transactional
	public Author findById(long id) {
		try {
			return entityManager.find(Author.class, id);
		} catch (PersistenceException e) {
			throw new DAOException(
					"Something went wrong in database layer when finding Author by id.",
					e);
		}
	}

	@Override
	@Transactional
	public long create(Author author) {
		try {
			entityManager.persist(author);
			entityManager.flush();
			return author.getId();
		} catch (PersistenceException e) {
			throw new DAOException(
					"Something went wrong in database layer when creating new Author.",
					e);
		}
	}

	@Override
	@Transactional
	public boolean delete(long id) {
		try {
			Author author = findById(id);
			entityManager.remove(author);
			return true;
		} catch (PersistenceException e) {
			throw new DAOException(
					"Something went wrong in database layer when deleting Author.",
					e);
		}
	}

	@Override
	@Transactional
	public boolean update(Author author) {
		try {
			entityManager.merge(author);
			return true;
		} catch (PersistenceException e) {
			throw new DAOException(
					"Something went wrong in database layer when updating Author.",
					e);
		}
	}

	@Override
	@Transactional
	public long createNewsAuthor(News news, Author author) {
		try {
			NewsAuthor newsAuthor = new NewsAuthor();
			newsAuthor.setNewsId(news.getId());
			newsAuthor.setAuthorId(author.getId());
			entityManager.persist(newsAuthor);
			entityManager.flush();
			return newsAuthor.getId();
		} catch (PersistenceException e) {
			throw new DAOException(
					"Something went wrong in database layer when creating new NewsAuthor.",
					e);
		}
	}

	@Override
	@Transactional
	public Author findByNewsId(long id) {
		try {
			Query query = entityManager.createQuery(FIND_BY_NEWS_ID,
					Author.class);
			query.setParameter("newsId", id);
			@SuppressWarnings("unchecked")
			List<Author> authors = query.getResultList();
			if (authors.size() > 0) {
				return (Author) query.getResultList().get(0);
			} else {
				return null;
			}
		} catch (PersistenceException e) {
			throw new DAOException(
					"Something went wrong in database layer when finding Author by News id.",
					e);
		}
	}

	@Override
	@Transactional
	public boolean expire(long id) {
		try {
			Author author = findById(id);
			author.setExpirationDate(Calendar.getInstance().getTime());
			update(author);
			return true;
		} catch (PersistenceException e) {
			throw new DAOException(
					"Something went wrong in database layer when expiring Author.",
					e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<Author> findAllNotExpired() {
		try {
			Query query = entityManager.createQuery(SELECT_NOT_EXPIRED,
					Author.class);
			return query.getResultList();
		} catch (PersistenceException e) {
			throw new DAOException(
					"Something went wrong in database layer when finding all not expired Authors.",
					e);
		}
	}

	@Override
	public boolean deleteNewsAuthor(long id) {
		// TODO Auto-generated method stub
		return false;
	}

}
