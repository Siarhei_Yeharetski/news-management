package com.epam.newsmanagement.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsmanagement.dao.UserDAO;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.model.User;
import com.epam.newsmanagement.utils.DbUtils;

public class JdbcUserDAO implements UserDAO {

	private static final String USER_ID_COLUMN = "USER_ID";
	private static final String USER_NAME_COLUMN = "USER_NAME";
	private static final String USER_LOGIN_COLUMN = "LOGIN";
	private static final String USER_PASSWORD_COLUMN = "PASSWORD";

	private static final String SELECT_BY_LOGIN = "SELECT USER_ID, USER_NAME, LOGIN, PASSWORD FROM USERTABLE WHERE LOGIN = ?";
	private static final String INSERT = "INSERT INTO USERTABLE(USER_ID, USER_NAME, LOGIN, PASSWORD) VALUES (?, ?, ?, ?)";
	private static final String INSERT_NO_ID = "INSERT INTO USERTABLE(USER_ID, USER_NAME, LOGIN, PASSWORD) VALUES (user_id_seq.NEXTVAL, ?, ?, ?)";
	private static final String DELETE_BY_ID = "DELETE FROM USERTABLE WHERE USER_ID = ?";
	private static final String SELECT_USER_ROLES = "SELECT ROLE_NAME FROM ROLES WHERE USER_ID IN (SELECT USER_ID FROM USERTABLE WHERE LOGIN = ?)";

	@Autowired
	private DataSource dataSource;

	public JdbcUserDAO() {
	}

	@Override
	public User userIsExist(String login) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SELECT_BY_LOGIN);

			preparedStatement.setString(1, login);

			resultSet = preparedStatement.executeQuery();

			if (resultSet.isBeforeFirst()) {
				User user = new User();

				while (resultSet.next()) {
					user.setId(resultSet.getLong(USER_ID_COLUMN));
					user.setUsername(resultSet.getString(USER_NAME_COLUMN));
					user.setLogin(resultSet.getString(USER_LOGIN_COLUMN));
					user.setPassword(resultSet.getString(USER_PASSWORD_COLUMN));
				}

				return user;
			} else {
				return null;
			}
		} catch (SQLException e) {
			throw new DAOException(
					"Something went wrong in database layer when finding User by username.",
					e);
		} finally {
			DbUtils.close(resultSet, preparedStatement, connection);
		}
	}

	@Override
	public long addUser(User user) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);

			if (user.getId() == null) {
				preparedStatement = connection.prepareStatement(INSERT_NO_ID,
						new String[] { "USER_ID" });

				preparedStatement.setString(1, user.getUsername());
				preparedStatement.setString(2, user.getLogin());
				preparedStatement.setString(3, user.getPassword());
			} else {
				preparedStatement = connection.prepareStatement(INSERT,
						new String[] { "USER_ID" });

				preparedStatement.setLong(1, user.getId());
				preparedStatement.setString(2, user.getUsername());
				preparedStatement.setString(3, user.getLogin());
				preparedStatement.setString(4, user.getPassword());
			}

			preparedStatement.executeUpdate();

			resultSet = preparedStatement.getGeneratedKeys();

			if (resultSet.next()) {
				return resultSet.getLong(1);
			} else {
				throw new DAOException(
						"Could not retrieve TAG_ID value from DB.");
			}
		} catch (SQLException e) {
			throw new DAOException(
					"Something went wrong in database layer when creating new User.",
					e);
		} finally {
			DbUtils.close(preparedStatement, connection);
		}
	}

	@Override
	public boolean deleteUser(long id) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(DELETE_BY_ID);

			preparedStatement.setLong(1, id);

			return preparedStatement.executeUpdate() > 0;
		} catch (SQLException e) {
			throw new DAOException(
					"Something went wrong in database layer when deleting User.",
					e);
		} finally {
			DbUtils.close(preparedStatement, connection);
		}
	}

	@Override
	public List<String> getRolesOfUser(String login) {
		List<String> roles = new ArrayList<>();
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SELECT_USER_ROLES);

			preparedStatement.setString(1, login);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				String role = resultSet.getString("ROLE_NAME");
				roles.add(role);
			}
		} catch (SQLException e) {
			throw new DAOException(
					"Something went wrong in database layer when finding User by username.",
					e);
		} finally {
			DbUtils.close(resultSet, preparedStatement, connection);
		}
		return roles;
	}
}