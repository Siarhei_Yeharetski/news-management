package com.epam.newsmanagement.dao.jdbc;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.model.News;
import com.epam.newsmanagement.model.Tag;
import com.epam.newsmanagement.utils.DbUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

/**
 * TagDAOImpl class implements ITagDAO interface and provides access to TAG
 * database entity.
 *
 * @author Siarhei_Yeharetski
 */
@Repository
public class JdbcTagDAO implements TagDAO {

	private static final String TAG_ID_COLUMN = "TAG_ID";
	private static final String TAG_NAME_COLUMN = "TAG_NAME";

	private static final String SELECT_ALL = "SELECT TAG_ID, TAG_NAME FROM TAG";
	private static final String INSERT = "INSERT INTO TAG(TAG_ID, TAG_NAME) VALUES (?, ?)";
	private static final String INSERT_NO_ID = "INSERT INTO TAG(TAG_ID, TAG_NAME) VALUES (tag_id_seq.NEXTVAL, ?)";
	private static final String FIND_BY_ID = "SELECT TAG_ID, TAG_NAME FROM TAG WHERE TAG_ID = ?";
	private static final String FIND_BY_NEWS_ID = "SELECT TAG_ID, TAG_NAME FROM TAG WHERE TAG_ID IN (SELECT TAG_ID FROM NEWS_TAG WHERE NEWS_ID = ?)";
	private static final String DELETE_BY_ID = "DELETE FROM TAG WHERE TAG_ID = ?";
	private static final String DELETE_NEWS_TAG = "DELETE FROM NEWS_TAG WHERE NEWS_ID = ?";
	private static final String UPDATE = "UPDATE TAG SET TAG_NAME = ? WHERE TAG_ID = ?";

	private static final String INSERT_NEWS_TAG = "INSERT INTO NEWS_TAG(NEWS_TAG_ID, NEWS_ID, TAG_ID) VALUES(news_tag_id_seq.NEXTVAL, ?, ?)";

	@Autowired
	private DataSource dataSource;

	public JdbcTagDAO() {
	}

	@Override
	public List<Tag> findAll() {
		List<Tag> tagList = new ArrayList<>();
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			statement = connection.createStatement();
			resultSet = statement.executeQuery(SELECT_ALL);

			while (resultSet.next()) {
				Tag tag = new Tag();
				tag.setId(resultSet.getLong(TAG_ID_COLUMN));
				tag.setName(resultSet.getString(TAG_NAME_COLUMN));
				tagList.add(tag);
			}
		} catch (SQLException e) {
			throw new DAOException(
					"Something went wrong in database layer when finding all Tags.",
					e);
		} finally {
			DbUtils.close(resultSet, statement, connection);
		}
		return tagList;
	}

	@Override
	public Tag findById(long id) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(FIND_BY_ID);

			preparedStatement.setLong(1, id);

			resultSet = preparedStatement.executeQuery();

			if (resultSet.isBeforeFirst()) {
				Tag tag = new Tag();

				while (resultSet.next()) {
					tag.setId(resultSet.getLong(TAG_ID_COLUMN));
					tag.setName(resultSet.getString(TAG_NAME_COLUMN));
				}

				return tag;
			} else {
				return null;
			}
		} catch (SQLException e) {
			throw new DAOException(
					"Something went wrong in database layer when finding Tag by id.",
					e);
		} finally {
			DbUtils.close(resultSet, preparedStatement, connection);
		}
	}

	@Override
	public long create(Tag tag) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);

			if (tag.getId() == null) {
				preparedStatement = connection.prepareStatement(INSERT_NO_ID,
						new String[] { "TAG_ID" });

				preparedStatement.setString(1, tag.getName());
			} else {
				preparedStatement = connection.prepareStatement(INSERT,
						new String[] { "TAG_ID" });

				preparedStatement.setLong(1, tag.getId());
				preparedStatement.setString(2, tag.getName());
			}

			preparedStatement.executeUpdate();

			resultSet = preparedStatement.getGeneratedKeys();

			if (resultSet.next()) {
				return resultSet.getLong(1);
			} else {
				throw new DAOException(
						"Could not retrieve TAG_ID value from DB.");
			}
		} catch (SQLException e) {
			throw new DAOException(
					"Something went wrong in database layer when creating new Tag.",
					e);
		} finally {
			DbUtils.close(preparedStatement, connection);
		}
	}

	@Override
	public boolean delete(long id) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(DELETE_BY_ID);

			preparedStatement.setLong(1, id);

			return preparedStatement.executeUpdate() > 0;
		} catch (SQLException e) {
			throw new DAOException(
					"Something went wrong in database layer when deleting Tag.",
					e);
		} finally {
			DbUtils.close(preparedStatement, connection);
		}
	}

	@Override
	public boolean update(Tag tag) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(UPDATE);

			preparedStatement.setLong(2, tag.getId());
			preparedStatement.setString(1, tag.getName());

			return preparedStatement.executeUpdate() > 0;
		} catch (SQLException e) {
			throw new DAOException(
					"Something went wrong in database layer when updating Tag.",
					e);
		} finally {
			DbUtils.close(preparedStatement, connection);
		}
	}

	@Override
	public long createNewsTag(News news, Tag tag) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);

			preparedStatement = connection.prepareStatement(INSERT_NEWS_TAG,
					new String[] { "NEWS_TAG_ID" });

			preparedStatement.setLong(1, news.getId());
			preparedStatement.setLong(2, tag.getId());

			preparedStatement.executeUpdate();

			resultSet = preparedStatement.getGeneratedKeys();

			if (resultSet.next()) {
				return resultSet.getLong(1);
			} else {
				throw new DAOException(
						"Could not retrieve NEWS_TAG_ID value from DB.");
			}
		} catch (SQLException e) {
			throw new DAOException(
					"Something went wrong in database layer when creating new NewsTag.",
					e);
		} finally {
			DbUtils.close(preparedStatement, connection);
		}
	}

	@Override
	public List<Tag> findByNewsId(long id) {
		List<Tag> tagList = new ArrayList<>();
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(FIND_BY_NEWS_ID);

			preparedStatement.setLong(1, id);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				Tag tag = new Tag();
				tag.setId(resultSet.getLong(TAG_ID_COLUMN));
				tag.setName(resultSet.getString(TAG_NAME_COLUMN));
				tagList.add(tag);
			}
		} catch (SQLException e) {
			throw new DAOException(
					"Something went wrong in database layer when finding Tag by News id.",
					e);
		} finally {
			DbUtils.close(resultSet, preparedStatement, connection);
		}
		return tagList;
	}

	@Override
	public boolean deleteNewsTag(long id) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(DELETE_NEWS_TAG);

			preparedStatement.setLong(1, id);

			return preparedStatement.executeUpdate() > 0;
		} catch (SQLException e) {
			throw new DAOException(
					"Something went wrong in database layer when deleting NewsTag.",
					e);
		} finally {
			DbUtils.close(preparedStatement, connection);
		}
	}
}
