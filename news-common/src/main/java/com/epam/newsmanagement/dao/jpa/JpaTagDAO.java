package com.epam.newsmanagement.dao.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.model.News;
import com.epam.newsmanagement.model.NewsTag;
import com.epam.newsmanagement.model.Tag;

public class JpaTagDAO implements TagDAO {

	private static final String FIND_BY_NEWS_ID = "SELECT t.id, t.name FROM Tag t WHERE t.id IN (SELECT n.tagId FROM NewsTag n WHERE n.newsId = :newsId)";

	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<Tag> findAll() {
		try {
			Query query = entityManager.createNamedQuery("Tag.findAll");
			return query.getResultList();
		} catch (PersistenceException e) {
			throw new DAOException(
					"Something went wrong in database layer when finding all Tags.",
					e);
		}
	}

	@Override
	@Transactional
	public Tag findById(long id) {
		try {
			return entityManager.find(Tag.class, id);
		} catch (PersistenceException e) {
			throw new DAOException(
					"Something went wrong in database layer when finding Tag by id.",
					e);
		}
	}

	@Override
	@Transactional
	public long create(Tag tag) {
		try {
			entityManager.persist(tag);
			entityManager.flush();
			return tag.getId();
		} catch (PersistenceException e) {
			throw new DAOException(
					"Something went wrong in database layer when creating new Tag.",
					e);
		}
	}

	@Override
	@Transactional
	public boolean delete(long id) {
		try {
			Tag tag = findById(id);
			entityManager.remove(tag);
			return true;
		} catch (PersistenceException e) {
			throw new DAOException(
					"Something went wrong in database layer when deleting Tag.",
					e);
		}
	}

	@Override
	@Transactional
	public boolean update(Tag tag) {
		try {
			entityManager.merge(tag);
			return true;
		} catch (PersistenceException e) {
			throw new DAOException(
					"Something went wrong in database layer when updating Tag.",
					e);
		}
	}

	@Override
	@Transactional
	public long createNewsTag(News news, Tag tag) {
		try {
			NewsTag newsTag = new NewsTag();
			newsTag.setNewsId(news.getId());
			newsTag.setTagId(tag.getId());
			entityManager.persist(newsTag);
			entityManager.flush();
			return newsTag.getId();
		} catch (PersistenceException e) {
			throw new DAOException(
					"Something went wrong in database layer when creating new NewsTag.",
					e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<Tag> findByNewsId(long id) {
		try {
			Query query = entityManager.createQuery(FIND_BY_NEWS_ID, Tag.class);
			query.setParameter("newsId", id);
			return query.getResultList();
		} catch (PersistenceException e) {
			throw new DAOException(
					"Something went wrong in database layer when finding Tag by News id.",
					e);
		}
	}

	@Override
	public boolean deleteNewsTag(long id) {
		// TODO Auto-generated method stub
		return false;
	}

}
