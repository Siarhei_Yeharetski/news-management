package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.model.News;
import com.epam.newsmanagement.model.helper.SearchCriteria;

import java.util.List;

/**
 * INewsDAO provides DAO interface for database NEWS table.
 *
 * @author Siarhei_Yeharetski
 */
public interface NewsDAO {

    /**
     * Get News by id from the database.
     *
     * @param id the id of News.
     * @return News from the database.
     */
    News findById(long id);

    /**
     * Adds News to the database.
     *
     * @param news the News.
     * @return id of the added News.
     */
    long create(News news);

    /**
     * Deletes News by id.
     *
     * @param id the News id to be deleted.
     * @return true, if successfully deleted.
     */
    boolean delete(long id);

    /**
     * Updates News.
     *
     * @param news the News to be updated.
     * @return true, if successfully updated.
     */
    boolean update(News news);

    /**
     * Return list of news depending on search criteria.
     *
     * @param searchCriteria object containing author or tag list.
     * @return all news, if criteria is empty, or news list depending on search criteria content.
     */
    List<News> findByCriteria(SearchCriteria searchCriteria);
    
    /**
     * Return count of news affected by SearchCriteria.
     * 
     * @param filter
     * @return number of news.
     */
    public int countOfNews(SearchCriteria filter);
    
    /**
     * Selects news from database according to indexes and SearchCriteria
     * 
     * @param firstIndex
     * @param secondIndex
     * @param filter
     * @return News collection.
     */
    List<News> selectNewsForPage(int firstIndex, int secondIndex, SearchCriteria filter);
    
    News next(long newsId);
    
    News prev(long newsId);
}
