package com.epam.newsmanagement.dao.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.UserDAO;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.model.Roles;
import com.epam.newsmanagement.model.User;

public class HibernateUserDAO implements UserDAO {

	private static final String SELECT_USER_ROLES = "SELECT USER_ID, ROLE_NAME FROM ROLES WHERE USER_ID IN (SELECT USER_ID FROM USERTABLE WHERE LOGIN = :login)";

	@Autowired
	private SessionFactory sessionFactory;

	protected final Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	@Transactional
	public User userIsExist(String login) {
		try {
			Criteria criteria = getCurrentSession().createCriteria(User.class);
			criteria.add(Restrictions.eq("login", login));
			if (criteria.list().isEmpty()) {
				return null;
			} else {
				return (User) criteria.list().get(0);
			}
		} catch (HibernateException e) {
			throw new DAOException(
					"Something went wrong in database layer when finding User by username.",
					e);
		}
	}

	@Override
	@Transactional
	public long addUser(User user) {
		try {
			getCurrentSession().save(user);
			return user.getId();
		} catch (HibernateException e) {
			throw new DAOException(
					"Something went wrong in database layer when creating new User.",
					e);
		}
	}

	@Override
	@Transactional
	public boolean deleteUser(long id) {
		try {
			User user = (User) getCurrentSession().load(User.class, id);
			getCurrentSession().delete(user);
			return true;
		} catch (HibernateException e) {
			throw new DAOException(
					"Something went wrong in database layer when deleting User.",
					e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<String> getRolesOfUser(String login) {
		try {
			List<String> userRoles = new ArrayList<String>();
			Query query = getCurrentSession().createSQLQuery(SELECT_USER_ROLES)
					.addEntity(Roles.class).setParameter("login", login);
			List<Roles> rolesList = query.list();
			for (Roles role : rolesList) {
				userRoles.add(role.getRoleName());
			}
			return userRoles;
		} catch (HibernateException e) {
			throw new DAOException(
					"Something went wrong in database layer when finding User roles.",
					e);
		}
	}

}
