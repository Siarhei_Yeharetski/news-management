package com.epam.newsmanagement.dao.hibernate;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.model.Comment;
import com.epam.newsmanagement.model.News;

public class HibernateCommentDAO implements CommentDAO {

	@Autowired
	private SessionFactory sessionFactory;

	protected final Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<Comment> findAll() {
		try {
			return getCurrentSession().createCriteria(Comment.class).list();
		} catch (HibernateException e) {
			throw new DAOException(
					"Something went wrong in database layer when finding all Comments.",
					e);
		}
	}

	@Override
	@Transactional
	public Comment findById(long id) {
		try {
			return (Comment) getCurrentSession().get(Comment.class, id);
		} catch (HibernateException e) {
			throw new DAOException(
					"Something went wrong in database layer when finding Comment by id.",
					e);
		}
	}

	@Override
	@Transactional
	public long create(Comment comment) {
		try {
			getCurrentSession().save(comment);
			return comment.getId();
		} catch (HibernateException e) {
			throw new DAOException(
					"Something went wrong in database layer when creating new Comment.",
					e);
		}
	}

	@Override
	@Transactional
	public long create(News news, Comment comment) {
		try {
			comment.setNewsId(news.getId());
			return create(comment);
		} catch (HibernateException e) {
			throw new DAOException(
					"Something went wrong in database layer when creating new Comment.",
					e);
		}
	}

	@Override
	@Transactional
	public boolean delete(long id) {
		try {
			Comment comment = findById(id);
			getCurrentSession().delete(comment);
			return true;
		} catch (HibernateException e) {
			throw new DAOException(
					"Something went wrong in database layer when deleting Comment.",
					e);
		}
	}

	@Override
	@Transactional
	public boolean update(Comment comment) {
		try {
			getCurrentSession().update(comment);
			return true;
		} catch (HibernateException e) {
			throw new DAOException(
					"Something went wrong in database layer when updating Comment.",
					e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<Comment> findByNewsId(long id) {
		try {
			Criteria criteria = getCurrentSession().createCriteria(
					Comment.class);
			criteria.add(Restrictions.eq("newsId", id));
			criteria.addOrder(Order.desc("creationDate"));
			return criteria.list();
		} catch (HibernateException e) {
			throw new DAOException(
					"Something went wrong in database layer when finding Comment by News id.",
					e);
		}
	}

}
