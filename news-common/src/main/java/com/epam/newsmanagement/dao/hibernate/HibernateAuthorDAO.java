package com.epam.newsmanagement.dao.hibernate;

import java.util.Calendar;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.model.Author;
import com.epam.newsmanagement.model.News;
import com.epam.newsmanagement.model.NewsAuthor;

public class HibernateAuthorDAO implements AuthorDAO {

	private static final String FIND_BY_NEWS_ID = "SELECT AUTHOR_ID, NAME, EXPIRED FROM AUTHOR WHERE AUTHOR_ID = (SELECT AUTHOR_ID FROM NEWS_AUTHOR WHERE NEWS_ID = :newsId)";

	@Autowired
	private SessionFactory sessionFactory;

	protected final Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<Author> findAll() {
		try {
			return getCurrentSession().createCriteria(Author.class).list();
		} catch (HibernateException e) {
			throw new DAOException(
					"Something went wrong in database layer when finding all Authors.",
					e);
		}
	}

	@Override
	@Transactional
	public Author findById(long id) {
		try {
			return (Author) getCurrentSession().get(Author.class, id);
		} catch (HibernateException e) {
			throw new DAOException(
					"Something went wrong in database layer when finding Author by id.",
					e);
		}
	}

	@Override
	@Transactional
	public long create(Author author) {
		try {
			getCurrentSession().save(author);
			return author.getId();
		} catch (HibernateException e) {
			throw new DAOException(
					"Something went wrong in database layer when creating new Author.",
					e);
		}
	}

	@Override
	@Transactional
	public boolean delete(long id) {
		try {
			Author author = findById(id);
			getCurrentSession().delete(author);
			return true;
		} catch (HibernateException e) {
			throw new DAOException(
					"Something went wrong in database layer when deleting Author.",
					e);
		}
	}

	@Override
	@Transactional
	public boolean update(Author author) {
		try {
			getCurrentSession().update(author);
			return true;
		} catch (HibernateException e) {
			throw new DAOException(
					"Something went wrong in database layer when updating Author.",
					e);
		}
	}

	@Override
	@Transactional
	public long createNewsAuthor(News news, Author author) {
		try {
			NewsAuthor newsAuthor = new NewsAuthor();
			newsAuthor.setNewsId(news.getId());
			newsAuthor.setAuthorId(author.getId());
			getCurrentSession().save(newsAuthor);
			return newsAuthor.getId();
		} catch (HibernateException e) {
			throw new DAOException(
					"Something went wrong in database layer when creating new NewsAuthor.",
					e);
		}
	}

	@Override
	@Transactional
	public Author findByNewsId(long id) {
		try {
			Query query = getCurrentSession().createSQLQuery(FIND_BY_NEWS_ID)
					.addEntity(Author.class).setParameter("newsId", id);
			if (query.list().isEmpty()) {
				return null;
			} else {
				return (Author) query.list().get(0);
			}
		} catch (HibernateException e) {
			throw new DAOException(
					"Something went wrong in database layer when finding Author by News id.",
					e);
		}
	}

	@Override
	@Transactional
	public boolean expire(long id) {
		try {
			Author author = findById(id);
			author.setExpirationDate(Calendar.getInstance().getTime());
			update(author);
			return true;
		} catch (HibernateException e) {
			throw new DAOException(
					"Something went wrong in database layer when expiring Author.",
					e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<Author> findAllNotExpired() {
		try {
			Criteria criteria = getCurrentSession()
					.createCriteria(Author.class);
			criteria.add(Restrictions.isNull("expirationDate"));
			return criteria.list();
		} catch (HibernateException e) {
			throw new DAOException(
					"Something went wrong in database layer when finding all not expired Authors.",
					e);
		}
	}

	@Override
	@Transactional
	public boolean deleteNewsAuthor(long id) {
		try {
			NewsAuthor newsAuthor = (NewsAuthor) getCurrentSession().get(
					NewsAuthor.class, id);
			if (newsAuthor != null) {
				getCurrentSession().delete(newsAuthor);
			}
			return true;
		} catch (HibernateException e) {
			throw new DAOException(
					"Something went wrong in database layer when deleting NewsAuthor.",
					e);
		}
	}

}
