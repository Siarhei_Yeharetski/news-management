package com.epam.newsmanagement.dao.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.model.Comment;
import com.epam.newsmanagement.model.News;

public class JpaCommentDAO implements CommentDAO {

	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<Comment> findAll() {
		try {
			Query query = entityManager.createNamedQuery("Comment.findAll");
			return query.getResultList();
		} catch (PersistenceException e) {
			throw new DAOException(
					"Something went wrong in database layer when finding all Comments.",
					e);
		}
	}

	@Override
	@Transactional
	public Comment findById(long id) {
		try {
			return entityManager.find(Comment.class, id);
		} catch (PersistenceException e) {
			throw new DAOException(
					"Something went wrong in database layer when finding Comment by id.",
					e);
		}
	}

	@Override
	@Transactional
	public long create(Comment comment) {
		try {
			entityManager.persist(comment);
			entityManager.flush();
			return comment.getId();
		} catch (PersistenceException e) {
			throw new DAOException(
					"Something went wrong in database layer when creating new Comment.",
					e);
		}
	}

	@Override
	@Transactional
	public long create(News news, Comment comment) {
		try {
			comment.setId(news.getId());
			return create(comment);
		} catch (PersistenceException e) {
			throw new DAOException(
					"Something went wrong in database layer when creating new Comment.",
					e);
		}
	}

	@Override
	@Transactional
	public boolean delete(long id) {
		try {
			Comment comment = findById(id);
			entityManager.remove(comment);
			return true;
		} catch (PersistenceException e) {
			throw new DAOException(
					"Something went wrong in database layer when deleting Comment.",
					e);
		}
	}

	@Override
	@Transactional
	public boolean update(Comment comment) {
		try {
			entityManager.merge(comment);
			return true;
		} catch (PersistenceException e) {
			throw new DAOException(
					"Something went wrong in database layer when updating Comment.",
					e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<Comment> findByNewsId(long id) {
		try {
			Query query = entityManager
					.createNamedQuery("Comment.findByNewsId");
			query.setParameter("newsId", id);
			return query.getResultList();
		} catch (PersistenceException e) {
			throw new DAOException(
					"Something went wrong in database layer when finding Comment by News id.",
					e);
		}
	}

}
