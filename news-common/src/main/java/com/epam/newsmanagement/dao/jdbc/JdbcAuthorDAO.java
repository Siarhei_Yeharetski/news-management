package com.epam.newsmanagement.dao.jdbc;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.model.Author;
import com.epam.newsmanagement.model.News;
import com.epam.newsmanagement.utils.DateUtils;
import com.epam.newsmanagement.utils.DbUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.sql.DataSource;

/**
 * AuthorDAOImpl class implements IAuthorDAO interface and provides access to
 * AUTHOR database entity.
 *
 * @author Siarhei_Yeharetski
 */
@Repository
public class JdbcAuthorDAO implements AuthorDAO {

	private static final String AUTHOR_ID_COLUMN = "AUTHOR_ID";
	private static final String AUTHOR_NAME_COLUMN = "NAME";
	private static final String AUTHOR_EXPIRATION_DATE_COLUMN = "EXPIRED";

	private static final String SELECT_ALL = "SELECT AUTHOR_ID, NAME, EXPIRED FROM AUTHOR";
	private static final String SELECT_NOT_EXPIRED = "SELECT AUTHOR_ID, NAME FROM AUTHOR WHERE EXPIRED IS NULL";
	private static final String INSERT = "INSERT INTO AUTHOR(AUTHOR_ID, NAME, EXPIRED) VALUES (?, ?, ?)";
	private static final String INSERT_NO_ID = "INSERT INTO AUTHOR(AUTHOR_ID, NAME, EXPIRED) VALUES (author_id_seq.NEXTVAL, ?, ?)";
	private static final String FIND_BY_ID = "SELECT AUTHOR_ID, NAME, EXPIRED FROM AUTHOR WHERE AUTHOR_ID = ?";
	private static final String FIND_BY_NEWS_ID = "SELECT AUTHOR_ID, NAME, EXPIRED FROM AUTHOR WHERE AUTHOR.AUTHOR_ID = (SELECT AUTHOR_ID FROM NEWS_AUTHOR WHERE NEWS_AUTHOR.NEWS_ID = ?)";
	private static final String DELETE_BY_ID = "DELETE FROM AUTHOR WHERE AUTHOR_ID = ?";
	private static final String DELETE_NEWS_AUTHOR = "DELETE FROM NEWS_AUTHOR WHERE NEWS_ID = ?";
	private static final String UPDATE = "UPDATE AUTHOR SET NAME = ?, EXPIRED = ? WHERE AUTHOR_ID = ?";
	private static final String EXPIRE = "UPDATE AUTHOR SET EXPIRED = ? WHERE AUTHOR_ID = ?";

	private static final String INSERT_NEWS_AUTHOR = "INSERT INTO NEWS_AUTHOR(NEWS_AUTHOR_ID, NEWS_ID, AUTHOR_ID) VALUES (news_author_id_seq.NEXTVAL, ?, ?)";

	@Autowired
	private DataSource dataSource;

	public JdbcAuthorDAO() {
	}

	@Override
	public List<Author> findAll() {
		List<Author> authorList = new ArrayList<>();
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			statement = connection.createStatement();
			resultSet = statement.executeQuery(SELECT_ALL);

			while (resultSet.next()) {
				Author author = new Author();
				author.setId(resultSet.getLong(AUTHOR_ID_COLUMN));
				author.setName(resultSet.getString(AUTHOR_NAME_COLUMN));
				author.setExpirationDate(resultSet
						.getDate(AUTHOR_EXPIRATION_DATE_COLUMN));
				authorList.add(author);
			}
		} catch (SQLException e) {
			throw new DAOException(
					"Something went wrong in database layer when finding all Authors.",
					e);
		} finally {
			DbUtils.close(resultSet, statement, connection);
		}
		return authorList;
	}

	@Override
	public Author findById(long id) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(FIND_BY_ID);

			preparedStatement.setLong(1, id);

			resultSet = preparedStatement.executeQuery();

			if (resultSet.isBeforeFirst()) {
				Author author = new Author();

				while (resultSet.next()) {
					author.setId(resultSet.getLong(AUTHOR_ID_COLUMN));
					author.setName(resultSet.getString(AUTHOR_NAME_COLUMN));
					author.setExpirationDate(resultSet
							.getDate(AUTHOR_EXPIRATION_DATE_COLUMN));
				}

				return author;
			} else {
				return null;
			}
		} catch (SQLException e) {
			throw new DAOException(
					"Something went wrong in database layer when finding Author by id.",
					e);
		} finally {
			DbUtils.close(resultSet, preparedStatement, connection);
		}
	}

	@Override
	public long create(Author author) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);

			if (author.getId() == null) {
				preparedStatement = connection.prepareStatement(INSERT_NO_ID,
						new String[] { "AUTHOR_ID" });

				preparedStatement.setString(1, author.getName());
				if (author.getExpirationDate() != null) {
					preparedStatement.setDate(2, DateUtils
							.convertUtilDateToSqlDate(author
									.getExpirationDate()));
				} else {
					preparedStatement.setNull(2, Types.DATE);
				}
			} else {
				preparedStatement = connection.prepareStatement(INSERT,
						new String[] { "AUTHOR_ID" });

				preparedStatement.setLong(1, author.getId());
				preparedStatement.setString(2, author.getName());
				if (author.getExpirationDate() != null) {
					preparedStatement.setDate(3, DateUtils
							.convertUtilDateToSqlDate(author
									.getExpirationDate()));
				} else {
					preparedStatement.setNull(3, Types.DATE);
				}
			}

			preparedStatement.executeUpdate();

			resultSet = preparedStatement.getGeneratedKeys();

			if (resultSet.next()) {
				return resultSet.getLong(1);
			} else {
				throw new DAOException(
						"Could not retrieve AUTHOR_ID value from DB.");
			}
		} catch (SQLException e) {
			throw new DAOException(
					"Something went wrong in database layer when creating new Author.",
					e);
		} finally {
			DbUtils.close(preparedStatement, connection);
		}
	}

	@Override
	public boolean delete(long id) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(DELETE_BY_ID);

			preparedStatement.setLong(1, id);

			return preparedStatement.executeUpdate() > 0;
		} catch (SQLException e) {
			throw new DAOException(
					"Something went wrong in database layer when deleting Author.",
					e);
		} finally {
			DbUtils.close(preparedStatement, connection);
		}
	}

	@Override
	public boolean update(Author author) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(UPDATE);

			preparedStatement.setLong(3, author.getId());
			preparedStatement.setString(1, author.getName());
			if (author.getExpirationDate() != null) {
				preparedStatement.setDate(2, DateUtils
						.convertUtilDateToSqlDate(author.getExpirationDate()));
			} else {
				preparedStatement.setDate(2, null);
				// preparedStatement.setNull(2, Types.DATE);
			}

			return preparedStatement.executeUpdate() > 0;
		} catch (SQLException e) {
			throw new DAOException(
					"Something went wrong in database layer when updating Author.",
					e);
		} finally {
			DbUtils.close(preparedStatement, connection);
		}
	}

	@Override
	public long createNewsAuthor(News news, Author author) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);

			preparedStatement = connection.prepareStatement(INSERT_NEWS_AUTHOR,
					new String[] { "NEWS_AUTHOR_ID" });

			preparedStatement.setLong(1, news.getId());
			preparedStatement.setLong(2, author.getId());

			preparedStatement.executeUpdate();

			resultSet = preparedStatement.getGeneratedKeys();

			if (resultSet.next()) {
				return resultSet.getLong(1);
			} else {
				throw new DAOException(
						"Could not retrieve NEWS_AUTHOR_ID value from DB.");
			}
		} catch (SQLException e) {
			throw new DAOException(
					"Something went wrong in database layer when creating new NewsAuthor.",
					e);
		} finally {
			DbUtils.close(preparedStatement, connection);
		}
	}

	@Override
	public Author findByNewsId(long id) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(FIND_BY_NEWS_ID);

			preparedStatement.setLong(1, id);

			resultSet = preparedStatement.executeQuery();

			if (resultSet.isBeforeFirst()) {
				Author author = new Author();

				while (resultSet.next()) {
					author.setId(resultSet.getLong(AUTHOR_ID_COLUMN));
					author.setName(resultSet.getString(AUTHOR_NAME_COLUMN));
					author.setExpirationDate(resultSet
							.getDate(AUTHOR_EXPIRATION_DATE_COLUMN));
				}

				return author;
			} else {
				return null;
			}
		} catch (SQLException e) {
			throw new DAOException(
					"Something went wrong in database layer when finding Author by News id.",
					e);
		} finally {
			DbUtils.close(resultSet, preparedStatement, connection);
		}
	}

	@Override
	public boolean expire(long id) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(EXPIRE);

			preparedStatement.setLong(2, id);
			preparedStatement
					.setDate(1, DateUtils.convertUtilDateToSqlDate(Calendar
							.getInstance().getTime()));

			return preparedStatement.executeUpdate() > 0;
		} catch (SQLException e) {
			throw new DAOException(
					"Something went wrong in database layer when expiring Author.",
					e);
		} finally {
			DbUtils.close(preparedStatement, connection);
		}
	}

	@Override
	public List<Author> findAllNotExpired() {
		List<Author> authorList = new ArrayList<>();
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			statement = connection.createStatement();
			resultSet = statement.executeQuery(SELECT_NOT_EXPIRED);

			while (resultSet.next()) {
				Author author = new Author();
				author.setId(resultSet.getLong(AUTHOR_ID_COLUMN));
				author.setName(resultSet.getString(AUTHOR_NAME_COLUMN));
				authorList.add(author);
			}
		} catch (SQLException e) {
			throw new DAOException(
					"Something went wrong in database layer when finding all not expired Authors.",
					e);
		} finally {
			DbUtils.close(resultSet, statement, connection);
		}
		return authorList;
	}

	@Override
	public boolean deleteNewsAuthor(long id) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(DELETE_NEWS_AUTHOR);

			preparedStatement.setLong(1, id);

			return preparedStatement.executeUpdate() > 0;
		} catch (SQLException e) {
			throw new DAOException(
					"Something went wrong in database layer when deleting NewsAuthor.",
					e);
		} finally {
			DbUtils.close(preparedStatement, connection);
		}
	}
}
