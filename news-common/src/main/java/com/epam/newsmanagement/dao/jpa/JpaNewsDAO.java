package com.epam.newsmanagement.dao.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.model.News;
import com.epam.newsmanagement.model.helper.SearchCriteria;

public class JpaNewsDAO implements NewsDAO {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	@Transactional
	public News findById(long id) {
		try {
			return entityManager.find(News.class, id);
		} catch (PersistenceException e) {
			throw new DAOException(
					"Something went wrong in database layer when finding News by id.",
					e);
		}
	}

	@Override
	@Transactional
	public long create(News news) {
		try {
			entityManager.persist(news);
			entityManager.flush();
			return news.getId();
		} catch (PersistenceException e) {
			throw new DAOException(
					"Something went wrong in database layer when creating new News.",
					e);
		}
	}

	@Override
	@Transactional
	public boolean delete(long id) {
		try {
			News news = findById(id);
			entityManager.remove(news);
			return true;
		} catch (PersistenceException e) {
			throw new DAOException(
					"Something went wrong in database layer when deleting News.",
					e);
		}
	}

	@Override
	@Transactional
	public boolean update(News news) {
		try {
			entityManager.merge(news);
			return true;
		} catch (PersistenceException e) {
			throw new DAOException(
					"Something went wrong in database layer when updating News.",
					e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<News> findByCriteria(SearchCriteria searchCriteria) {
		try {
			Query query = entityManager.createNamedQuery("News.findAll");
			return query.getResultList();
		} catch (PersistenceException e) {
			throw new DAOException(
					"Something went wrong in database layer when finding News.",
					e);
		}
	}

	@Override
	@Transactional
	public int countOfNews(SearchCriteria filter) {
		try {
			Query query = entityManager.createNamedQuery("News.findAll");
			return query.getResultList().size();
		} catch (PersistenceException e) {
			throw new DAOException(
					"Something went wrong in database layer when finding News count.",
					e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<News> selectNewsForPage(int firstIndex, int secondIndex,
			SearchCriteria filter) {
		try {
			Query query = entityManager.createNamedQuery("News.findAll");
			return query.getResultList();
		} catch (PersistenceException e) {
			throw new DAOException(
					"Something went wrong in database layer when finding News.",
					e);
		}
	}

	@Override
	public News next(long newsId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public News prev(long newsId) {
		// TODO Auto-generated method stub
		return null;
	}

}
