package com.epam.newsmanagement.dao.hibernate;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.model.News;
import com.epam.newsmanagement.model.NewsTag;
import com.epam.newsmanagement.model.Tag;

public class HibernateTagDAO implements TagDAO {

	private static final String FIND_BY_NEWS_ID = "SELECT TAG_ID, TAG_NAME FROM TAG WHERE TAG_ID IN (SELECT TAG_ID FROM NEWS_TAG WHERE NEWS_ID = :newsId)";

	@Autowired
	private SessionFactory sessionFactory;

	protected Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<Tag> findAll() {
		try {
			return getCurrentSession().createCriteria(Tag.class).list();
		} catch (HibernateException e) {
			throw new DAOException(
					"Something went wrong in database layer when finding all Tags.",
					e);
		}
	}

	@Override
	@Transactional
	public Tag findById(long id) {
		try {
			return (Tag) getCurrentSession().get(Tag.class, id);
		} catch (HibernateException e) {
			throw new DAOException(
					"Something went wrong in database layer when finding Tag by id.",
					e);
		}
	}

	@Override
	@Transactional
	public long create(Tag tag) {
		try {
			getCurrentSession().save(tag);
			return tag.getId();
		} catch (HibernateException e) {
			throw new DAOException(
					"Something went wrong in database layer when creating new Tag.",
					e);
		}
	}

	@Override
	@Transactional
	public boolean delete(long id) {
		try {
			Tag tag = findById(id);
			getCurrentSession().delete(tag);
			return true;
		} catch (HibernateException e) {
			throw new DAOException(
					"Something went wrong in database layer when deleting Tag.",
					e);
		}
	}

	@Override
	@Transactional
	public boolean update(Tag tag) {
		try {
			getCurrentSession().update(tag);
			return true;
		} catch (HibernateException e) {
			throw new DAOException(
					"Something went wrong in database layer when updating Tag.",
					e);
		}
	}

	@Override
	@Transactional
	public long createNewsTag(News news, Tag tag) {
		try {
			NewsTag newsTag = new NewsTag();
			newsTag.setNewsId(news.getId());
			newsTag.setTagId(tag.getId());
			getCurrentSession().save(newsTag);
			return newsTag.getId();
		} catch (HibernateException e) {
			throw new DAOException(
					"Something went wrong in database layer when creating new NewsTag.",
					e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<Tag> findByNewsId(long id) {
		try {
			Query query = getCurrentSession().createSQLQuery(FIND_BY_NEWS_ID)
					.addEntity(Tag.class).setParameter("newsId", id);
			return query.list();
		} catch (HibernateException e) {
			throw new DAOException(
					"Something went wrong in database layer when finding Tag by News id.",
					e);
		}
	}

	@Override
	@Transactional
	public boolean deleteNewsTag(long id) {
		try {
			NewsTag newsTag = (NewsTag) getCurrentSession().get(NewsTag.class,
					id);
			if (newsTag != null) {
				getCurrentSession().delete(newsTag);
			}
			return true;
		} catch (HibernateException e) {
			throw new DAOException(
					"Something went wrong in database layer when deleting NewsTag.",
					e);
		}
	}

}
