package com.epam.newsmanagement.dao.hibernate;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.model.News;
import com.epam.newsmanagement.model.helper.SearchCriteria;

public class HibernateNewsDAO implements NewsDAO {
	
	private static final String FIND_NEXT = "SELECT * FROM (SELECT * FROM NEWS WHERE NEWS.NEWS_ID > :newsId ORDER BY NEWS.NEWS_ID asc) WHERE ROWNUM = 1";
	private static final String FIND_PREV = "SELECT * FROM (SELECT * FROM NEWS WHERE NEWS.NEWS_ID < :newsId ORDER BY NEWS.NEWS_ID desc) WHERE ROWNUM = 1";

	@Autowired
	private SessionFactory sessionFactory;

	protected final Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	@Transactional
	public News findById(long id) {
		try {
			return (News) getCurrentSession().get(News.class, id);
		} catch (HibernateException e) {
			throw new DAOException(
					"Something went wrong in database layer when finding News by id.",
					e);
		}
	}

	@Override
	@Transactional
	public long create(News news) {
		try {
			getCurrentSession().save(news);
			return news.getId();
		} catch (HibernateException e) {
			throw new DAOException(
					"Something went wrong in database layer when creating new News.",
					e);
		}
	}

	@Override
	@Transactional
	public boolean delete(long id) {
		try {
			News news = findById(id);
			getCurrentSession().delete(news);
			return true;
		} catch (HibernateException e) {
			throw new DAOException(
					"Something went wrong in database layer when deleting News.",
					e);
		}
	}

	@Override
	@Transactional
	public boolean update(News news) {
		try {
			getCurrentSession().update(news);
			return true;
		} catch (HibernateException e) {
			throw new DAOException(
					"Something went wrong in database layer when updating News.",
					e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<News> findByCriteria(SearchCriteria searchCriteria) {
		try {
			return getCurrentSession().createCriteria(News.class).list();
		} catch (HibernateException e) {
			throw new DAOException(
					"Something went wrong in database layer when finding News.",
					e);
		}
	}

	@Override
	@Transactional
	public int countOfNews(SearchCriteria filter) {
		try {
			return getCurrentSession().createCriteria(News.class).list().size();
		} catch (HibernateException e) {
			throw new DAOException(
					"Something went wrong in database layer when finding News count.",
					e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<News> selectNewsForPage(int firstIndex, int secondIndex,
			SearchCriteria filter) {
		try {
			return getCurrentSession().createCriteria(News.class).list();
		} catch (HibernateException e) {
			throw new DAOException(
					"Something went wrong in database layer when finding News.",
					e);
		}
	}

	@Override
	@Transactional
	public News next(long newsId) {
		Query query = getCurrentSession().createSQLQuery(FIND_NEXT).addEntity(News.class).setParameter("newsId", newsId);
		if  (query.list().isEmpty()) {
			return null;
		} else {
			return (News) query.list().get(0);
		}
	}

	@Override
	@Transactional
	public News prev(long newsId) {
		Query query = getCurrentSession().createSQLQuery(FIND_PREV).addEntity(News.class).setParameter("newsId", newsId);
		if  (query.list().isEmpty()) {
			return null;
		} else {
			return (News) query.list().get(0);
		}
	}

}
