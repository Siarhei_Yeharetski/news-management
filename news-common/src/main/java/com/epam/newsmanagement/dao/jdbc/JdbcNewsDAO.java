package com.epam.newsmanagement.dao.jdbc;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.model.News;
import com.epam.newsmanagement.model.Tag;
import com.epam.newsmanagement.model.helper.SearchCriteria;
import com.epam.newsmanagement.utils.DateUtils;
import com.epam.newsmanagement.utils.DbUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

/**
 * NewsDAOImpl class implements INewsDAO interface and provides access to NEWS
 * database entity.
 *
 * @author Siarhei_Yeharetski
 */
@Repository
public class JdbcNewsDAO implements NewsDAO {

	private static final String NEWS_ID_COLUMN = "NEWS_ID";
	private static final String NEWS_SHORT_TEXT_COLUMN = "SHORT_TEXT";
	private static final String NEWS_FULL_TEXT_COLUMN = "FULL_TEXT";
	private static final String NEWS_TITLE_COLUMN = "TITLE";
	private static final String NEWS_CREATION_DATE_COLUMN = "CREATION_DATE";
	private static final String NEWS_MODIFICATION_DATE_COLUMN = "MODIFICATION_DATE";
	private static final String NEWS_VERSION_COLUMN = "VERSION";

	private static final String INSERT = "INSERT INTO NEWS(NEWS_ID, SHORT_TEXT, FULL_TEXT, TITLE, CREATION_DATE, MODIFICATION_DATE, VERSION) VALUES (?, ?, ?, ?, ?, ?, 1)";
	private static final String INSERT_NO_ID = "INSERT INTO NEWS(NEWS_ID, SHORT_TEXT, FULL_TEXT, TITLE, CREATION_DATE, MODIFICATION_DATE, VERSION) VALUES (news_id_seq.NEXTVAL, ?, ?, ?, ?, ?, 1)";
	private static final String FIND_BY_ID = "SELECT NEWS_ID, SHORT_TEXT, FULL_TEXT, TITLE, CREATION_DATE, MODIFICATION_DATE, VERSION FROM NEWS WHERE NEWS_ID = ?";
	private static final String FIND_NEXT = "SELECT * FROM (SELECT * FROM NEWS WHERE NEWS.NEWS_ID > ? ORDER BY NEWS.NEWS_ID asc) WHERE ROWNUM = 1";
	private static final String FIND_PREV = "SELECT * FROM (SELECT * FROM NEWS WHERE NEWS.NEWS_ID < ? ORDER BY NEWS.NEWS_ID desc) WHERE ROWNUM = 1";
	private static final String DELETE_BY_ID = "DELETE FROM NEWS WHERE NEWS_ID = ?";
	private static final String UPDATE = "UPDATE NEWS SET SHORT_TEXT = ?, FULL_TEXT = ?, TITLE = ?, MODIFICATION_DATE = ?, VERSION = (VERSION + 1) WHERE ((NEWS_ID = ?) AND (VERSION = ?))";

	private static final String NEWS_FIELDS_SELECT = "SELECT NEWS.NEWS_ID, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.TITLE, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE, NEWS.VERSION FROM NEWS ";
	private static final String NEWS_JOIN_AUTHOR = "JOIN NEWS_AUTHOR ON NEWS_AUTHOR.NEWS_ID = NEWS.NEWS_ID JOIN AUTHOR ON AUTHOR.AUTHOR_ID = NEWS_AUTHOR.AUTHOR_ID ";
	// private static final String NEWS_JOIN_AUTHOR_ANDREI =
	// "JOIN NEWS_AUTHOR ON NEWS_AUTHOR.NEWS_ID = NEWS.NEWS_ID AND NEWS_AUTHOR.AUTHOR_ID = ? ";
	private static final String NEWS_JOIN_TAG = "JOIN NEWS_TAG ON NEWS_TAG.NEWS_ID = NEWS.NEWS_ID JOIN TAG ON TAG.TAG_ID = NEWS_TAG.TAG_ID ";
	private static final String NEWS_GROUPPED_BY_COMMENTS = "LEFT JOIN (SELECT NEWS_ID, COUNT(*) AS NUM_COMMENTS FROM COMMENTS GROUP BY NEWS_ID) CMT ON CMT.NEWS_ID = NEWS.NEWS_ID ";
	private static final String NEWS_SELECT_ORDER = "ORDER BY NUM_COMMENTS DESC, NEWS.CREATION_DATE DESC";

	@Autowired
	private DataSource dataSource;

	public JdbcNewsDAO() {
	}

	@Override
	public News findById(long id) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(FIND_BY_ID);

			preparedStatement.setLong(1, id);

			resultSet = preparedStatement.executeQuery();

			if (resultSet.isBeforeFirst()) {
				News news = new News();

				while (resultSet.next()) {
					news.setId(resultSet.getLong(NEWS_ID_COLUMN));
					news.setShortText(resultSet
							.getString(NEWS_SHORT_TEXT_COLUMN));
					news.setFullText(resultSet.getString(NEWS_FULL_TEXT_COLUMN));
					news.setTitle(resultSet.getString(NEWS_TITLE_COLUMN));
					news.setCreationDate(resultSet
							.getTimestamp(NEWS_CREATION_DATE_COLUMN));
					news.setModificationDate(resultSet
							.getDate(NEWS_MODIFICATION_DATE_COLUMN));
					news.setVersion(resultSet.getLong(NEWS_VERSION_COLUMN));
				}

				return news;
			} else {
				return null;
			}
		} catch (SQLException e) {
			throw new DAOException(
					"Something went wrong in database layer when finding News by id.",
					e);
		} finally {
			DbUtils.close(resultSet, preparedStatement, connection);
		}
	}

	@Override
	public long create(News news) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);

			if (news.getId() == null) {
				preparedStatement = connection.prepareStatement(INSERT_NO_ID,
						new String[] { "NEWS_ID" });

				preparedStatement.setString(1, news.getShortText());
				preparedStatement.setString(2, news.getFullText());
				preparedStatement.setString(3, news.getTitle());
				preparedStatement.setTimestamp(4, DateUtils
						.convertUtilDateToSqlTimestamp(news.getCreationDate()));
				preparedStatement.setDate(5, DateUtils
						.convertUtilDateToSqlDate(news.getModificationDate()));
			} else {
				preparedStatement = connection.prepareStatement(INSERT,
						new String[] { "NEWS_ID" });

				preparedStatement.setLong(1, news.getId());
				preparedStatement.setString(2, news.getShortText());
				preparedStatement.setString(3, news.getFullText());
				preparedStatement.setString(4, news.getTitle());
				preparedStatement.setTimestamp(5, DateUtils
						.convertUtilDateToSqlTimestamp(news.getCreationDate()));
				preparedStatement.setDate(6, DateUtils
						.convertUtilDateToSqlDate(news.getModificationDate()));
			}

			preparedStatement.executeUpdate();

			resultSet = preparedStatement.getGeneratedKeys();

			if (resultSet.next()) {
				return resultSet.getLong(1);
			} else {
				throw new DAOException(
						"Could not retrieve NEWS_ID value from DB.");
			}
		} catch (SQLException e) {
			throw new DAOException(
					"Something went wrong in database layer when creating new News.",
					e);
		} finally {
			DbUtils.close(preparedStatement, connection);
		}
	}

	@Override
	public boolean delete(long id) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(DELETE_BY_ID);

			preparedStatement.setLong(1, id);

			return preparedStatement.executeUpdate() > 0;
		} catch (SQLException e) {
			throw new DAOException(
					"Something went wrong in database layer when deleting News.",
					e);
		} finally {
			DbUtils.close(preparedStatement, connection);
		}
	}

	@Override
	public boolean update(News news) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(UPDATE);

			preparedStatement.setLong(5, news.getId());
			preparedStatement.setLong(6, news.getVersion());
			preparedStatement.setString(1, news.getShortText());
			preparedStatement.setString(2, news.getFullText());
			preparedStatement.setString(3, news.getTitle());
			preparedStatement.setDate(4, DateUtils
					.convertUtilDateToSqlDate(news.getModificationDate()));

			if (preparedStatement.executeUpdate() > 0) {
				return true;
			} else {
				throw new DAOException("Locking error when updating News.");
			}
		} catch (SQLException e) {
			throw new DAOException(
					"Something went wrong in database layer when updating News.",
					e);
		} finally {
			DbUtils.close(preparedStatement, connection);
		}
	}

	@Override
	public List<News> findByCriteria(SearchCriteria searchCriteria) {
		List<News> newsList = new ArrayList<>();
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);

			StringBuilder query = new StringBuilder();
			StringBuilder authorQuery = new StringBuilder();
			StringBuilder tagQuery = new StringBuilder();

			query.append(NEWS_FIELDS_SELECT);
			query.append(NEWS_JOIN_AUTHOR);
			query.append(NEWS_JOIN_TAG);
			query.append(NEWS_GROUPPED_BY_COMMENTS);

			// BEGIN
			// query.append(NEWS_FIELDS_SELECT);
			// if (searchCriteria.getAuthor() != null ) {
			// query.append(NEWS_JOIN_AUTHOR_ANDREI);
			// }
			// if (searchCriteria.getTagList() != null &&
			// !searchCriteria.getTagList().isEmpty()) {
			// query.append(NEWS_JOIN_TAGS_ANDREI);
			// ....
			// }
			// query.append(NEWS_GROUPPED_BY_COMMENTS);
			// END

			if (searchCriteria.getAuthor() != null
					&& searchCriteria.getAuthor().getName() != null) {
				authorQuery.append("WHERE AUTHOR.NAME = '");
				authorQuery.append(searchCriteria.getAuthor().getName());
				authorQuery.append("' ");
				query.append(authorQuery);
			}

			if (searchCriteria.getTagList() != null
					&& !searchCriteria.getTagList().isEmpty()) {
				if (authorQuery.length() == 0) {
					tagQuery.append("WHERE ");
				} else {
					tagQuery.append("AND ");
				}

				tagQuery.append("TAG.TAG_NAME IN (");

				for (int i = 0; i < searchCriteria.getTagList().size(); i++) {
					if (i > 0) {
						tagQuery.append(",");
					}
					tagQuery.append("'");
					tagQuery.append(searchCriteria.getTagList().get(i)
							.getName());
					tagQuery.append("'");
				}
				tagQuery.append(") ");
				query.append(tagQuery);
			}

			query.append(NEWS_SELECT_ORDER);

			statement = connection.createStatement();
			resultSet = statement.executeQuery(query.toString());

			if (resultSet.isBeforeFirst()) {
				while (resultSet.next()) {
					News news = new News();
					news.setId(resultSet.getLong(NEWS_ID_COLUMN));
					news.setShortText(resultSet
							.getString(NEWS_SHORT_TEXT_COLUMN));
					news.setFullText(resultSet.getString(NEWS_FULL_TEXT_COLUMN));
					news.setTitle(resultSet.getString(NEWS_TITLE_COLUMN));
					news.setCreationDate(resultSet
							.getTimestamp(NEWS_CREATION_DATE_COLUMN));
					news.setModificationDate(resultSet
							.getDate(NEWS_MODIFICATION_DATE_COLUMN));
					news.setVersion(resultSet.getLong(NEWS_VERSION_COLUMN));
					newsList.add(news);
				}
				return newsList;
			} else {
				return null;
			}
		} catch (SQLException e) {
			throw new DAOException(
					"Something went wrong in database layer when finding News.",
					e);
		} finally {
			DbUtils.close(resultSet, statement, connection);
		}
	}

	@Override
	public int countOfNews(SearchCriteria filter) {
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			statement = connection.createStatement();
			resultSet = statement.executeQuery("SELECT COUNT(NEWS_ID) FROM "
					+ filterQueryMaker(filter) + " NEWS");

			if (resultSet.next()) {
				return resultSet.getInt("COUNT(NEWS_ID)");
			} else {
				throw new DAOException(
						"Could not retrieve Count(NEWS_ID) value from DB.");
			}
		} catch (SQLException e) {
			throw new DAOException(
					"Something went wrong in database layer when finding count of News.",
					e);
		} finally {
			DbUtils.close(resultSet, statement, connection);
		}
	}

	private String filterQueryMaker(SearchCriteria filter) {
		StringBuilder filterGuery = new StringBuilder("");
		if (filter.getTagList() == null) {
			filter.setTagList(new ArrayList<Tag>());
		}
		String resultTags = join(filter.getTagList(), "\', \'");
		if (!filter.getTagList().isEmpty()
				&& "".equals(filter.getAuthor().getName())) {
			filterGuery
					.append(" (SELECT * FROM NEWS INNER JOIN "
							+ "(SELECT NEWS_ID AS FILTER_TAG_NEWS_ID FROM "
							+ "(SELECT NEWS_TAG.NEWS_ID, TAG.TAG_NAME "
							+ "FROM NEWS_TAG INNER JOIN TAG ON NEWS_TAG.TAG_ID = TAG.TAG_ID) "
							+ "WHERE TAG_NAME IN (\'")
					.append(resultTags)
					.append("\') "
							+ "GROUP BY NEWS_ID) ON NEWS.NEWS_ID = FILTER_TAG_NEWS_ID) ");
		}
		if (filter.getTagList().isEmpty()
				&& !"".equals(filter.getAuthor().getName())) {
			filterGuery
					.append(" (SELECT * FROM NEWS INNER JOIN "
							+ "(SELECT NEWS_ID AS FILTER_AUTHOR_NEWS_ID "
							+ "FROM NEWS_AUTHOR WHERE NEWS_AUTHOR.AUTHOR_ID IN "
							+ "(SELECT AUTHOR_ID FROM AUTHOR WHERE AUTHOR.NAME = \'")
					.append(filter.getAuthor().getName())
					.append("\')) ON NEWS.NEWS_ID = FILTER_AUTHOR_NEWS_ID) ");
		}
		if (!filter.getTagList().isEmpty()
				&& !"".equals(filter.getAuthor().getName())) {
			filterGuery
					.append(" (SELECT * FROM NEWS WHERE NEWS_ID IN ("
							+ "SELECT FILTER_AUTHOR_NEWS_ID AS FILTER_AUTHOR_AND_TAG_NEWS_ID FROM "
							+ "(SELECT NEWS_ID AS FILTER_AUTHOR_NEWS_ID "
							+ "FROM NEWS_AUTHOR WHERE NEWS_AUTHOR.AUTHOR_ID IN "
							+ "(SELECT AUTHOR_ID FROM AUTHOR WHERE AUTHOR.NAME = \'")
					.append(filter.getAuthor().getName())
					.append("\')) "
							+ "INNER JOIN\n"
							+ "(SELECT NEWS_ID AS FILTER_TAG_NEWS_ID "
							+ "FROM ( Select NEWS_TAG.NEWS_ID, TAG.TAG_NAME "
							+ "FROM NEWS_TAG INNER JOIN TAG ON NEWS_TAG.TAG_ID = TAG.TAG_ID) "
							+ "WHERE TAG_NAME IN (\'")
					.append(resultTags)
					.append("\'))"
							+ "ON FILTER_AUTHOR_NEWS_ID = FILTER_TAG_NEWS_ID)) ");
		}
		return filterGuery.toString();
	}

	private String join(List<Tag> tags, String conjunction) {
		StringBuilder sb = new StringBuilder();
		boolean first = true;
		for (Tag tag : tags) {
			if (first)
				first = false;
			else
				sb.append(conjunction);
			sb.append(tag.getName());
		}
		return sb.toString();
	}

	@Override
	public List<News> selectNewsForPage(int firstIndex, int secondIndex,
			SearchCriteria filter) {
		List<News> newsList = new ArrayList<>();
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection
					.prepareStatement(""
							+ "SELECT rownum, "
							+ "COUNTOFCOMMENTS, "
							+ "NEWS_ID, "
							+ "SHORT_TEXT, "
							+ "FULL_TEXT, "
							+ "TITLE, "
							+ "CREATION_DATE, "
							+ "MODIFICATION_DATE, "
							+ "VERSION "
							+ "from ( select COUNT(COMM_ID) AS COUNTOFCOMMENTS, "
							+ "NEWS_ID, "
							+ "SHORT_TEXT, "
							+ "FULL_TEXT, "
							+ "TITLE, "
							+ "CREATION_DATE, "
							+ "MODIFICATION_DATE, "
							+ "VERSION "
							+ "from (select COMMENTS.NEWS_ID AS COMM_ID, "
							+ "NEWS.NEWS_ID, "
							+ "NEWS.SHORT_TEXT, "
							+ "NEWS.FULL_TEXT, "
							+ "NEWS.TITLE, "
							+ "NEWS.CREATION_DATE, "
							+ "NEWS.MODIFICATION_DATE, "
							+ "NEWS.VERSION "
							+ " from COMMENTS RIGHT JOIN "
							+ filterQueryMaker(filter)
							+ " NEWS ON COMMENTS.NEWS_ID = NEWS.NEWS_ID) "
							+ "group by NEWS_ID, "
							+ "SHORT_TEXT, "
							+ "FULL_TEXT, "
							+ "TITLE, "
							+ "CREATION_DATE, "
							+ "MODIFICATION_DATE, VERSION  order by 1 desc, 7) "
							+ "group by rownum, "
							+ "COUNTOFCOMMENTS, "
							+ "NEWS_ID, "
							+ "SHORT_TEXT, "
							+ "FULL_TEXT, "
							+ "TITLE, "
							+ "CREATION_DATE, "
							+ "MODIFICATION_DATE, VERSION  HAVING rownum BETWEEN ? AND ? order by 1");
			preparedStatement.setInt(1, firstIndex);
			preparedStatement.setInt(2, secondIndex);
			resultSet = preparedStatement.executeQuery();

			if (resultSet.isBeforeFirst()) {
				while (resultSet.next()) {
					News news = new News();
					news.setId(resultSet.getLong(NEWS_ID_COLUMN));
					news.setShortText(resultSet
							.getString(NEWS_SHORT_TEXT_COLUMN));
					news.setFullText(resultSet.getString(NEWS_FULL_TEXT_COLUMN));
					news.setTitle(resultSet.getString(NEWS_TITLE_COLUMN));
					news.setCreationDate(resultSet
							.getTimestamp(NEWS_CREATION_DATE_COLUMN));
					news.setModificationDate(resultSet
							.getDate(NEWS_MODIFICATION_DATE_COLUMN));
					news.setVersion(resultSet.getLong(NEWS_VERSION_COLUMN));
					newsList.add(news);
				}
				return newsList;
			} else {
				return null;
			}
		} catch (SQLException e) {
			throw new DAOException(
					"Something went wrong in database layer when finding News.",
					e);
		} finally {
			DbUtils.close(resultSet, preparedStatement, connection);
		}
	}

	@Override
	public News next(long newsId) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(FIND_NEXT);

			preparedStatement.setLong(1, newsId);

			resultSet = preparedStatement.executeQuery();

			if (resultSet.isBeforeFirst()) {
				News news = new News();

				while (resultSet.next()) {
					news.setId(resultSet.getLong(NEWS_ID_COLUMN));
					news.setShortText(resultSet
							.getString(NEWS_SHORT_TEXT_COLUMN));
					news.setFullText(resultSet.getString(NEWS_FULL_TEXT_COLUMN));
					news.setTitle(resultSet.getString(NEWS_TITLE_COLUMN));
					news.setCreationDate(resultSet
							.getTimestamp(NEWS_CREATION_DATE_COLUMN));
					news.setModificationDate(resultSet
							.getDate(NEWS_MODIFICATION_DATE_COLUMN));
					news.setVersion(resultSet.getLong(NEWS_VERSION_COLUMN));
				}

				return news;
			} else {
				return null;
			}
		} catch (SQLException e) {
			throw new DAOException(
					"Something went wrong in database layer when finding next News by id.",
					e);
		} finally {
			DbUtils.close(resultSet, preparedStatement, connection);
		}
	}

	@Override
	public News prev(long newsId) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(FIND_PREV);

			preparedStatement.setLong(1, newsId);

			resultSet = preparedStatement.executeQuery();

			if (resultSet.isBeforeFirst()) {
				News news = new News();

				while (resultSet.next()) {
					news.setId(resultSet.getLong(NEWS_ID_COLUMN));
					news.setShortText(resultSet
							.getString(NEWS_SHORT_TEXT_COLUMN));
					news.setFullText(resultSet.getString(NEWS_FULL_TEXT_COLUMN));
					news.setTitle(resultSet.getString(NEWS_TITLE_COLUMN));
					news.setCreationDate(resultSet
							.getTimestamp(NEWS_CREATION_DATE_COLUMN));
					news.setModificationDate(resultSet
							.getDate(NEWS_MODIFICATION_DATE_COLUMN));
					news.setVersion(resultSet.getLong(NEWS_VERSION_COLUMN));
				}

				return news;
			} else {
				return null;
			}
		} catch (SQLException e) {
			throw new DAOException(
					"Something went wrong in database layer when finding prev News by id.",
					e);
		} finally {
			DbUtils.close(resultSet, preparedStatement, connection);
		}
	}
}
