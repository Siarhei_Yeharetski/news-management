package com.epam.newsmanagement.exception;

/**
 * DAOException is the superclass of those exceptions that can be thrown during invocation of any DAO methods.
 *
 * @author Siarhei_Yeharetski
 */
public class DAOException extends RuntimeException {
    /**
	 * 
	 */
	private static final long serialVersionUID = 4038316817459509384L;

	/**
     * @see java.lang.Exception#Exception()
     */
    public DAOException() {
    }

    /**
     * @see java.lang.Exception#Exception(String)
     */
    public DAOException(String message) {
        super(message);
    }

    /**
     * @see java.lang.Exception#Exception(String, Throwable)
     */
    public DAOException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * @see java.lang.Exception#Exception(Throwable)
     */
    public DAOException(Throwable cause) {
        super(cause);
    }
}
