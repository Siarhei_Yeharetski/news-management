package com.epam.newsmanagement.exception;

/**
 * ServiceException is the superclass of those exceptions that can be thrown during invocation of any service layer methods.
 *
 * @author Siarhei_Yeharetski
 */
public class ServiceException extends RuntimeException {
    /**
	 * 
	 */
	private static final long serialVersionUID = -2646974064450599479L;

	/**
     * @see java.lang.Exception#Exception()
     */
    public ServiceException() {
    }

    /**
     * @see java.lang.Exception#Exception(String)
     */
    public ServiceException(String message) {
        super(message);
    }

    /**
     * @see java.lang.Exception#Exception(String, Throwable)
     */
    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * @see java.lang.Exception#Exception(Throwable)
     */
    public ServiceException(Throwable cause) {
        super(cause);
    }
}
