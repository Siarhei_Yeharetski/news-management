package com.epam.newsmanagement.utils;

public final class DbUtils {

	/**
	 * Don't let anyone instantiate this class.
	 */
	private DbUtils() {
	}

	/**
	 * Close all closeables (such as Connection, Statement, PreparedStatement
	 * and so on).
	 * 
	 * @param closeables
	 *            closeable params to be closed.
	 */
	public static void close(AutoCloseable... closeables) {
		for (AutoCloseable autoCloseable : closeables) {
			if (autoCloseable != null) {
				try {
					autoCloseable.close();
				} catch (Exception e) {
					// we can't do anything about it really
				}
			}
		}
	}
}
