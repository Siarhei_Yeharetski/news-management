package com.epam.newsmanagement.utils;

import com.epam.newsmanagement.model.Author;
import com.epam.newsmanagement.model.Comment;
import com.epam.newsmanagement.model.News;
import com.epam.newsmanagement.model.Tag;

/**
 * The class EntityUtils contains methods for checking Entities to be null or empty
 *
 * @author Siarhei_Yeharetski
 * @see com.epam.newsmanagement.model.Entity
 */
public final class EntityUtils {

    /**
     * Don't let anyone instantiate this class.
     */
    private EntityUtils() {
    }

    /**
     * Checks if any of Comment fields (excluding id) are null or empty.
     *
     * @param comment to be checked.
     * @return true, if all requested fields are not null or empty.
     */
    public static boolean checkCommentNotNullOrEmpty(Comment comment) {
        return checkCommentNotNullOrEmptyNoNewsId(comment)
                && comment.getNewsId() != null && comment.getNewsId() > 0;
    }

    /**
     * Checks if any of Comment fields (excluding id and newsId) are null or empty.
     *
     * @param comment to be checked.
     * @return true, if all requested fields are not null or empty.
     */
    public static boolean checkCommentNotNullOrEmptyNoNewsId(Comment comment) {
        return comment.getCommentText() != null && !comment.getCommentText().isEmpty()
                && comment.getCreationDate() != null;
    }

    /**
     * Checks if any of News fields (excluding id) are null or empty.
     * @param news to be checked.
     * @return true, if all requested fields are not null or empty.
     */
    public static boolean checkNewsNotNullOrEmpty(News news) {
        return news.getShortText() != null && !news.getShortText().isEmpty()
                && news.getFullText() != null && !news.getFullText().isEmpty()
                && news.getTitle() != null && !news.getTitle().isEmpty()
                && news.getCreationDate() != null
                && news.getModificationDate() != null;
    }

    /**
     * Checks if any of Author fields (excluding id) are null of empty.
     * @param author to be checked.
     * @return true, if all requested fields are not null or empty.
     */
    public static boolean checkAuthorNotNullOrEmpty(Author author) {
        return author.getName() != null && !author.getName().isEmpty();
    }

    /**
     * Checks if any of Tag fields (excluding id) are null or empty.
     * @param tag to be checked.
     * @return true, if all requested fields are not null or empty.
     */
    public static boolean checkTagNotNullOrEmpty(Tag tag) {
        return tag.getName() != null && !tag.getName().isEmpty();
    }

}
