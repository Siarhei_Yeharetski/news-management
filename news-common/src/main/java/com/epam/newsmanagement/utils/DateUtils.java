package com.epam.newsmanagement.utils;

/**
 * The class DateUtils contains methods for performing basic operations with dates (sql and util) and timestamps.
 *
 * @author Siarhei_Yeharetski
 * @see java.sql.Date
 * @see java.util.Date
 * @see java.sql.Timestamp
 */
public final class DateUtils {

    /**
     * Don't let anyone instantiate this class.
     */
    private DateUtils() {
    }

    /**
     * Converts util date to sql date
     *
     * @param utilDate date to be converted.
     * @return sql date.
     */
    public static java.sql.Date convertUtilDateToSqlDate(java.util.Date utilDate) {
        long timeInMillis = utilDate.getTime();
        return new java.sql.Date(timeInMillis);
    }

    /**
     * Converts  util date to a timestamp.
     * @param utilDate date to be converted.
     * @return timestamp value of a date.
     */
    public static java.sql.Timestamp convertUtilDateToSqlTimestamp(java.util.Date utilDate) {
        long timeInMillis = utilDate.getTime();
        return new java.sql.Timestamp(timeInMillis);
    }
}
