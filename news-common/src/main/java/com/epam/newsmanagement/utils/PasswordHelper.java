package com.epam.newsmanagement.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.inject.Named;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.apache.log4j.Logger;

@Named
public class PasswordHelper implements PasswordEncoder {

	private static final Logger logger = Logger.getLogger(PasswordHelper.class);

	private MessageDigest md;

	public PasswordHelper() {
		try {
			this.md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException ex) {
			logger.error(ex);
		}
	}

	@Override
	public String encode(CharSequence rawPassword) {
		if (md == null) {
			return rawPassword.toString();
		}
		md.update(rawPassword.toString().getBytes());
		byte byteData[] = md.digest();
		StringBuilder hexString = new StringBuilder();
		for (int i = 0; i < byteData.length; i++) {
			String hex = Integer.toHexString(0xff & byteData[i]);
			if (hex.length() == 1) {
				hexString.append('0');
			}
			hexString.append(hex);
		}
		return hexString.toString();
	}

	@Override
	public boolean matches(CharSequence rawPassword, String encodedPassword) {
		return encode(rawPassword).equals(encodedPassword);
	}

}
