package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.model.Author;
import com.epam.newsmanagement.model.News;
import com.epam.newsmanagement.model.Tag;
import com.epam.newsmanagement.model.helper.NewsVO;
import com.epam.newsmanagement.model.helper.NewsFormHelper;
import com.epam.newsmanagement.model.helper.NewsListVO;
import com.epam.newsmanagement.model.helper.SearchCriteria;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.NewsManagementService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;

/**
 * NewsManagementServiceImpl class implements INewsManagementService interface
 * and aggregates some other services to provide complicated service layer.
 *
 * @author Siarhei_Yeharetski
 */
public class NewsManagementServiceImpl implements NewsManagementService {
	private static final Logger LOG = Logger
			.getLogger(NewsManagementServiceImpl.class);

	@Autowired
	private NewsService newsService;
	@Autowired
	private AuthorService authorService;
	@Autowired
	private TagService tagService;
	@Autowired
	private CommentService commentService;

	public NewsManagementServiceImpl() {
	}

	@Override
	public long addFullNewsMessage(NewsFormHelper form) {
		Assert.notNull(form.getNews(), "No news specified.");

		long newsId = newsService.addNews(form.getNews());
		Author author = new Author();
		author.setId(form.getAuthorID());
		form.getNews().setId(newsId);
		authorService.addNewsAuthor(form.getNews(), author);
		if (form.getTagsID() != null) {
			tagService.addNewsTags(form.getNews(), form.getTagsID());
		}
		return newsId;
	}

	@Override
	public NewsListVO getNewsList(int currentIndex, SearchCriteria filter,
			int countNewsOfPage) {
		int countOfNews = newsService.countOfNews(filter);
		List<News> newsList = newsService.getAllNewsForPage(currentIndex
				* countNewsOfPage - 2, currentIndex * countNewsOfPage, filter);
		List<NewsVO> newsVOList = new ArrayList<NewsVO>();
		int iter = 1;
		for (News news : newsList) {
			NewsVO concreteNews = new NewsVO();
			concreteNews.setNews(news);
			concreteNews
					.setAuthor(authorService.getAuthorByNewsId(news.getId()));
			concreteNews.setCommentList(commentService.getCommentsByNewsId(news
					.getId()));
			concreteNews.setTagList(tagService.getTagsByNewsId(news.getId()));
			concreteNews.setNumberOfNews(iter);
			newsVOList.add(concreteNews);
			iter++;
		}
		NewsListVO newsListVO = new NewsListVO(currentIndex, countOfNews,
				newsVOList, filter);
		return newsListVO;
	}

	@Override
	public NewsListVO filter(SearchCriteria filterForm, int countNewsOfPage) {
		List<News> newsList = newsService.getAllNewsForPage(1, countNewsOfPage,
				filterForm);
		int countOfNews = newsService.countOfNews(filterForm);
		List<NewsVO> newsVOList = new ArrayList<NewsVO>();
		int iter = 1;
		for (News news : newsList) {
			NewsVO concreteNews = new NewsVO();
			concreteNews.setNews(news);
			concreteNews
					.setAuthor(authorService.getAuthorByNewsId(news.getId()));
			concreteNews.setCommentList(commentService.getCommentsByNewsId(news
					.getId()));
			concreteNews.setTagList(tagService.getTagsByNewsId(news.getId()));
			concreteNews.setNumberOfNews(iter);
			newsVOList.add(concreteNews);
			iter++;
		}
		NewsListVO newsListVO = new NewsListVO(1, countOfNews, newsVOList,
				filterForm);
		return newsListVO;
	}

	@Override
	public NewsVO getConcreteNews(long id) {
		NewsVO newsVO = new NewsVO();
		newsVO.setNews(newsService.getNews(id));
		newsVO.setAuthor(authorService.getAuthorByNewsId(id));
		newsVO.setCommentList(commentService.getCommentsByNewsId(id));
		newsVO.setTagList(tagService.getTagsByNewsId(id));
		return newsVO;
	}

	@Override
	public NewsVO next(int newsId) {
		News news = newsService.next(newsId);
		if (news == null) {
			return null;
		} else {
			return getConcreteNews(news.getId());
		}
	}

	@Override
	public NewsVO prev(int newsId) {
		News news = newsService.prev(newsId);
		if (news == null) {
			return null;
		} else {
			return getConcreteNews(news.getId());
		}
	}
}
