package com.epam.newsmanagement.service;


import com.epam.newsmanagement.model.News;
import com.epam.newsmanagement.model.Tag;

import java.util.List;

/**
 * ITagService provides service interface layer methods for Tag.
 *
 * @author Siarhei_Yeharetski
 */
public interface TagService {

    /**
     * Adds new Tag.
     *
     * @param tag to be added.
     * @return true, if successfully added.
     */
    boolean addTag(Tag tag);

    /**
     * Adds Tags.
     *
     * @param tagList tags to be added.
     * @return true, if successfully added.
     */
    boolean addTags(List<Tag> tagList);

    /**
     * Edits Tag.
     *
     * @param tag to be edited.
     * @return true, if successfully updated.
     */
    boolean editTag(Tag tag);

    /**
     * Deletes Tag.
     *
     * @param id of the Tag to be deleted.
     * @return true, if successfully deleted.
     */
    boolean deleteTag(long id);

    /**
     * Gets Tag by id.
     *
     * @param id the requested Tag id.
     * @return requested Tag.
     */
    Tag getTag(long id);

    /**
     * Get all Tags.
     *
     * @return list of all Tags.
     */
    List<Tag> getAllTags();

    /**
     * Adds new news tag.
     *
     * @param news news VO.
     * @param tag  author VO.
     * @return true if added.
     */
    boolean addNewsTag(News news, Tag tag);

    /**
     * Adds new news tags.
     *
     * @param news    news VO.
     * @param tagList list of author VO.
     * @return true if added.
     */
    boolean addNewsTags(News news, List<Integer> tagList);
    
    /**
     * Find all Tags by News id.
     * 
     * @param id
     * @return Tag collection.
     */
    List<Tag> getTagsByNewsId(long id);
}
