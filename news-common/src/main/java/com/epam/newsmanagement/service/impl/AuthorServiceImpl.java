package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.model.Author;
import com.epam.newsmanagement.model.News;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.utils.EntityUtils;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import java.util.List;

/**
 * AuthorServiceImpl class implements IAuthorService interface and provides service layer for Author.
 *
 * @author Siarhei_Yeharetski
 */
public class AuthorServiceImpl implements AuthorService {
    private static final Logger LOG = Logger.getLogger(AuthorServiceImpl.class);

    @Autowired
    private AuthorDAO authorDAO;
    
    public AuthorServiceImpl() {
	}

    @Override
    public boolean addAuthor(Author author) {
        Assert.notNull(author, "No author specified.");
        Assert.isTrue(EntityUtils.checkAuthorNotNullOrEmpty(author), "Some of author fields are null or empty!");
        return authorDAO.create(author) > 0;
    }

    @Override
    public boolean editAuthor(Author author) {
        Assert.notNull(author, "No author specified.");
        Assert.isTrue(EntityUtils.checkAuthorNotNullOrEmpty(author), "Some of author fields are null or empty!");
        if (authorDAO.update(author)) {
            return true;
        } else {
            throw new ServiceException("Author " + author.toString() + " was not updated!");
        }
    }

    @Override
    public boolean deleteAuthor(long id) {
        Assert.isTrue(id > 0, "Wrong id!");
        if (authorDAO.delete(id)) {
            return true;
        } else {
            throw new ServiceException("Author with id " + id + " was not deleted!");
        }
    }

    @Override
    public Author getAuthor(long id) {
        Assert.isTrue(id > 0, "Wrong id!");
        Author author = authorDAO.findById(id);
        Assert.notNull(author, "Author with id " + id + " was not found!");
        return author;
    }

    @Override
    public List<Author> getAllAuthors() {
        return authorDAO.findAll();
    }

    @Override
    public boolean addNewsAuthor(News news, Author author) {
        Assert.notNull(news, "No news specified");
        Assert.notNull(author, "No author specified");

        if (getAuthor(author.getId()) == null) {
            addAuthor(author);
        }
        
        authorDAO.deleteNewsAuthor(news.getId());

        return authorDAO.createNewsAuthor(news, author) > 0;
    }

	@Override
	public Author getAuthorByNewsId(long id) {
        Assert.isTrue(id > 0, "Wrong id!");
        Author author = authorDAO.findByNewsId(id);
        //Assert.notNull(author, "Author with  News id " + id + " was not found!");
        return author;
	}

	@Override
	public boolean expireAuthor(Author author) {
		Assert.notNull(author, "No author specified.");
        if (authorDAO.expire(author.getId())) {
            return true;
        } else {
            throw new ServiceException("Author " + author.toString() + " was not expired!");
        }
	}

	@Override
	public List<Author> getAllNotExpiredAuthors() {
		return authorDAO.findAllNotExpired();
	}
}
