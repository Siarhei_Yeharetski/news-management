package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.model.Comment;
import com.epam.newsmanagement.model.News;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.utils.EntityUtils;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import java.util.List;

/**
 * CommentServiceImpl class implements ICommentService interface and provides service layer for Comment.
 *
 * @author Siarhei_Yeharetski
 */
public class CommentServiceImpl implements CommentService {
    private static final Logger LOG = Logger.getLogger(CommentServiceImpl.class);
    
    @Autowired
    private CommentDAO commentDAO;
    
    public CommentServiceImpl() {
	}

    @Override
    public boolean addComment(Comment comment) {
        Assert.notNull(comment, "No Comment specified.");
        Assert.isTrue(EntityUtils.checkCommentNotNullOrEmptyNoNewsId(comment), "Some of comment fields are null or empty!");
        return commentDAO.create(comment) > 0;
    }

    @Override
    public boolean addComments(List<Comment> commentList) {
        Assert.notEmpty(commentList, "Comment list is empty!");
        for (Comment comment : commentList) {
            addComment(comment);
        }
        return true;
    }

    @Override
    public boolean addComment(News news, Comment comment) {
        Assert.notNull(news, "No news specified.");
        Assert.notNull(comment, "No comment specified.");
        Assert.isTrue(news.getId() > 0, "Wrong or null news id.");
        return commentDAO.create(news, comment) > 0;
    }

    @Override
    public boolean addComments(News news, List<Comment> commentList) {
        Assert.notNull(news, "No news specified.");
        Assert.notEmpty(commentList, "Comment list is empty.");
        for (Comment comment : commentList) {
            addComment(news, comment);
        }
        return true;
    }

    @Override
    public boolean editComment(Comment comment) {
        Assert.notNull(comment, "No Comment specified.");
        Assert.isTrue(EntityUtils.checkCommentNotNullOrEmpty(comment), "Some of comment fields are null or empty!");
        if (commentDAO.update(comment)) {
            return true;
        } else {
            throw new ServiceException("Comment " + comment.toString() + " was not updated!");
        }
    }

    @Override
    public boolean deleteComment(long id) {
        Assert.isTrue(id > 0, "Wrong id!");
        if (commentDAO.delete(id)) {
            return true;
        } else {
            throw new ServiceException("Comment with id " + id + " was not deleted!");
        }
    }

    @Override
    public boolean deleteComments(List<Comment> commentList) {
        Assert.notEmpty(commentList, "Comment list is empty!");
        for (Comment comment : commentList) {
            deleteComment(comment.getId());
        }
        return true;
    }

    @Override
    public Comment getComment(long id) {
        Assert.isTrue(id > 0, "Wrong id!");
        Comment comment = commentDAO.findById(id);
        Assert.notNull(comment, "Comment with id " + id + " was not found!");
        return comment;
    }

    @Override
    public List<Comment> getAllComments() {
        return commentDAO.findAll();
    }

	@Override
	public List<Comment> getCommentsByNewsId(long id) {
		Assert.isTrue(id > 0, "Wrong id!");
		return commentDAO.findByNewsId(id);
	}


}
