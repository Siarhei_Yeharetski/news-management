package com.epam.newsmanagement.service;

import com.epam.newsmanagement.model.Comment;
import com.epam.newsmanagement.model.News;

import java.util.List;

/**
 * ICommentService provides service interface layer methods for Comment.
 *
 * @author Siarhei_Yeharetski
 */
public interface CommentService {

    /**
     * Adds new Comment.
     *
     * @param comment to be added.
     * @return true, if successfully added.
     */
    boolean addComment(Comment comment);

    /**
     * Adds Comments.
     * @param commentList comments to be added.
     * @return true, if successfully added.
     */
    boolean addComments(List<Comment> commentList);

    /**
     * Adds new Comment with News link.
     *
     * @param news    news VO.
     * @param comment to be added.
     * @return true, if successfully added.
     */
    boolean addComment(News news, Comment comment);

    /**
     * Adds News Comments list.
     *
     * @param news        news VO.
     * @param commentList comments to be added.
     * @return true, if successfully added.
     */
    boolean addComments(News news, List<Comment> commentList);

    /**
     * Edits Comment.
     *
     * @param comment to be edited.
     * @return true, if successfully updated.
     */
    boolean editComment(Comment comment);

    /**
     * Deletes Comment.
     * @param id of the Comment to be deleted.
     * @return true, if successfully deleted.
     */
    boolean deleteComment(long id);

    /**
     * Deletes Comments.
     * @param commentList list of comments to be deleted.
     * @return true, if successfully deleted.
     */
    boolean deleteComments(List<Comment> commentList);

    /**
     * Gets Comment by id.
     *
     * @param id the requested Comment id.
     * @return requested Comment.
     */
    Comment getComment(long id);

    /**
     * Get all Comments.
     *
     * @return list of all Comments.
     */
    List<Comment> getAllComments();
    
    /**
     * Get Comments by News id.
     * 
     * @param id
     * @return Comment collection.
     */
    List<Comment> getCommentsByNewsId(long id);
}
