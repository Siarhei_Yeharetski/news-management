package com.epam.newsmanagement.service;

import com.epam.newsmanagement.model.helper.NewsVO;
import com.epam.newsmanagement.model.helper.NewsFormHelper;
import com.epam.newsmanagement.model.helper.NewsListVO;
import com.epam.newsmanagement.model.helper.SearchCriteria;
/**
 * INewsManagementService provides complicated service interface layer methods.
 *
 * @author Siarhei_Yeharetski
 */
public interface NewsManagementService {

    /**
     * Adds news message with author and tags.
     *
     * @param news    news message.
     * @param author  news author.
     * @param tagList news tags.
     * @return true, if successfully added.
     */
    long addFullNewsMessage(NewsFormHelper form);
    
    /**
     * 
     * @param currentIndex
     * @param filter
     * @param countNewsOfPage
     * @return
     */
    NewsListVO getNewsList(int currentIndex, SearchCriteria filter, int countNewsOfPage);
    
    /**
     * 
     * @param filterForm
     * @param countNewsOfPage
     * @return
     */
    NewsListVO filter(SearchCriteria filterForm, int countNewsOfPage);
    
    /**
     * 
     * @param id
     * @return
     */
    NewsVO getConcreteNews(long id);
    
    /**
     * 
     * @param currentIndex
     * @param numberOfNews
     * @param filter
     * @param countNewsOfPage
     * @return
     */
    NewsVO next(int newsId);
    
    /**
     * 
     * @param currentIndex
     * @param numberOfNews
     * @param filter
     * @param countNewsOfPage
     * @return
     */
    NewsVO prev(int newsId);
}
