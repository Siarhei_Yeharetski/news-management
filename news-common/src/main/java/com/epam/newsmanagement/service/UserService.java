package com.epam.newsmanagement.service;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.epam.newsmanagement.model.User;

public interface UserService extends UserDetailsService {
	
	/**
	 * Checks if user is in DB.
	 * 
	 * @param login
	 * @return User, if found.
	 */
	User userIsExist(String login);

	/**
	 * Add new User to DB.
	 * 
	 * @param user
	 * @return true, if added.
	 */
	boolean addUser(User user);

	/**
	 * Delete user from DB.
	 * 
	 * @param id
	 * @return true, if deleted.
	 */
	boolean deleteUser(long id);

	/**
	 * Get all Roles for specified User login.
	 * 
	 * @param login
	 * @return String Collection of User Roles.
	 */
	List<String> getRolesOfUser(String login);
	
	@Override
    public UserDetails loadUserByUsername(String string) throws UsernameNotFoundException;
}