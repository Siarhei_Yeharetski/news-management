package com.epam.newsmanagement.service;

import com.epam.newsmanagement.model.Author;
import com.epam.newsmanagement.model.News;

import java.util.List;

/**
 * IAuthorService provides service interface layer methods for Author.
 *
 * @author Siarhei_Yeharetski
 */
public interface AuthorService {

    /**
     * Adds new Author.
     *
     * @param author to be added.
     * @return true, if successfully added.
     */
    boolean addAuthor(Author author);

    /**
     * Edits Author.
     *
     * @param author to be edited.
     * @return true, if successfully updated.
     */
    boolean editAuthor(Author author);

    /**
     * Deletes Author.
     *
     * @param id of the Author to be deleted.
     * @return true, if successfully deleted.
     */
    boolean deleteAuthor(long id);

    /**
     * Gets Author by id.
     *
     * @param id the requested Author id.
     * @return requested Author.
     */
    Author getAuthor(long id);

    /**
     * Get all Authors.
     *
     * @return list of all Authors.
     */
    List<Author> getAllAuthors();

    /**
     * Adds new news author.
     *
     * @param news   news VO.
     * @param author author VO.
     * @return true if added.
     */
    boolean addNewsAuthor(News news, Author author);
    
    /**
     * Find Author by News id.
     * 
     * @param id
     * @return requested Author.
     */
    Author getAuthorByNewsId(long id);
    
    /**
     * Expires Author.
     * 
     * @param author
     * @return true, if expired.
     */
    boolean expireAuthor(Author author);
    
    /**
     * Find all not expired Authors
     * 
     * @return Author collection.
     */
    List<Author> getAllNotExpiredAuthors();
}
