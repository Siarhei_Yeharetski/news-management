package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.model.News;
import com.epam.newsmanagement.model.Tag;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.utils.EntityUtils;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import java.util.List;

/**
 * TagServiceImpl class implements ITagService interface and provides service layer for Tag.
 *
 * @author Siarhei_Yeharetski
 */
public class TagServiceImpl implements TagService {
    private static final Logger LOG = Logger.getLogger(TagServiceImpl.class);
    
    @Autowired
    private TagDAO tagDAO;
    
    public TagServiceImpl() {
	}

    @Override
    public boolean addTag(Tag tag) {
        Assert.notNull(tag, "No tag specified.");
        Assert.isTrue(EntityUtils.checkTagNotNullOrEmpty(tag), "Some of tag fields are null or empty!");
        return tagDAO.create(tag) > 0;
    }

    @Override
    public boolean addTags(List<Tag> tagList) {
        Assert.notEmpty(tagList, "Tag list is empty.");
        for (Tag tag : tagList) {
            addTag(tag);
        }
        return true;
    }

    @Override
    public boolean editTag(Tag tag) {
        Assert.notNull(tag, "No tag specified.");
        Assert.isTrue(EntityUtils.checkTagNotNullOrEmpty(tag), "Some of tag fields are null or empty!");
        if (tagDAO.update(tag)) {
            return true;
        } else {
            throw new ServiceException("Tag " + tag.toString() + " was not updated!");
        }
    }

    @Override
    public boolean deleteTag(long id) {
        Assert.isTrue(id > 0, "Wrong id!");
        if (tagDAO.delete(id)) {
            return true;
        } else {
            throw new ServiceException("Tag with id " + id + " was not deleted!");
        }
    }

    @Override
    public Tag getTag(long id) {
        Assert.isTrue(id > 0, "Wrong id!");
        Tag tag = tagDAO.findById(id);
        Assert.notNull(tag, "Tag with id " + id + " was not found!");
        return tag;
    }

    @Override
    public List<Tag> getAllTags() {
        return tagDAO.findAll();
    }

    @Override
    public boolean addNewsTag(News news, Tag tag) {
        Assert.notNull(news, "No news specified.");
        Assert.notNull(tag, "No tag specified.");

        Assert.isTrue(news.getId() > 0, "Incorrect or null news id!");
        Assert.isTrue(tag.getId() > 0, "Incorrect or null tag id!");

        if (getTag(tag.getId()) == null) {
            addTag(tag);
        }

        return tagDAO.createNewsTag(news, tag) > 0;
    }

    @Override
    public boolean addNewsTags(News news, List<Integer> tagList) {
        Assert.notNull(news, "No news specified.");
        Assert.notEmpty(tagList, "Tag list is empty!");
        
        tagDAO.deleteNewsTag(news.getId());

        for (Integer tagId : tagList) {
        	Tag tag = new Tag();
        	tag.setId(Long.valueOf(tagId));
            addNewsTag(news, tag);
        }

        return true;
    }

	@Override
	public List<Tag> getTagsByNewsId(long id) {
		Assert.isTrue(id > 0, "Wrong id!");
		return tagDAO.findByNewsId(id);
	}
}
