package com.epam.newsmanagement.service;

import com.epam.newsmanagement.model.News;
import com.epam.newsmanagement.model.helper.SearchCriteria;

import java.util.List;

/**
 * INewsService provides service interface layer methods for News.
 *
 * @author Siarhei_Yeharetski
 */
public interface NewsService {

    /**
     * Adds new News.
     *
     * @param news to be added.
     * @return true, if successfully added.
     */
    long addNews(News news);

    /**
     * Edits News.
     * @param news to be edited.
     * @return true, if successfully updated.
     */
    boolean editNews(News news);

    /**
     * Deletes News.
     * @param id of the News to be deleted.
     * @return true, if successfully deleted.
     */
    boolean deleteNews(long id);

    /**
     * Gets News by id.
     * @param id the requested News id.
     * @return requested Author.
     */
    News getNews(long id);

    /**
     * Gets News by SearchCriteria
     * @param searchCriteria containing author or tag list.
     * @return list of news depending on searchCriteria content.
     */
    List<News> getNews(SearchCriteria searchCriteria);
    
    /**
     * Get count of News affected by SearchCriteria.
     * 
     * @param filter
     * @return count of News.
     */
    int countOfNews(SearchCriteria filter);
    
    /**
     * Return all News to fit News page.
     * 
     * @param firstIndex
     * @param secondIndex
     * @param filter
     * @return News collection.
     */
    List<News> getAllNewsForPage(int firstIndex, int secondIndex, SearchCriteria filter);
    
    News next(int newsId);
    
    News prev(int newsId);
}
