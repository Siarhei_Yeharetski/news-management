package com.epam.newsmanagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.util.Assert;

import com.epam.newsmanagement.dao.UserDAO;
import com.epam.newsmanagement.model.User;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.UserService;

public class UserServiceImpl implements UserService {
	private static final Logger LOG = Logger.getLogger(UserServiceImpl.class);

	@Autowired
	private UserDAO userDAO;

	public UserServiceImpl() {
	}

	@Override
	public User userIsExist(String login) {
		Assert.notNull(login, "No login specified");
		return userDAO.userIsExist(login);
	}

	@Override
	public boolean addUser(User user) {
		Assert.notNull(user, "No user specified");
		return userDAO.addUser(user) > 0;
	}

	@Override
	public boolean deleteUser(long id) {
		Assert.isTrue(id > 0, "Wrong id!");
		if (userDAO.deleteUser(id)) {
			return true;
		} else {
			throw new ServiceException("User with id " + id
					+ " was not deleted!");
		}
	}

	@Override
	public List<String> getRolesOfUser(String login) {
		Assert.notNull(login, "No login specified");
		return userDAO.getRolesOfUser(login);
	}

	@Override
	public UserDetails loadUserByUsername(String login)
			throws UsernameNotFoundException {
		User user = userIsExist(login);
		if (user == null) {
			throw new UsernameNotFoundException("User: " + login
					+ " not found!");
		} else {
			List<String> userRoles = this.getRolesOfUser(user.getLogin());
			List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>(
					userRoles.size());
			for (String role : userRoles) {
				authorities.add(new SimpleGrantedAuthority(role));
				return buildUserForAuthentication(user, authorities);
			}
		}
		return null;
	}

	private org.springframework.security.core.userdetails.User buildUserForAuthentication(
			User user, List<GrantedAuthority> authorities) {
		return new org.springframework.security.core.userdetails.User(
				user.getUsername(), user.getPassword(), true, true, true, true,
				authorities);
	}

}