package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.model.News;
import com.epam.newsmanagement.model.helper.SearchCriteria;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.utils.EntityUtils;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import java.util.List;

/**
 * NewsServiceImpl class implements INewsService interface and provides service layer for News.
 *
 * @author Siarhei_Yeharetski
 */
public class NewsServiceImpl implements NewsService {
    private static final Logger LOG = Logger.getLogger(NewsServiceImpl.class);
    
    @Autowired
    private NewsDAO newsDAO;
    
    public NewsServiceImpl() {
	}

    @Override
    public long addNews(News news) {
        Assert.notNull(news, "No news specified.");
        Assert.isTrue(EntityUtils.checkNewsNotNullOrEmpty(news), "Some of news fields are null or empty!");
        return newsDAO.create(news);
    }

    @Override
    public boolean editNews(News news) {
        Assert.notNull(news, "No news specified.");
        if (newsDAO.update(news)) {
            return true;
        } else {
            throw new ServiceException("News " + news.toString() + " was not updated!");
        }
    }

    @Override
    public boolean deleteNews(long id) {
        Assert.isTrue(id > 0, "Wrong id!");
        if (newsDAO.delete(id)) {
            return true;
        } else {
            throw new ServiceException("News with id " + id + " was not deleted!");
        }
    }

    @Override
    public News getNews(long id) {
        Assert.isTrue(id > 0, "Wrong id!");
        News news = newsDAO.findById(id);
        //Assert.notNull(news, "News with id " + id + " was not found!");
        return news;
    }

    @Override
    public List<News> getNews(SearchCriteria searchCriteria) {
        Assert.notNull(searchCriteria, "No search criteria specified.");
        return newsDAO.findByCriteria(searchCriteria);
    }

	@Override
	public int countOfNews(SearchCriteria filter) {
		Assert.notNull(filter, "No search criteria specified.");
		return newsDAO.countOfNews(filter);
	}

	@Override
	public List<News> getAllNewsForPage(int firstIndex, int secondIndex,
			SearchCriteria filter) {
		return newsDAO.selectNewsForPage(firstIndex, secondIndex, filter);
	}

	@Override
	public News next(int newsId) {
		News news = newsDAO.next(newsId);
		if (news == null) {
			return null;
		} else {
			return news;
		}
	}

	@Override
	public News prev(int newsId) {
		News news = newsDAO.prev(newsId);
		if (news == null) {
			return null;
		} else {
			return news;
		}
	}

}
