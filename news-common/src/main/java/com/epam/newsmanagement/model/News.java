package com.epam.newsmanagement.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "NEWS", schema = "NMADMIN")
@SequenceGenerator(name = "NEWS_ID_SEQ", sequenceName = "NEWS_ID_SEQ")
@NamedQueries({
		@NamedQuery(name = "News.findAll", query = "SELECT n FROM News n"),
		@NamedQuery(name = "News.findByShortText", query = "SELECT n FROM News n WHERE n.shortText = :shortText"),
		@NamedQuery(name = "News.findByFullText", query = "SELECT n FROM News n WHERE n.fullText = :fullText"),
		@NamedQuery(name = "News.findByTitle", query = "SELECT n FROM News n WHERE n.title = :title"),
		@NamedQuery(name = "News.findByCreationDate", query = "SELECT n FROM News n WHERE n.creationDate = :creationDate"),
		@NamedQuery(name = "News.findByModificationDate", query = "SELECT n FROM News n WHERE n.modificationDate = :modificationDate"),
		@NamedQuery(name = "News.findById", query = "SELECT n FROM News n WHERE n.id = :id") })
public class News implements Serializable {

	private static final long serialVersionUID = 1601712078200939130L;

	@Id
	@Column(name = "NEWS_ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "NEWS_ID_SEQ")
	private Long id;

	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 100)
	@Column(name = "SHORT_TEXT")
	private String shortText;

	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 2000)
	@Column(name = "FULL_TEXT")
	private String fullText;

	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 30)
	@Column(name = "TITLE")
	private String title;

	@Basic(optional = false)
	@NotNull
	@Column(name = "CREATION_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;

	@Basic(optional = false)
	@NotNull
	@Column(name = "MODIFICATION_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modificationDate;
	
	@Basic(optional = false)
	@NotNull
	@Column(name = "VERSION")
	@Version
	private Long version;

	public News() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getShortText() {
		return shortText;
	}

	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	public String getFullText() {
		return fullText;
	}

	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}
	
	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result
				+ ((fullText == null) ? 0 : fullText.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime
				* result
				+ ((modificationDate == null) ? 0 : modificationDate.hashCode());
		result = prime * result
				+ ((shortText == null) ? 0 : shortText.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		News other = (News) obj;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (fullText == null) {
			if (other.fullText != null)
				return false;
		} else if (!fullText.equals(other.fullText))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (modificationDate == null) {
			if (other.modificationDate != null)
				return false;
		} else if (!modificationDate.equals(other.modificationDate))
			return false;
		if (shortText == null) {
			if (other.shortText != null)
				return false;
		} else if (!shortText.equals(other.shortText))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "News [id=" + id + ", shortText=" + shortText + ", fullText="
				+ fullText + ", title=" + title + ", creationDate="
				+ creationDate + ", modificationDate=" + modificationDate
				+ ", version=" + version + "]";
	}

}
