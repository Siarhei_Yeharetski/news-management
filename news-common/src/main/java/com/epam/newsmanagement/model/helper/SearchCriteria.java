package com.epam.newsmanagement.model.helper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.epam.newsmanagement.model.Author;
import com.epam.newsmanagement.model.Tag;

public class SearchCriteria implements Serializable {
	
	private static final long serialVersionUID = 6091688420798250818L;
	
	private Author author;
    private List<Tag> tagList;

    public SearchCriteria() {
    	this.author = new Author("");
    	tagList = new ArrayList<>();
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public List<Tag> getTagList() {
        return tagList;
    }

    public void setTagList(List<Tag> tagList) {
        this.tagList = tagList;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((author == null) ? 0 : author.hashCode());
		result = prime * result + ((tagList == null) ? 0 : tagList.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SearchCriteria other = (SearchCriteria) obj;
		if (author == null) {
			if (other.author != null)
				return false;
		} else if (!author.equals(other.author))
			return false;
		if (tagList == null) {
			if (other.tagList != null)
				return false;
		} else if (!tagList.equals(other.tagList))
			return false;
		return true;
	}
    
}
