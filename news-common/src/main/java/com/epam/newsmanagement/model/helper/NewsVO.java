package com.epam.newsmanagement.model.helper;

import java.util.List;

import com.epam.newsmanagement.model.Author;
import com.epam.newsmanagement.model.Comment;
import com.epam.newsmanagement.model.News;
import com.epam.newsmanagement.model.Tag;

public class NewsVO {
	
    private News news;
    private Author author;
    private List<Tag> tagList;
    private List<Comment> commentList;
    private int numberOfNews;
    
    public News getNews() {
		return news;
	}
    
    public void setNews(News news) {
		this.news = news;
	}
    
    public Author getAuthor() {
		return author;
	}
    
    public void setAuthor(Author author) {
		this.author = author;
	}
    
    public List<Tag> getTagList() {
		return tagList;
	}
    
    public void setTagList(List<Tag> tagList) {
		this.tagList = tagList;
	}
    
    public List<Comment> getCommentList() {
		return commentList;
	}
    
    public void setCommentList(List<Comment> commentList) {
		this.commentList = commentList;
	}
    
    public int getNumberOfNews() {
		return numberOfNews;
	}
    
    public void setNumberOfNews(int numberOfNews) {
		this.numberOfNews = numberOfNews;
	}

}
