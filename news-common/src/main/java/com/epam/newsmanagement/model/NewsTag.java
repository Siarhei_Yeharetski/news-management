package com.epam.newsmanagement.model;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "NEWS_TAG", schema = "NMADMIN")
@SequenceGenerator(name = "NEWS_TAG_ID_SEQ", sequenceName = "NEWS_TAG_ID_SEQ")
@NamedQueries({
		@NamedQuery(name = "NewsTag.findAll", query = "SELECT n FROM NewsTag n"),
		@NamedQuery(name = "NewsTag.findByNewsId", query = "SELECT n FROM NewsTag n WHERE n.newsId = :newsId"),
		@NamedQuery(name = "NewsTag.findByTagId", query = "SELECT n FROM NewsTag n WHERE n.tagId = :tagId"),
		@NamedQuery(name = "NewsTag.findById", query = "SELECT n FROM NewsTag n WHERE n.id = :id") })
public class NewsTag implements Serializable {

	private static final long serialVersionUID = -5725506970677375626L;

	@Id
	@Column(name = "NEWS_TAG_ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "NEWS_TAG_ID_SEQ")
	private Long id;

	@Basic(optional = false)
	@NotNull
	@Column(name = "NEWS_ID")
	private Long newsId;

	@Basic(optional = false)
	@NotNull
	@Column(name = "TAG_ID")
	private Long tagId;

	public NewsTag() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	public Long getTagId() {
		return tagId;
	}

	public void setTagId(Long tagId) {
		this.tagId = tagId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((newsId == null) ? 0 : newsId.hashCode());
		result = prime * result + ((tagId == null) ? 0 : tagId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NewsTag other = (NewsTag) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (newsId == null) {
			if (other.newsId != null)
				return false;
		} else if (!newsId.equals(other.newsId))
			return false;
		if (tagId == null) {
			if (other.tagId != null)
				return false;
		} else if (!tagId.equals(other.tagId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "NewsTag [id=" + id + ", newsId=" + newsId + ", tagId=" + tagId
				+ "]";
	}

}
