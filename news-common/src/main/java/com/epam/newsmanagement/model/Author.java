package com.epam.newsmanagement.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "AUTHOR", schema = "NMADMIN")
@SequenceGenerator(name = "AUTHOR_ID_SEQ", sequenceName = "AUTHOR_ID_SEQ")
@NamedQueries({
		@NamedQuery(name = "Author.findAll", query = "SELECT a FROM Author a"),
		@NamedQuery(name = "Author.findByName", query = "SELECT a FROM Author a WHERE a.name = :name"),
		@NamedQuery(name = "Author.findByExpirationDate", query = "SELECT a FROM Author a WHERE a.expirationDate = :expirationDate"),
		@NamedQuery(name = "Author.findById", query = "SELECT a FROM Author a WHERE a.id = :id") })
public class Author implements Serializable {
	
	private static final long serialVersionUID = 8751499317187257920L;

	@Id
	@Column(name = "AUTHOR_ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AUTHOR_ID_SEQ")
	private Long id;

	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 30)
	@Column(name = "NAME")
	private String name;

	@Column(name = "EXPIRED", nullable = true)
	@Temporal(TemporalType.DATE)
	private Date expirationDate;

	public Author() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Author(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((expirationDate == null) ? 0 : expirationDate.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Author other = (Author) obj;
		if (expirationDate == null) {
			if (other.expirationDate != null)
				return false;
		} else if (!expirationDate.equals(other.expirationDate))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Author [id=" + id + ", name=" + name + ", expirationDate="
				+ expirationDate + "]";
	}

}
