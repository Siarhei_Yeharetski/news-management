package com.epam.newsmanagement.model.helper;

import java.util.List;

public class NewsListVO {
	
	private SearchCriteria filter;
	private List<NewsVO> newsList;
	private int countOfNews;
	private int currentIndex;
	
	public NewsListVO() {
		
	}

	public NewsListVO(int currentIndex, int countOfNews, List<NewsVO> newsList, SearchCriteria filter) {
		this.currentIndex = currentIndex;
		this.countOfNews = countOfNews;
		this.newsList = newsList;
		this.filter = filter;
	}
	
	public int getCountOfNews() {
		return countOfNews;
	}
	
	public int getCurrentIndex() {
		return currentIndex;
	}
	
	public SearchCriteria getFilter() {
		return filter;
	}
	
	public List<NewsVO> getNewsList() {
		return newsList;
	}
	
	public void setCountOfNews(int countOfNews) {
		this.countOfNews = countOfNews;
	}
	
	public void setCurrentIndex(int currentIndex) {
		this.currentIndex = currentIndex;
	}
	
	public void setFilter(SearchCriteria filter) {
		this.filter = filter;
	}
	
	public void setNewsList(List<NewsVO> newsList) {
		this.newsList = newsList;
	}
}
