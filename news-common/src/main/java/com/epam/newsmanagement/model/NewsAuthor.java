package com.epam.newsmanagement.model;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "NEWS_AUTHOR", schema = "NMADMIN")
@SequenceGenerator(name="NEWS_AUTHOR_ID_SEQ", sequenceName="NEWS_AUTHOR_ID_SEQ")
@NamedQueries({
    @NamedQuery(name = "NewsAuthor.findAll", query = "SELECT n FROM NewsAuthor n"),
    @NamedQuery(name = "NewsAuthor.findByNewsId", query = "SELECT n FROM NewsAuthor n WHERE n.newsId = :newsId"),
    @NamedQuery(name = "NewsAuthor.findByAuthorId", query = "SELECT n FROM NewsAuthor n WHERE n.authorId = :authorId"),
    @NamedQuery(name = "NewsAuthor.findById", query = "SELECT n FROM NewsAuthor n WHERE n.id = :id")})
public class NewsAuthor implements Serializable {
	
	private static final long serialVersionUID = 7972360403337134833L;

	@Id
    @Column(name = "NEWS_AUTHOR_ID")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="NEWS_AUTHOR_ID_SEQ")
    private Long id;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "NEWS_ID")
    private Long newsId;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "AUTHOR_ID")
    private Long authorId;

    public NewsAuthor() {
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((authorId == null) ? 0 : authorId.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((newsId == null) ? 0 : newsId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NewsAuthor other = (NewsAuthor) obj;
		if (authorId == null) {
			if (other.authorId != null)
				return false;
		} else if (!authorId.equals(other.authorId))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (newsId == null) {
			if (other.newsId != null)
				return false;
		} else if (!newsId.equals(other.newsId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "NewsAuthor [id=" + id + ", newsId=" + newsId + ", authorId="
				+ authorId + "]";
	} 
    
}
