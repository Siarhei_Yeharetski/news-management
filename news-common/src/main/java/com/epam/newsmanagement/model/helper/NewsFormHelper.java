package com.epam.newsmanagement.model.helper;

import java.util.List;

import com.epam.newsmanagement.model.News;
import com.epam.newsmanagement.model.Tag;

public class NewsFormHelper {

	private News news;
	private List<Integer> tagsID;
	private Long authorID;
	private String dateForForm;
	private List<Tag> tags;
	
	public NewsFormHelper() {
		this.news = new News();
	}
	
	public Long getAuthorID() {
		return authorID;
	}
	
	public String getDateForForm() {
		return dateForForm;
	}
	
	public News getNews() {
		return news;
	}
	
	public List<Tag> getTags() {
		return tags;
	}
	
	public List<Integer> getTagsID() {
		return tagsID;
	}
	
	public void setAuthorID(Long authorID) {
		this.authorID = authorID;
	}
	
	public void setDateForForm(String dateForForm) {
		this.dateForForm = dateForForm;
	}
	
	public void setNews(News news) {
		this.news = news;
	}
	
	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}
	
	public void setTagsID(List<Integer> tagsID) {
		this.tagsID = tagsID;
	}
}
