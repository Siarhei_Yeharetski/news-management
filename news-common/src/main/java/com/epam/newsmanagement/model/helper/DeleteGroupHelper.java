package com.epam.newsmanagement.model.helper;

public class DeleteGroupHelper {
	private int[] deletedID;
	
	public int[] getDeletedID() {
		return deletedID;
	}
	
	public void setDeletedID(int[] deletedID) {
		this.deletedID = deletedID;
	}

}
