package com.epam.newsmanagement.dao.jdbc;

import com.epam.newsmanagement.dao.jdbc.JdbcAuthorDAO;
import com.epam.newsmanagement.model.Author;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import com.github.springtestdbunit.dataset.ReplacementDataSetLoader;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.util.List;

import static junit.framework.TestCase.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/spring/spring-context-jdbc-test.xml"})
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@DbUnitConfiguration(dataSetLoader = ReplacementDataSetLoader.class)
public class JdbcAuthorDAOTest {

    @Autowired
    private JdbcAuthorDAO authorDAO;

    @Test
    @DatabaseSetup(value = "/dao/author/author-data.xml")
    public void testFindAll() {
        List<Author> authorList = this.authorDAO.findAll();
        assertEquals(10, authorList.size());
    }

    @Test
    @DatabaseSetup(value = "/dao/author/author-data.xml")
    public void testFind() {
        Author author = this.authorDAO.findById(2);
        assertEquals("SecondAuthor", author.getName());
    }

    @Test
    @DatabaseSetup(value = "/dao/author/author-data.xml")
    @ExpectedDatabase(value = "/dao/author/expected-data-delete.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void testDelete() {
        this.authorDAO.delete(10);
        List<Author> authorList = this.authorDAO.findAll();
        assertEquals(9, authorList.size());
    }

    @Test
    @DatabaseSetup(value = "/dao/author/author-data.xml")
    @ExpectedDatabase(value = "/dao/author/expected-data-update.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void testUpdate() {
        Author updatedAuthor = new Author();
        updatedAuthor.setId(1L);
        updatedAuthor.setName("UpdatedName");
        this.authorDAO.update(updatedAuthor);
    }

}
