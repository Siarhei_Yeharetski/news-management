package com.epam.newsmanagement.dao.hibernate;

import static junit.framework.TestCase.assertEquals;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.newsmanagement.model.Author;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import com.github.springtestdbunit.dataset.ReplacementDataSetLoader;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/spring/spring-context-hibernate-test.xml" })
@DbUnitConfiguration(dataSetLoader = ReplacementDataSetLoader.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
public class HibernateAuthorDAOTest {

	@Autowired
	private HibernateAuthorDAO authorDAO;

	@Test
	@DatabaseSetup(value = "/dao/author/author-data.xml")
	public void testFindAll() {
		List<Author> authorList = this.authorDAO.findAll();
		assertEquals(10, authorList.size());
	}

	@Test
	@DatabaseSetup(value = "/dao/author/author-data.xml")
	public void testFind() {
		Author author = this.authorDAO.findById(2);
		assertEquals("SecondAuthor", author.getName());
	}

	@Test
	@DatabaseSetup(value = "/dao/author/author-data.xml")
	@ExpectedDatabase(value = "/dao/author/expected-data-delete.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	public void testDelete() {
		this.authorDAO.delete(10);
		List<Author> authorList = this.authorDAO.findAll();
		assertEquals(9, authorList.size());
	}

	@Test
	@DatabaseSetup(value = "/dao/author/author-data.xml")
	@ExpectedDatabase(value = "/dao/author/expected-data-update.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	public void testUpdate() {
		Author updatedAuthor = new Author();
		updatedAuthor.setId(1L);
		updatedAuthor.setName("UpdatedName");
		this.authorDAO.update(updatedAuthor);
	}
	
	@Test
	@DatabaseSetup(value = "/dao/author/author-data.xml")
	public void testFindByNewsId() {
		Author author = this.authorDAO.findByNewsId(1);
		assertEquals("FirstAuthor", author.getName());
	}
}
