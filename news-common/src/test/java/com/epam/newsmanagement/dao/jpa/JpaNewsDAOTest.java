package com.epam.newsmanagement.dao.jpa;

import static junit.framework.TestCase.assertEquals;

import java.sql.Date;
import java.sql.Timestamp;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.newsmanagement.model.News;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import com.github.springtestdbunit.dataset.ReplacementDataSetLoader;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/spring/spring-context-jpa-test.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@DbUnitConfiguration(dataSetLoader = ReplacementDataSetLoader.class)
public class JpaNewsDAOTest {

	@Autowired
	private JpaNewsDAO newsDAO;
	
	@Test
	@DatabaseSetup(value = "/dao/news/news-data.xml")
	public void testFindById() {
		News news = this.newsDAO.findById(2);
		assertEquals("Title2", news.getTitle());
	}

	@Test
	@DatabaseSetup(value = "/dao/news/news-data.xml")
	@ExpectedDatabase(value = "/dao/news/expected-data-update.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	public void testUpdate() {
		News updatedNews = new News();
		updatedNews.setId(1L);
		updatedNews.setShortText("UpdatedShortText");
		updatedNews.setFullText("UpdatedFullText");
		updatedNews.setCreationDate(Timestamp
				.valueOf("2015-04-19 00:00:00.000000000"));
		updatedNews.setModificationDate(Date.valueOf("2015-04-20"));
		updatedNews.setTitle("UpdatedTitle");
		this.newsDAO.update(updatedNews);
	}
}
