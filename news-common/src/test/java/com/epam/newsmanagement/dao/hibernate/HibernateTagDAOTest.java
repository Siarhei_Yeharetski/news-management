package com.epam.newsmanagement.dao.hibernate;

import static junit.framework.TestCase.assertEquals;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.newsmanagement.dao.hibernate.HibernateTagDAO;
import com.epam.newsmanagement.model.Tag;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/spring/spring-context-hibernate-test.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
public class HibernateTagDAOTest {

	@Autowired
	private HibernateTagDAO tagDAO;

	@Test
	@DatabaseSetup(value = "/dao/tag/tag-data.xml")
	public void testFindAll() {
		List<Tag> tagList = this.tagDAO.findAll();
		assertEquals(10, tagList.size());
	}

	@Test
	@DatabaseSetup(value = "/dao/tag/tag-data.xml")
	public void testFind() {
		Tag tag = this.tagDAO.findById(2);
		assertEquals("SecondTag", tag.getName());
	}

	@Test
	@DatabaseSetup(value = "/dao/tag/tag-data.xml")
	@ExpectedDatabase(value = "/dao/tag/expected-data-delete.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
	public void testDelete() {
		this.tagDAO.delete(10);
	}

	@Test
	@DatabaseSetup(value = "/dao/tag/tag-data.xml")
	@ExpectedDatabase(value = "/dao/tag/expected-data-update.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
	public void testUpdate() {
		Tag updatedTag = new Tag();
		updatedTag.setId(1L);
		updatedTag.setName("UpdatedTag");
		this.tagDAO.update(updatedTag);
	}
	
	@Test
	@DatabaseSetup(value = "/dao/tag/tag-data.xml")
	public void testFindByNewsId() {
		List<Tag> tagList = this.tagDAO.findByNewsId(1);
		assertEquals(3, tagList.size());
	}
}
