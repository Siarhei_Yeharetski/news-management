package com.epam.newsmanagement.dao.jdbc;

import com.epam.newsmanagement.dao.jdbc.JdbcCommentDAO;
import com.epam.newsmanagement.model.Comment;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.sql.Timestamp;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/spring/spring-context-jdbc-test.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
public class JdbcCommentDAOTest {

    @Autowired
    private JdbcCommentDAO commentDAO;

    @Test
    @DatabaseSetup(value = "/dao/comment/comment-data.xml")
    public void testFindAll() {
        List<Comment> commentList = this.commentDAO.findAll();
        assertEquals(10, commentList.size());
    }

    @Test
    @DatabaseSetup(value = "/dao/comment/comment-data.xml")
    public void testFind() {
        Comment comment = this.commentDAO.findById(2);
        assertEquals("CommentText2", comment.getCommentText());
    }

    @Test
    @DatabaseSetup(value = "/dao/comment/comment-data.xml")
    @ExpectedDatabase(value = "/dao/comment/expected-data-delete.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void testDelete() {
        this.commentDAO.delete(10);
        List<Comment> commentList = this.commentDAO.findAll();
        assertEquals(9, commentList.size());
    }

    @Test
    @DatabaseSetup(value = "/dao/comment/comment-data.xml")
    @ExpectedDatabase(value = "/dao/comment/expected-data-update.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void testUpdate() {
        Comment updatedComment = new Comment();
        updatedComment.setId(1L);
        updatedComment.setCommentText("UpdatedCommentText");
        updatedComment.setCreationDate(Timestamp.valueOf("2015-04-18 00:00:00.000000000"));
        updatedComment.setNewsId(2L);
        this.commentDAO.update(updatedComment);
    }
}
