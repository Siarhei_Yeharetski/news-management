package com.epam.newsmanagement.dao.jdbc;

import com.epam.newsmanagement.dao.jdbc.JdbcNewsDAO;
import com.epam.newsmanagement.model.Author;
import com.epam.newsmanagement.model.News;
import com.epam.newsmanagement.model.Tag;
import com.epam.newsmanagement.model.helper.SearchCriteria;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import com.github.springtestdbunit.dataset.ReplacementDataSetLoader;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/spring/spring-context-jdbc-test.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
@DbUnitConfiguration(dataSetLoader = ReplacementDataSetLoader.class)
public class JdbcNewsDAOTest {

    @Autowired
    private JdbcNewsDAO newsDAO;

    @Test
    @DatabaseSetup(value = "/dao/news/news-data.xml")
    public void testFindById() {
        News news = this.newsDAO.findById(2);
        assertEquals("Title2", news.getTitle());
    }

    @Test
    @DatabaseSetup(value = "/dao/news/news-data.xml")
    @ExpectedDatabase(value = "/dao/news/expected-data-update.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void testUpdate() {
        News updatedNews = new News();
        updatedNews.setId(1L);
        updatedNews.setShortText("UpdatedShortText");
        updatedNews.setFullText("UpdatedFullText");
        updatedNews.setCreationDate(Timestamp.valueOf("2015-04-19 00:00:00.000000000"));
        updatedNews.setModificationDate(Date.valueOf("2015-04-20"));
        updatedNews.setTitle("UpdatedTitle");
        this.newsDAO.update(updatedNews);
    }

    @Test
    @DatabaseSetup(value = "/dao/news/news-data.xml")
    public void testFindByAuthor() {
        Author author = new Author();
        author.setName("UpdatedName");
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setAuthor(author);
        List<News> newsList = this.newsDAO.findByCriteria(searchCriteria);
        assertEquals(1, newsList.size());
    }

    @Test
    @DatabaseSetup(value = "/dao/news/news-data.xml")
    public void testFindFullCriteria() {
        Author author = new Author();
        author.setName("UpdatedName");

        Tag tag = new Tag();
        tag.setName("UpdatedTag");
        List<Tag> tagList = new ArrayList<>();
        tagList.add(tag);

        SearchCriteria searchCriteria = new SearchCriteria();

        searchCriteria.setAuthor(author);
        searchCriteria.setTagList(tagList);

        List<News> newsList = this.newsDAO.findByCriteria(searchCriteria);
        assertEquals(1, newsList.size());
    }
}
