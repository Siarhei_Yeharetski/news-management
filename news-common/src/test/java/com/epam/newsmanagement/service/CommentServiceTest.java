package com.epam.newsmanagement.service;

import static org.junit.Assert.assertEquals;

import com.epam.newsmanagement.dao.jdbc.JdbcCommentDAO;
import com.epam.newsmanagement.model.Comment;
import com.epam.newsmanagement.service.impl.CommentServiceImpl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class CommentServiceTest {

	@Mock
	private JdbcCommentDAO commentDAO;
	@InjectMocks
	private CommentServiceImpl commentService;
	private final static ApplicationContext applicationContext = new ClassPathXmlApplicationContext(
			"classpath:/spring/spring-context-jdbc-test.xml");

	@Before
	public void init() {
		commentService = (CommentServiceImpl) applicationContext
				.getBean("commentService");
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testGetCommentById() {
		Comment comment = new Comment();
		comment.setId(1L);
		Mockito.when(commentDAO.findById(1)).thenReturn(comment);
		assertEquals(comment.getId(), this.commentService.getComment(1).getId());
	}

	@Test
	public void testDeleteComment() {
		Mockito.when(commentDAO.delete(1)).thenReturn(true);
		this.commentService.deleteComment(1);
		Mockito.verify(commentDAO, Mockito.times(1)).delete(1);
	}

	@Test
	public void testGetComments() {
		List<Comment> commentList = new ArrayList<>();
		commentList.add(new Comment());
		Mockito.when(commentDAO.findAll()).thenReturn(commentList);
		this.commentService.getAllComments();
		Mockito.verify(commentDAO).findAll();
	}

	@Test(expected = IllegalArgumentException.class)
	public void testUpdateNewsWithException() {
		Comment comment = new Comment();
		this.commentService.editComment(comment);
	}
}
