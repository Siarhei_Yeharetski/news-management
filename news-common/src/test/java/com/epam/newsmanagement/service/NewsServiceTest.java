package com.epam.newsmanagement.service;

import static org.junit.Assert.assertEquals;

import com.epam.newsmanagement.dao.jdbc.JdbcNewsDAO;
import com.epam.newsmanagement.model.News;
import com.epam.newsmanagement.model.helper.SearchCriteria;
import com.epam.newsmanagement.service.impl.NewsServiceImpl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class NewsServiceTest {

	@Mock
	private JdbcNewsDAO newsDAO;
	@InjectMocks
	private NewsServiceImpl newsService;
	private final static ApplicationContext applicationContext = new ClassPathXmlApplicationContext(
			"classpath:/spring/spring-context-jdbc-test.xml");

	@Before
	public void init() {
		newsService = (NewsServiceImpl) applicationContext
				.getBean("newsService");
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testGetNewsById() {
		News news = new News();
		news.setId(1L);
		Mockito.when(newsDAO.findById(1)).thenReturn(news);
		assertEquals(news.getId(), this.newsService.getNews(1).getId());
	}

	@Test
	public void testDeleteNews() {
		Mockito.when(newsDAO.delete(1)).thenReturn(true);
		this.newsService.deleteNews(1);
		Mockito.verify(newsDAO, Mockito.times(1)).delete(1);
	}

	@Test
	public void testGetNews() {
		List<News> newsList = new ArrayList<>();
		newsList.add(new News());
		SearchCriteria searchCriteria = new SearchCriteria();
		Mockito.when(newsDAO.findByCriteria(searchCriteria)).thenReturn(
				newsList);
		this.newsService.getNews(searchCriteria);
		Mockito.verify(newsDAO).findByCriteria(searchCriteria);
	}
}
