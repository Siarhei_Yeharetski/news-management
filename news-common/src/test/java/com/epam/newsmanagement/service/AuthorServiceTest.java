package com.epam.newsmanagement.service;

import static org.junit.Assert.assertEquals;

import com.epam.newsmanagement.dao.jdbc.JdbcAuthorDAO;
import com.epam.newsmanagement.model.Author;
import com.epam.newsmanagement.service.impl.AuthorServiceImpl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class AuthorServiceTest {

	@Mock
	private JdbcAuthorDAO authorDAO;
	@InjectMocks
	private AuthorServiceImpl authorService;
	private final static ApplicationContext applicationContext = new ClassPathXmlApplicationContext(
			"classpath:/spring/spring-context-jdbc-test.xml");

	@Before
	public void init() {
		authorService = (AuthorServiceImpl) applicationContext
				.getBean("authorService");
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testGetAuthorById() {
		Author author = new Author();
		author.setId(1L);
		Mockito.when(authorDAO.findById(1)).thenReturn(author);
		assertEquals(author.getId(), this.authorService.getAuthor(1).getId());
	}

	@Test
	public void testDeleteAuthor() {
		Mockito.when(authorDAO.delete(1)).thenReturn(true);
		this.authorService.deleteAuthor(1);
		Mockito.verify(authorDAO, Mockito.times(1)).delete(1);
	}

	@Test
	public void testGetAuthors() {
		List<Author> authorList = new ArrayList<>();
		authorList.add(new Author());
		Mockito.when(authorDAO.findAll()).thenReturn(authorList);
		this.authorService.getAllAuthors();
		Mockito.verify(authorDAO).findAll();
	}

	@Test(expected = IllegalArgumentException.class)
	public void testUpdateNewsWithException() {
		Author author = new Author();
		this.authorService.editAuthor(author);
	}
}
