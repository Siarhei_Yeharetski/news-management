package com.epam.newsmanagement.service;

import static org.junit.Assert.assertEquals;

import com.epam.newsmanagement.dao.jdbc.JdbcTagDAO;
import com.epam.newsmanagement.model.Tag;
import com.epam.newsmanagement.service.impl.TagServiceImpl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class TagServiceTest {

	@Mock
	private JdbcTagDAO tagDAO;
	@InjectMocks
	private TagServiceImpl tagService;
	private final static ApplicationContext applicationContext = new ClassPathXmlApplicationContext(
			"classpath:/spring/spring-context-jdbc-test.xml");

	@Before
	public void init() {
		tagService = (TagServiceImpl) applicationContext.getBean("tagService");
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testGetTagById() {
		Tag tag = new Tag();
		tag.setId(1L);
		Mockito.when(tagDAO.findById(1)).thenReturn(tag);
		assertEquals(tag.getId(), this.tagService.getTag(1).getId());
	}

	@Test
	public void testDeleteTag() {
		Mockito.when(tagDAO.delete(1)).thenReturn(true);
		this.tagService.deleteTag(1);
		Mockito.verify(tagDAO, Mockito.times(1)).delete(1);
	}

	@Test
	public void testGetTags() {
		List<Tag> tagList = new ArrayList<>();
		tagList.add(new Tag());
		Mockito.when(tagDAO.findAll()).thenReturn(tagList);
		this.tagService.getAllTags();
		Mockito.verify(tagDAO).findAll();
	}

	@Test(expected = IllegalArgumentException.class)
	public void testUpdateNewsWithException() {
		Tag tag = new Tag();
		this.tagService.editTag(tag);
	}
}
