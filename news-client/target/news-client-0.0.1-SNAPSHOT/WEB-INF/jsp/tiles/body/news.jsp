<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div id="viewNews">
	<div id="back">
		<a href="${pageContext.request.contextPath}/newsList/1" style="color: #0000EE"><spring:message code="content.back" /></a>
	</div>
	<div id="content">
		<table class="tableNameNews" cellpadding="5px">
			<tr id="1v">
				<td id="titleNewsView">
					<h3>${newsListPage.newsList[0].news.title}</h3>
				</td>
				<td id="nameAuthorView">
					<h3>(<spring:message code="content.author" />: ${newsListPage.newsList[0].author.name})</h3>
				</td>
				<td id="dateNewsView">
					<h3>
						<spring:message var="dateFormat" code="locale.dateFormat" />
						<fmt:formatDate value="${newsListPage.newsList[0].news.modificationDate}" var="dateString" pattern="${dateFormat}" />
						${dateString}
					</h3>
				</td>
			</tr>
		</table>
		<div id="contentNewsView">
			<h4>${newsListPage.newsList[0].news.fullText}</h4>
		</div>
		<div id="commentsNewsView">
			<div id="commentList">
				<table>
					<tbody>
						<c:forEach var="comment" items="${newsListPage.newsList[0].commentList}">
							<tr>
								<td>
									<h4 class="dateInComments">${comment.creationDate}</h4>
								</td>
							</tr>
							<tr>
								<td bgcolor="aliceblue">
									<h4>${comment.commentText}</h4>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
			<div id="commentForm">
				<form:form
					action="${pageContext.request.contextPath}/news/sendComment/${newsListPage.currentIndex}/${newsListPage.newsList[0].numberOfNews}"
					method="post" commandName="comment"
					onsubmit="return validateAddComment(this)">
					<p>
						<b><spring:message code="content.enterYourComment" /></b>
					</p>
					<p>
						<form:textarea path="commentText" rows="5" cols="30" />
					</p>
					<p>
						<form:errors path="commentText" cssStyle="color: red" />
					</p>
					<p>
						<form:hidden path="newsId"
							value="${newsListPage.newsList[0].news.id}" />
					</p>
					<p>
						<input type="submit" value="<spring:message code="content.send"/>">
					</p>
				</form:form>
			</div>
		</div>
	</div>
	<div id="prevNextButtons">
		<div id="prevButton">
			<a
				href="${pageContext.request.contextPath}/prev/${newsListPage.currentIndex}/${newsListPage.newsList[0].numberOfNews}"
				style="color: #0000EE"> <spring:message code="content.previous" />
			</a>
		</div>
		<div id="nextButton">
			<a
				href="${pageContext.request.contextPath}/next/${newsListPage.currentIndex}/${newsListPage.newsList[0].numberOfNews}"
				style="color: #0000EE"> <spring:message code="content.next" />
			</a>
		</div>
	</div>
</div>
