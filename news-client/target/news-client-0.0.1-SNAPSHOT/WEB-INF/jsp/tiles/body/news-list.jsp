<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<div id="newsList">
	<form:form action="filter" method="post" commandName="filterForm">
		<div id="filter">
			<div class="dropdown1">
				<form:select id="author" path="author" name="one"
					class="dropdown1-select">
					<form:option value="">
						<spring:message code="content.selectAuthor" />
					</form:option>
					<c:forEach var="author" items="${authors}">
						<c:if test="${sessionScope.filter.author.name == author.name}">
							<form:option selected="true" value="${author.name}">${author.name}</form:option>
						</c:if>
						<c:if test="${sessionScope.filter.author.name != author.name}">
							<form:option value="${author.name}">${author.name}</form:option>
						</c:if>
					</c:forEach>
				</form:select>
			</div>
			<dl class="dropdown">
				<dt>
					<a href="#"> <span class="hida"><spring:message
								code="content.selectTags" /></span>
						<p class="multiSel"></p>
					</a>
				</dt>
				<dd>
					<div class="mutliSelect">
						<ul>
							<c:forEach var="tag" items="${tags}">
								<c:if test="${!sessionScope.filter.tagList.isEmpty()}">
									<c:set var="bool" value="${false}" />
									<c:forEach var="currentTag"
										items="${sessionScope.filter.tagList}">
										<c:forEach var="currentTag"
											items="${sessionScope.filter.tagList}">
											<c:if test="${currentTag.name == tagList.name}">
												<c:set var="bool" value="${true}" />
											</c:if>
											<c:if test="${currentTag.name != tag.name}">
											</c:if>
										</c:forEach>
									</c:forEach>
									<c:if test="${bool == true}">
										<li><form:checkbox checked="checked" class="checkbox"
												path="tagList" text="${tag.name}" value="${tag.name}" />${tag.tagName}</li>
									</c:if>
									<c:if test="${bool == false}">
										<li><form:checkbox class="checkbox" path="tagList"
												text="${tag.name}" value="${tag.name}" />${tag.name}</li>
									</c:if>
								</c:if>
								<c:if test="${sessionScope.filter.tagList.isEmpty()}">
									<li><form:checkbox class="checkbox" path="tagList"
											text="${tag.name}" value="${tag.name}" />${tag.name}</li>
								</c:if>
							</c:forEach>
						</ul>
					</div>
				</dd>
				<input id="buttonFilter" type="submit" name="filter" value="<spring:message code="content.filter"/>" />
				<input id="buttonFilter" type="submit" name="reset" value="<spring:message code="content.reset"/>" />						
			</dl>
		</div>
	</form:form>
	<div id="content">
		<c:forEach var="newsEntity" items="${newsListPage.newsList}">
			<div class="newsInf">
				<table class="tableNameNews" cellpadding="5px">
					<tr id="1v">
						<td id="titleNewsView">
							<h3>${newsEntity.news.title}</h3>
						</td>
						<td id="nameAuthorView">
							<h3>
								(<spring:message code="content.author" />: ${newsEntity.author.name})
							</h3>
						</td>
						<td id="dateNewsView">
							<h3>
								<spring:message var="dateFormat" code="locale.dateFormat" />
								<fmt:formatDate value="${newsEntity.news.modificationDate}"
									var="dateString" pattern="${dateFormat}" />
								${dateString}
							</h3>
						</td>
					</tr>
				</table>
				<div id="shortTextNewsView">
					<h4>${newsEntity.news.shortText}</h4>
				</div>
				<div class="commentsTagsAndViewButton">
					<div class="tagList">
						<b style="color: gray"> (<c:forEach var="tag" items="${newsEntity.tagList}">${tag.name}</c:forEach>)
						</b>
					</div>
					<div class="commentsCount">
						<b style="color: red"> <spring:message code="content.comments" />
							(${fn:length(newsEntity.commentList)})
						</b>
					</div>
					<div class="viewButton">
						<a
							href="${pageContext.request.contextPath}/news/${newsEntity.news.id}/${newsListPage.currentIndex}/${newsEntity.numberOfNews}"
							style="color: #0000EE"> <spring:message code="content.view" />
						</a>
					</div>
				</div>
			</div>
		</c:forEach>
	</div>
	<div class="pagination">
		<ul>
			<c:if test="${(newsListPage.countOfNews%3) == 0}">
				<c:set var="countOfIndexes"
					value="${(newsListPage.countOfNews+(3-(newsListPage.countOfNews%3)))/3-1}" />
			</c:if>
			<c:if test="${(newsListPage.countOfNews%3) != 0}">
				<c:set var="countOfIndexes"
					value="${(newsListPage.countOfNews+(3-(newsListPage.countOfNews%3)))/3}" />
			</c:if>
			<c:set var="currentIndex" value="${newsListPage.currentIndex}" />
			<c:choose>
				<c:when test="${currentIndex < 5}">
				</c:when>
				<c:otherwise>
					<li><a href="${pageContext.request.contextPath}/newsList/1">
							<button class="passive">&lt;&lt;</button>
					</a></li>
				</c:otherwise>
			</c:choose>
			<c:choose>
				<c:when test="${countOfIndexes <= 5}">
					<c:forEach var="i" begin="1" end="${countOfIndexes}">
						<c:choose>
							<c:when test="${i == currentIndex}">
								<li><a
									href="${pageContext.request.contextPath}/newsList/${i}">
										<button class="active">
											<c:out value="${i}" />
										</button>
								</a></li>
							</c:when>
							<c:otherwise>
								<li><a
									href="${pageContext.request.contextPath}/newsList/${i}">
										<button class="passive">
											<c:out value="${i}" />
										</button>
								</a></li>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</c:when>
				<c:when test="${countOfIndexes > 5 && currentIndex < 5}">
					<c:forEach var="i" begin="1" end="5">
						<c:choose>
							<c:when test="${i == currentIndex}">
								<li><a
									href="${pageContext.request.contextPath}/newsList/${i}">
										<button class="active">
											<c:out value="${i}" />
										</button>
								</a></li>
							</c:when>
							<c:otherwise>
								<li><a
									href="${pageContext.request.contextPath}/newsList/${i}">
										<button class="passive">
											<c:out value="${i}" />
										</button>
								</a></li>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</c:when>
				<c:when test="${(countOfIndexes - currentIndex) < 3}">
					<c:forEach var="i" begin="${countOfIndexes-4}"
						end="${countOfIndexes}">
						<c:choose>
							<c:when test="${i == currentIndex}">
								<li><a
									href="${pageContext.request.contextPath}/newsList/${i}">
										<button class="active">
											<c:out value="${i}" />
										</button>
								</a></li>
							</c:when>
							<c:otherwise>
								<li><a
									href="${pageContext.request.contextPath}/newsList/${i}">
										<button class="passive">
											<c:out value="${i}" />
										</button>
								</a></li>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</c:when>
				<c:otherwise>
					<li><a
						href="${pageContext.request.contextPath}/newsList/${currentIndex-2}">
							<button class="passive">
								<c:out value="${currentIndex-2}" />
							</button>
					</a></li>
					<li><a
						href="${pageContext.request.contextPath}/newsList/${currentIndex-1}">
							<button class="passive">
								<c:out value="${currentIndex-1}" />
							</button>
					</a></li>
					<li><a
						href="${pageContext.request.contextPath}/newsList/${currentIndex}">
							<button class="active">
								<c:out value="${currentIndex}" />
							</button>
					</a></li>
					<li><a
						href="${pageContext.request.contextPath}/newsList/${currentIndex+1}">
							<button class="passive">
								<c:out value="${currentIndex+1}" />
							</button>
					</a></li>
					<li><a
						href="${pageContext.request.contextPath}/newsList/${currentIndex+2}">
							<button class="passive">
								<c:out value="${currentIndex+2}" />
							</button>
					</a></li>
				</c:otherwise>
			</c:choose>
			<c:choose>
				<c:when test="${(countOfIndexes - currentIndex) <= 3}">
				</c:when>
				<c:otherwise>
					<li><a
						href="${pageContext.request.contextPath}/newsList/${countOfIndexes}">
							<button class="passive">&gt;&gt;</button>
					</a></li>
				</c:otherwise>
			</c:choose>
		</ul>
	</div>
</div>