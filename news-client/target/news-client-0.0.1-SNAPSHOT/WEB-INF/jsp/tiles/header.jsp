<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<h1>
    <spring:message code="header.name"/>
</h1>
<p id="language">
    <a href="?locale=en">
        <spring:message code="header.language.en"/>
    </a>
    <a href="?locale=ru">
        <spring:message code="header.language.ru"/>
    </a>
</p>