function validateAddComment(form)
{
    var el,
    elName,
    value,
    type;
    for (var i = 0; i < form.elements.length; i++) {
        el = form.elements[i];
        elName = el.nodeName.toLowerCase();
        value = el.value;
        if (elName === "textarea") {
        	if (el.name === "commentText") {
                if(value == ""){
                    alert(errorText[1]);
                    el.focus();
                    return false;
                }
                if(value.length > 100){
                    alert(errorText[2]);
                    el.focus();
                    return false;
                }
            }
        }
    }
    return true;
}