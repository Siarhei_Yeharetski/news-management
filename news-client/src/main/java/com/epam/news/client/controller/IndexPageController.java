package com.epam.news.client.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.epam.newsmanagement.model.helper.SearchCriteria;

@Controller
public class IndexPageController {
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String indexPage(ModelAndView model, HttpServletRequest request) {
		request.getSession().setAttribute("filter", new SearchCriteria());
		return "redirect:newsList/1";
	}
}
