package com.epam.news.client.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.epam.news.client.validator.CommentValidator;
import com.epam.newsmanagement.model.Comment;
import com.epam.newsmanagement.model.News;
import com.epam.newsmanagement.model.helper.NewsVO;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.NewsManagementService;

@Controller
public class NewsPageController {

	@Autowired
	private CommentValidator validator;
	@Autowired
	private NewsManagementService newsManagementService;
	@Autowired
	private CommentService commentService;

	@RequestMapping(value = "/news/{newsId}", method = RequestMethod.GET)
	public String getConcreteNews(ModelMap model, HttpServletRequest request,
			@PathVariable("newsId") int newsId) {
		NewsVO newsVO = newsManagementService.getConcreteNews(newsId);
		Comment comment = new Comment();
		model.put("newsVO", newsVO);
		model.put("comment", comment);
		return "news";
	}

	@RequestMapping(value = "/next/{newsId}", method = RequestMethod.GET)
	public String next(ModelMap model,
			@PathVariable("newsId") int newsId,
			HttpServletRequest request) {
		NewsVO newsVO;
		if (newsManagementService.next(newsId) == null) {
			newsVO = newsManagementService.getConcreteNews(newsId);
			Comment comment = new Comment();
			model.put("newsVO", newsVO);
			model.put("comment", comment);
			return "news";
		} else {
			newsVO = newsManagementService.next(newsId);
			Comment comment = new Comment();
			model.put("newsVO", newsVO);
			model.put("comment", comment);
			return "news";
		}
	}

	@RequestMapping(value = "/prev/{newsId}", method = RequestMethod.GET)
	public String prev(ModelMap model,
			@PathVariable("newsId") int newsId,
			HttpServletRequest request) {
		NewsVO newsVO;
		if (newsManagementService.prev(newsId) == null) {
			newsVO = newsManagementService.getConcreteNews(newsId);
			Comment comment = new Comment();
			model.put("newsVO", newsVO);
			model.put("comment", comment);
			return "news";
		} else {
			newsVO = newsManagementService.prev(newsId);
			Comment comment = new Comment();
			model.put("newsVO", newsVO);
			model.put("comment", comment);
			return "news";
		}
	}

	@RequestMapping(value = "/news/sendComment", method = RequestMethod.POST)
	public String sendComment(HttpServletRequest request,
			ModelMap model, Comment comment, BindingResult result) {

		NewsVO newsVO = newsManagementService.getConcreteNews(comment
				.getNewsId());
		validator.validate(comment, result);
		if (result.hasErrors()) {
			model.put("newsVO", newsVO);
			return "news";
		} else {
			News news = new News();
			news.setId(comment.getNewsId());
			comment.setCreationDate(new Date(System.currentTimeMillis()));
			commentService.addComment(news, comment);
			model.put("newsVO", newsVO);
			return "redirect:/news/" + comment.getNewsId();
		}
	}

}
