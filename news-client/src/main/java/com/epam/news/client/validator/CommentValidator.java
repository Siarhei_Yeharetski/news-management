package com.epam.news.client.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.epam.newsmanagement.model.Comment;

@Component
public class CommentValidator implements Validator {
	
	private static final int MAX_COMMENT_LENGTH = 100;

	@Override
	public boolean supports(Class<?> clazz) {
		return Comment.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
        Comment comment = (Comment) target;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "commentText", "label.validate.commentIsEmply");
        String commentText = comment.getCommentText();
        if ((commentText.length()) > MAX_COMMENT_LENGTH) {
            errors.rejectValue("commentText", "label.validate.commentLarge");
        }
	}

}
