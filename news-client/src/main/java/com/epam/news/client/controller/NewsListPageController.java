package com.epam.news.client.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.epam.news.client.util.TagEditor;
import com.epam.newsmanagement.model.Author;
import com.epam.newsmanagement.model.Tag;
import com.epam.newsmanagement.model.helper.NewsListVO;
import com.epam.newsmanagement.model.helper.SearchCriteria;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.NewsManagementService;
import com.epam.newsmanagement.service.TagService;

@Controller
public class NewsListPageController {
	@Autowired
	private AuthorService authorService;
	@Autowired
	private TagService tagService;
	@Autowired
	private NewsManagementService newsManagementService;

	private static final int COUNT_OF_NEWS_ON_PAGE = 3;
	
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Tag.class, new TagEditor());
    }

	@RequestMapping(value = "/newsList/{pageIndex}", method = RequestMethod.GET)
	public String getNewsList(ModelMap model, HttpServletRequest request,
			@PathVariable("pageIndex") int pageIndex) {

		SearchCriteria filter = (SearchCriteria) request.getSession()
				.getAttribute("filter");
		List<Author> authors = this.authorService.getAllNotExpiredAuthors();
		List<Tag> tags = this.tagService.getAllTags();
		NewsListVO newsListPage = newsManagementService.getNewsList(pageIndex,
				filter, COUNT_OF_NEWS_ON_PAGE);
		fillModel(model, tags, authors, newsListPage);
		return "newsList";
	}

	@RequestMapping(value = "/newsList/filter", params = "filter", method = RequestMethod.POST)
	public String filter(ModelMap model, SearchCriteria filterForm,
			HttpServletRequest request) {
		request.getSession().setAttribute("filter", filterForm);
		List<Tag> tags = tagService.getAllTags();
		List<Author> authors = authorService.getAllNotExpiredAuthors();
		NewsListVO newsListPage = newsManagementService.filter(filterForm,
				COUNT_OF_NEWS_ON_PAGE);
		fillModel(model, tags, authors, newsListPage);
		return "redirect:/newsList/1";
	}
	
	@RequestMapping(value = "/newsList/filter", params = "reset", method = RequestMethod.POST)
	public String reset(ModelMap model, SearchCriteria filterForm,
			HttpServletRequest request) {
		request.getSession().setAttribute("filter", new SearchCriteria());
		List<Tag> tags = tagService.getAllTags();
		List<Author> authors = authorService.getAllNotExpiredAuthors();
		NewsListVO newsListPage = newsManagementService.filter(filterForm,
				COUNT_OF_NEWS_ON_PAGE);
		fillModel(model, tags, authors, newsListPage);
		return "redirect:/newsList/1";
	}
	
	private void fillModel(ModelMap model, List<Tag> tags,
			List<Author> authors, NewsListVO newsListPage) {
		SearchCriteria filterForm = new SearchCriteria();
		model.put("newsListPage", newsListPage);
		model.put("tags", tags);
		model.put("authors", authors);
		model.put("filterForm", filterForm);
	}
}
