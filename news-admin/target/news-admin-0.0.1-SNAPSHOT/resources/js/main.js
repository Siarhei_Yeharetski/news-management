/*
	Dropdown with Multiple checkbox select with jQuery - May 27, 2013
	(c) 2013 @ElmahdiMahmoud
	license: http://www.opensource.org/licenses/mit-license.php
*/ 

/* global errorText, formatDate, groupDel, confMessage, $colors */

$(".dropdown dt a").on('click', function () {
          $(".dropdown dd ul").slideToggle('fast');
      });

      $(".dropdown dd ul li a").on('click', function () {
          $(".dropdown dd ul").hide();
      });

      function getSelectedValue(id) {
           return $("#" + id).find("dt a span.value").html();
      }

      $(document).bind('click', function (e) {
          var $clicked = $(e.target);
          if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
      });


      $('.mutliSelect input[type="checkbox"]').on('click', function () {
        
          var title = $(this).attr('text') + ", ";
              
        
          if ($(this).is(':checked')) {
              var html = '<span title="' + title + '">' + title + '</span>';
              $('.multiSel').append(html);
              $(".hida").hide();
          } 
          else {
              $('span[title="' + title + '"]').remove();
              var ret = $(".hida");
              $('.dropdown dt a').append(ret);
              
          }
      });
      
      
      var colors = ['1abc9c', '2c3e50', '2980b9', '7f8c8d', 'f1c40f', 'd35400', '27ae60'];

colors.forEach(function (color) {
  $('.color-picker')[0].insert(
    '<div class="square" style="background: #' + color + '"></div>'
  );
});

$('.color-picker')[0].on('click', '.square', function(event, square) {
  background = square.getStyle('background');
  $('.custom-dropdown select').each(function (dropdown) {
    dropdown.setStyle({'background' : background});
  });
});



function confirmDelete(path) {
    if(confirm(confMessage)) {
        if(path !== null)
            location.replace(path);
        return true;
    } else {
        return false;
    }
}

function validateGroupDelete(form) {
    var checkbox;
    for(var i=0; i<form.elements.length; i++) {
        checkbox = form.elements[i];
        if(checkbox.checked) {
            return confirmDelete(null);
        }
    }
    alert(groupDel);
    return false;
}

function isInteger(s){
    var i;
    for (i = 0; i < s.length; i++){
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag){
    var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++){
        var c = s.charAt(i);
        if (bag.indexOf(c) === -1) returnString += c;
    }
    return returnString;
}

function daysInFebruary (year){
    // February has 29 days in any year evenly divisible by four,
    // EXCEPT for centurial years which are not also divisible by 400.
    return (((year % 4 === 0) && ( (!(year % 100 === 0)) || (year % 400 === 0))) ? 29 : 28 );
}
function DaysArray(n) {
    for (var i = 1; i <= n; i++) {
        this[i] = 31;
        if (i===4 || i===6 || i===9 || i===11) {
            this[i] = 30;
        }
        if (i===2) {
            this[i] = 29;
        }
    }
    return this;
}
   
function isDate(dtStr){
    var dtCh;
    var minYear=1900;
    var maxYear=2015;
    var daysInMonth = DaysArray(12);
    
    var strMonth, strDay, strYear;
    if("MM/dd/yyyy" === formatDate) {
        dtCh="/";
        var pos1=dtStr.indexOf(dtCh);
        var pos2=dtStr.indexOf(dtCh,pos1+1);
        strMonth=dtStr.substring(0,pos1);
        strDay=dtStr.substring(pos1+1,pos2);
        strYear=dtStr.substring(pos2+1);
    } else {
        dtCh=".";
        var pos1=dtStr.indexOf(dtCh);
        var pos2=dtStr.indexOf(dtCh,pos1+1);
        strDay=dtStr.substring(0,pos1);
        strMonth=dtStr.substring(pos1+1,pos2);
        strYear=dtStr.substring(pos2+1);
    }
    strYr=strYear;
    if (strDay.charAt(0)==="0" && strDay.length>1) strDay=strDay.substring(1);
    if (strMonth.charAt(0)==="0" && strMonth.length>1) strMonth=strMonth.substring(1);
    for (var i = 1; i <= 3; i++) {
        if (strYr.charAt(0)==="0" && strYr.length>1) strYr=strYr.substring(1);
    }
    month=parseInt(strMonth);
    day=parseInt(strDay);
    year=parseInt(strYr);
    if (pos1===-1 || pos2===-1){
        return errorText[5];
    }
    if (strMonth.length<1 || month<1 || month>12){
        return errorText[8];
    }
    if (strDay.length<1 || day<1 || day>31 || (month===2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
        return errorText[9];
    }
    if (strYear.length !== 4 || year===0 || year<minYear || year>maxYear){
        return errorText[10]+minYear+ errorText[11] + maxYear;
    }
    if (dtStr.indexOf(dtCh,pos2+1)!==-1 || isInteger(stripCharsInBag(dtStr, dtCh))===false){
        return errorText[12];
    }
    return "";
}




function validateAddEditNews(form)
{
    var el,
    elName,
    value,
    type,
    message = "",
    flag = true;
    for (var i = 0; i < form.elements.length; i++) {
        el = form.elements[i];
        elName = el.nodeName.toLowerCase();
        value = el.value;
        
        if (elName === "input") { // INPUT
            type = el.type.toLowerCase();
            switch (type) {
                case "text" :
                    if (el.name === "news.title") {
                        if(value === "") {
                            flag = false;
                            message = message.concat(errorText[1]+"\n");
                        } 
                        if(value.length > 30) {
                            flag = false;
                            message = message.concat(errorText[14]+"\n");
                        }
                    }

                    if (el.name === "dateForForm")
                    {                        
                        if (isDate(value,formatDate)!==""){
                            flag = false;
                            message = message.concat(isDate(value,formatDate)+"\n");
                        }
                    }
                    break;
                default :
                    // type = hidden, submit, button, image
                    break;
            }
        } else if (elName === "textarea") {
            if (el.name === "news.shortText") {
                if(value === ""){
                    flag = false;
                    message = message.concat(errorText[3]+"\n");
                }
                if(value.length > 100) {
                    flag = false;
                    message = message.concat(errorText[6]+"\n");
                }
            }
            if (el.name === "news.fullText") {
                if(value === ""){
                    flag = false;
                    message = message.concat(errorText[4]+"\n");
                }

                if(value.length > 2000){
                    flag = false;
                    message = message.concat(errorText[7]+"\n");
                }
            }
            if (el.name === "commentText") {
                if(value == ""){
                    flag = false;
                    message = message.concat(errorText[16]+"\n");
                }

                if(value.length > 100){
                    flag = false;
                    message = message.concat(errorText[17]+"\n");
                }
            }
        } else if(elName === "select"){
            if (el.name === "authorID") {
                if(value == ""){
                    flag = false;
                    message = message.concat(errorText[15]+"\n");
                }
            }
        }
    }
    if(!flag){
        alert(message);
    }
    return flag;

}