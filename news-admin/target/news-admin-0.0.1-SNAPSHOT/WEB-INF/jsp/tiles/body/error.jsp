<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<div id="errorMessage">

	<h1>
		<spring:message code="content.error" />
	</h1>

	<div id="back">
		<a href="${pageContext.request.contextPath}"
			style="color: #0000EE"><spring:message code="content.back" /></a>
	</div>

	<!--
    Failed URL: ${url}
    Exception:  ${exception.message}
        <c:forEach items="${exception.stackTrace}" var="ste">    ${ste} 
    </c:forEach>
    -->

</div>