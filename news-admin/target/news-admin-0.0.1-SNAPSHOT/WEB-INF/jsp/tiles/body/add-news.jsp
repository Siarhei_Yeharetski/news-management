<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div id="addNewsDiv">
    <form:form action="createNews" method="post" commandName="form" onsubmit="return validateAddEditNews(this)">
    <table id="table_edit" >
        <tr class="trAdd">
            <td width="100px" valign="top" class="tdAdd">
                <spring:message code="content.title"/>
            </td>
            <td class="tdAdd">
                <form:input path="news.title" size="80"/>
            </td>
        </tr>
        <tr class="trAdd">
            <td  valign="top" class="tdAdd">
                <p><spring:message code="content.date"/></p>
            </td>
            <td class="tdAdd">
                <spring:message var="dateFormat" code="locale.dateFormat"/>
                <fmt:formatDate value="${form.news.creationDate}" var="dateString" pattern="${dateFormat}"/>
                <form:input type="text" value="${dateString}" path="dateForForm" size="20"/>
            </td>
        </tr>
        <tr class="trAdd">
            <td  valign="top" class="tdAdd">
                <p><spring:message code="content.shortText"/></p>
            </td>
            <td class="tdAdd">
                <form:textarea path="news.shortText" cols="50" rows="5"/>
            </td>
        </tr>
        <tr class="trAdd">
            <td valign="top" class="tdAdd">
                <p><spring:message code="content.fullText"/></p>
            </td>
            <td class="tdAdd">
                <form:textarea path="news.fullText" cols="50" rows="5"/>
            </td>
        </tr>
    </table>
    <div id="filter">
        <div class="dropdown1">
            <form:select path="authorID" name="one" class="dropdown1-select">
                <form:option value=""><spring:message code="content.selectAuthor"/></form:option>
                <c:forEach var="authors" items="${authorList}">
                    <form:option value="${authors.id}">${authors.name}</form:option>
                </c:forEach>
            </form:select>
        </div>
        <dl class="dropdown"> 
            <dt>
                <a href="#">
                    <span class="hida"><spring:message code="content.selectTags"/></span>    
                    <p class="multiSel"></p>  
                </a>
            </dt>
            <dd>
                <div class="mutliSelect">
                    <ul>
                        <c:forEach var="tag" items="${form.tags}">
                            <li><form:checkbox path="tagsID" text="${tag.name}" value="${tag.id}"/>${tag.name}</li>
                        </c:forEach>
                    </ul>
                </div>
            </dd>
        </dl>
    </div>
    <p id="save_cancel">
        <input type="submit" value="<spring:message code="menu.addNews"/>">
    </p>
    <div class="clear"></div>
    </form:form>
</div>