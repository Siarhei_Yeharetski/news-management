<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div class="addUpdateDiv">
    <table>
        <c:forEach var="tagListItem" items="${tags}">
            <form:form action="${pageContext.request.contextPath}/updateTag" method="post" commandName="tag">
                <tr>
                    <td>
                        <spring:message code="content.tagt"/>
                    </td>
                    <td>
                        <c:choose>
                            <c:when test="${activeTagId == tagListItem.id}">
                                <form:input path="name" value="${tagListItem.name}" size="40" disabled="false"/>
                            </c:when>
                            <c:otherwise>
                                <form:input path="name" value="${tagListItem.name}" size="40" disabled="true"/>
                            </c:otherwise>
                        </c:choose>
                        <form:hidden path="id" value="${tagListItem.id}"/>
                    </td>
                    <td>
                        <c:choose>
                            <c:when test="${activeTagId == tagListItem.id}">
                                <input type="submit" value="<spring:message code="content.update"/>">
                                <input type="submit" value="<spring:message code="content.delete"/>" onclick="form.action = '${pageContext.request.contextPath}/deleteTag';">
                                <input type="submit" value="<spring:message code="content.cancel"/>" onclick="history.go(-1);">
                            </c:when>
                            <c:otherwise>
                                <a class="editClassButton" href="${pageContext.request.contextPath}/editTag/${tagListItem.id}">
                                    <spring:message code="content.edit"/>
                                </a>
                            </c:otherwise>
                        </c:choose>
                    </td>
                </tr>
            </form:form>
        </c:forEach>
    </table>
    <form:form action="addTag" method="post" commandName="tag">
        <table>
            <tr>
                <td>
                    <spring:message code="content.tagt"/>
                </td>
                <td>
                    <form:input path="name" size="40"/>
                </td>
                <td>
                    <input type="submit" value="<spring:message code="content.send"/>">
                </td>
            </tr>
        </table>
    </form:form>           
        
</div>
