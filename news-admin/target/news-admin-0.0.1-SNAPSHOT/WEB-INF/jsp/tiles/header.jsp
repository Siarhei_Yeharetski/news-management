<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div id="nameSite">
	<h1>
		<spring:message code="header.name" />
	</h1>
</div>
<div id="funcSite">
	<c:url value="/Logout" var="logoutUrl" />
	<form action="${logoutUrl}" method="post" id="logoutForm">
		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />
	</form>
	<script>
		function formSubmit() {
			document.getElementById("logoutForm").submit();
		}
	</script>

	<c:if test="${pageContext.request.userPrincipal.name != null}">
		<h2>
			<spring:message code="header.welcome" />: ${pageContext.request.userPrincipal.name} | <a href="javascript:formSubmit()"> <spring:message code="header.logout" /></a>
		</h2>
	</c:if>
	<p id="language">
		<a href="?locale=en"> <spring:message code="header.language.en" />
		</a> <a href="?locale=ru"> <spring:message code="header.language.ru" />
		</a>
	</p>
</div>