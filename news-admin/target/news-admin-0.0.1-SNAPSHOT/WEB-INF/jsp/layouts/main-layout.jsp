<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>
<head>
<title><spring:message code="header.name" /></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery-1.11.2.min.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/filter.js" />"></script>
<script type="text/javascript">
            var errorText = { <spring:message code="vars.add-edit"/> } ;
            var formatDate = "<spring:message code="locale.dateFormat"/>";
            var groupDel = "<spring:message code="vars.groupDel"/>";
            var confMessage = "<spring:message code="vars.confMessage"/>";
        </script>
</head>
<body>
	<div id="wrapper">
		<div id="header">
			<tiles:insertAttribute name="header" />
		</div>
		<div id="menu">
			<tiles:insertAttribute name="menu" />
		</div>
		<div id="body">
			<tiles:insertAttribute name="body" />
		</div>
		<div id="footer">
			<tiles:insertAttribute name="footer" />
		</div>
	</div>
	<script type="text/javascript"
		src="<c:url value="/resources/js/main.js" />"></script>

</body>
</html>
