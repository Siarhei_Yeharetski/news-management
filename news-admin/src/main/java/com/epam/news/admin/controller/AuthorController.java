package com.epam.news.admin.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.epam.news.admin.validator.AuthorValidator;
import com.epam.newsmanagement.model.Author;
import com.epam.newsmanagement.service.AuthorService;

@Controller
public class AuthorController {

	@Autowired
	private AuthorValidator validator;
	@Autowired
	private AuthorService authorService;

	@RequestMapping(value = "/editAuthor/{authorId}", method = RequestMethod.GET)
	public String editAuthor(ModelMap model,
			@PathVariable("authorId") int authorId) {
		fillModel(model, authorId);
		return "addUpdateAuthors";
	}

	@RequestMapping(value = "/updateAuthor", method = RequestMethod.POST)
	public String updateAuthor(ModelMap model, Author author) {
		if (author.getName().length() == 0) {
			authorService.deleteAuthor(author.getId());
		} else {
			authorService.editAuthor(author);
		}
		fillModel(model, 0);
		return "addUpdateAuthors";
	}

	@RequestMapping(value = "/addAuthor", method = RequestMethod.POST)
	public String addAuthor(HttpServletRequest request, ModelMap model,
			Author author) {
		authorService.addAuthor(author);
		fillModel(model, 0);
		return "addUpdateAuthors";
	}

	@RequestMapping(value = "/expireAuthor", method = RequestMethod.POST)
	public String expireAuthor(ModelMap model, Author author) {
		authorService.expireAuthor(author);
		fillModel(model, 0);
		return "addUpdateAuthors";
	}

	private void fillModel(ModelMap model, int activeAuthor) {
		Author author = new Author();
		List<Author> authors = authorService.getAllAuthors();
		model.put("activeAuthorId", activeAuthor);
		model.put("authors", authors);
		model.put("author", author);
	}
}
