package com.epam.news.admin.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.epam.news.admin.validator.TagValidator;
import com.epam.newsmanagement.model.Tag;
import com.epam.newsmanagement.service.TagService;

@Controller
public class TagController {

	@Autowired
	private TagValidator validator;
	@Autowired
	private TagService tagService;

	@RequestMapping(value = "/editTag/{tagId}", method = RequestMethod.GET)
	public String editTag(ModelMap model, @PathVariable("tagId") int tagId) {
		fillModel(model, tagId);
		return "addUpdateTags";
	}

	@RequestMapping(value = "/updateTag", method = RequestMethod.POST)
	public String updateTag(ModelMap model, Tag tag) {
		if (tag.getName().length() == 0) {
			tagService.deleteTag(tag.getId());
		} else {
			tagService.editTag(tag);
		}
		fillModel(model, 0);
		return "addUpdateTags";
	}

	@RequestMapping(value = "/addTag", method = RequestMethod.POST)
	public String addTag(HttpServletRequest request, ModelMap model, Tag tag) {
		tagService.addTag(tag);
		fillModel(model, 0);
		return "addUpdateTags";
	}

	@RequestMapping(value = "/deleteTag", method = RequestMethod.POST)
	public String deleteTag(ModelMap model, Tag tag) {
		tagService.deleteTag(tag.getId());
		fillModel(model, 0);
		return "addUpdateTags";
	}

	private void fillModel(ModelMap model, int tagId) {
		Tag tag = new Tag();
		List<Tag> tags = tagService.getAllTags();
		model.put("activeTagId", tagId);
		model.put("tags", tags);
		model.put("tag", tag);
	}
}
