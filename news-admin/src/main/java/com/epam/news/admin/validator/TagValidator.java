package com.epam.news.admin.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.epam.newsmanagement.model.Tag;

@Component
public class TagValidator implements Validator {
	
	private static final int MAX_TAG_NAME_LENGTH = 30;

	@Override
	public boolean supports(Class<?> clazz) {
		return Tag.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
        Tag tag = (Tag) target;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "label.validate.fieldIsEmply");
        String name = tag.getName();
        if ((name.length()) > MAX_TAG_NAME_LENGTH) {
            errors.rejectValue("commentText", "label.validate.nameLarge");
        }
	}

}
