package com.epam.news.admin.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.epam.newsmanagement.model.Author;
import com.epam.newsmanagement.model.Comment;
import com.epam.newsmanagement.model.Tag;
import com.epam.newsmanagement.model.helper.DeleteGroupHelper;
import com.epam.newsmanagement.model.helper.NewsVO;
import com.epam.newsmanagement.model.helper.NewsFormHelper;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.NewsManagementService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;

@Controller
public class NewsController {

	@Autowired
	NewsService newsService;
	@Autowired
	AuthorService authorService;
	@Autowired
	TagService tagService;
	@Autowired
	NewsManagementService newsManagementService;
	@Autowired
	private ApplicationContext context;

	@RequestMapping(value = "/news/{newsId}", method = RequestMethod.GET)
	public String getConcreteNews(ModelMap model, HttpServletRequest request,
			@PathVariable("newsId") int newsId) {
		NewsVO newsVO = newsManagementService.getConcreteNews(newsId);
		Comment comment = new Comment();
		model.put("newsVO", newsVO);
		model.put("comment", comment);
		return "news";
	}

	@RequestMapping(value = "/deleteNews/{pageIndex}", method = RequestMethod.POST)
	public String deleteNews(ModelMap model, DeleteGroupHelper form,
			@PathVariable("pageIndex") int pageIndex) {
		for (int i = 0; i < form.getDeletedID().length; i++) {
			newsService.deleteNews(form.getDeletedID()[i]);
		}
		return "redirect:/newsList/" + pageIndex;
	}

	@RequestMapping(value = "/createNews", method = RequestMethod.POST)
	public String addNews(ModelMap model, NewsFormHelper form)
			throws ParseException {
		long newsId;
		String dateformat = context.getMessage("locale.dateFormat", null,
				LocaleContextHolder.getLocale());
		SimpleDateFormat format = new SimpleDateFormat(dateformat);
		Date date;
		date = format.parse(form.getDateForForm());
		form.getNews().setCreationDate(date);
		form.getNews().setModificationDate(date);

		newsId = newsManagementService.addFullNewsMessage(form);

		return "redirect:/news/" + newsId;
	}

	@RequestMapping(value = "/updateNews", method = RequestMethod.POST)
	public String editNews(ModelMap model, NewsFormHelper form)
			throws ParseException {
		//long newsId = form.getNews().getId();
		String dateformat = context.getMessage("locale.dateFormat", null,
				LocaleContextHolder.getLocale());
		SimpleDateFormat format = new SimpleDateFormat(dateformat);
		Date date;
		date = format.parse(form.getDateForForm());
		form.getNews().setCreationDate(date);
		form.getNews().setModificationDate(date);
		newsService.editNews(form.getNews());
		Author author = new Author();
		author.setId(form.getAuthorID());
		authorService.addNewsAuthor(form.getNews(), author);
		tagService.addNewsTags(form.getNews(), form.getTagsID());
		
		return "redirect:/newsList/1";
	}

	@RequestMapping(value = "/editNews/{newsId}", method = RequestMethod.GET)
	public String editConcreteNews(ModelMap model,
			@PathVariable("newsId") int newsId) {
		NewsFormHelper form = new NewsFormHelper();
		form.setNews(newsService.getNews(newsId));
		form.setTags(tagService.getAllTags());
		List<Author> authors = authorService.getAllAuthors();
		List<Tag> tagsOfCurrnetNews = tagService.getTagsByNewsId(newsId);
		Author authorOfCurrentNews = authorService.getAuthorByNewsId(newsId);
		fillModel(authorOfCurrentNews, form, tagsOfCurrnetNews, authors, model);
		return "editNews";
	}

	@RequestMapping(value = "/next/{newsId}", method = RequestMethod.GET)
	public String next(ModelMap model,
			@PathVariable("newsId") int newsId,
			HttpServletRequest request) {
		NewsVO newsVO;
		if (newsManagementService.next(newsId) == null) {
			newsVO = newsManagementService.getConcreteNews(newsId);
			Comment comment = new Comment();
			model.put("newsVO", newsVO);
			model.put("comment", comment);
			return "news";
		} else {
			newsVO = newsManagementService.next(newsId);
			Comment comment = new Comment();
			model.put("newsVO", newsVO);
			model.put("comment", comment);
			return "news";
		}
	}

	@RequestMapping(value = "/prev/{newsId}", method = RequestMethod.GET)
	public String prev(ModelMap model,
			@PathVariable("newsId") int newsId,
			HttpServletRequest request) {
		NewsVO newsVO;
		if (newsManagementService.prev(newsId) == null) {
			newsVO = newsManagementService.getConcreteNews(newsId);
			Comment comment = new Comment();
			model.put("newsVO", newsVO);
			model.put("comment", comment);
			return "news";
		} else {
			newsVO = newsManagementService.prev(newsId);
			Comment comment = new Comment();
			model.put("newsVO", newsVO);
			model.put("comment", comment);
			return "news";
		}
	}

	private void fillModel(Author authorOfCurrentNews, NewsFormHelper form,
			List<Tag> tags, List<Author> authors, ModelMap model) {
		model.put("form", form);
		model.put("authorList", authors);
		model.put("newsAuthor", authorOfCurrentNews);
		model.put("newsTags", tags);
	}

}
