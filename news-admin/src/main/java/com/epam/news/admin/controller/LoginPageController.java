package com.epam.news.admin.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.newsmanagement.model.helper.SearchCriteria;

@Controller
public class LoginPageController {
	@Autowired
	private ApplicationContext context;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String startPage(ModelAndView model, HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		if (session == null) {
			request.getSession().setAttribute("filter", new SearchCriteria());
			return "loginPage";
		} else {
			return "redirect:/newsList/1";
		}
	}

	@RequestMapping(value = "/loginPage", method = RequestMethod.GET)
	public ModelAndView login(
			@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout,
			HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		if (error != null) {
			model.addObject("error", context.getMessage("content.invalid",
					null, LocaleContextHolder.getLocale()));
		}
		if (error == null) {
			request.getSession().setAttribute("filter", new SearchCriteria());
		}
		if (logout != null) {
			model.addObject("msg", context.getMessage("content.success", null,
					LocaleContextHolder.getLocale()));
		}
		model.setViewName("loginPage");
		return model;
	}
}
