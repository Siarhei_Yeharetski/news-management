package com.epam.news.admin.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.epam.newsmanagement.model.Author;
import com.epam.newsmanagement.model.Tag;
import com.epam.newsmanagement.model.helper.NewsFormHelper;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.TagService;

@Controller
public class MenuController {
	
	@Autowired
	private AuthorService authorService;
	@Autowired
	private TagService tagService;

	@RequestMapping(value = "/addNews", method = RequestMethod.GET)
	public String addNews(ModelMap model) {
		NewsFormHelper form = new NewsFormHelper();
		List<Author> authors = authorService.getAllNotExpiredAuthors();
		form.getNews().setCreationDate(new Date(System.currentTimeMillis()));
		form.setTags(tagService.getAllTags());
		model.put("form", form);
		model.put("authorList", authors);
		return "addNews";
	}
	
	@RequestMapping(value = "/addUpdateAuthors", method = RequestMethod.GET)
	public String addUpdateAuthors(ModelMap model) {
		List<Author> authors;
		Author author = new Author();
		authors = authorService.getAllAuthors();
		model.put("activeAuthorId", 0);
		model.put("authors", authors);
		model.put("author", author);
		return "addUpdateAuthors";
	}
	
	@RequestMapping(value = "/addUpdateTags", method = RequestMethod.GET)
	public String addUpdateTags(ModelMap model) {
		List<Tag> tags;
		Tag tag = new Tag();
		tags = tagService.getAllTags();
		model.put("activeTagId", 0);
		model.put("tags", tags);
		model.put("tag", tag);
		return "addUpdateTags";
	}
}
