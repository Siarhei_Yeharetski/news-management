package com.epam.news.admin.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.epam.newsmanagement.model.Author;

@Component
public class AuthorValidator implements Validator {
	
	private static final int MAX_AUTHOR_NAME_LENGTH = 30;

	@Override
	public boolean supports(Class<?> clazz) {
		return Author.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
        Author author = (Author) target;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "label.validate.fieldIsEmply");
        String name = author.getName();
        if ((name.length()) > MAX_AUTHOR_NAME_LENGTH) {
            errors.rejectValue("commentText", "label.validate.nameLarge");
        }
	}

}
