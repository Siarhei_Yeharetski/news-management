package com.epam.news.admin.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.epam.news.admin.validator.CommentValidator;
import com.epam.newsmanagement.model.Comment;
import com.epam.newsmanagement.model.News;
import com.epam.newsmanagement.model.helper.NewsVO;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.NewsManagementService;

@Controller
public class CommentController {

	@Autowired
	private CommentValidator validator;
	@Autowired
	private NewsManagementService newsManagementService;
	@Autowired
	private CommentService commentService;

	@RequestMapping(value = "/news/sendComment", method = RequestMethod.POST)
	public String processSendComment(ModelMap model, Comment comment,
			BindingResult result) {

		NewsVO newsVO = newsManagementService.getConcreteNews(comment
				.getNewsId());
		validator.validate(comment, result);
		if (result.hasErrors()) {
			model.put("newsVO", newsVO);
			return "news";
		} else {
			News news = new News();
			news.setId(comment.getNewsId());
			comment.setCreationDate(new Date(System.currentTimeMillis()));
			commentService.addComment(news, comment);
			model.put("newsVO", newsVO);
			return "redirect:/news/" + comment.getNewsId();
		}
	}

	@RequestMapping(value = "/news/deleteComment/{commentId}/{newsId}", method = RequestMethod.GET)
	public String processDeleteComment(ModelMap model,
			HttpServletRequest request,
			@PathVariable("commentId") int commentId,
			@PathVariable("newsId") int newsId) {
		commentService.deleteComment(commentId);
		return "redirect:/news/" + newsId;
	}

}
