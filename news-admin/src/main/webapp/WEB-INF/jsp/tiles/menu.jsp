<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div id="menuContent">
    <ul>
	<li>
            <a href="${pageContext.request.contextPath}/newsList/1">
                <spring:message code="menu.newsList"/>
            </a>
        </li>
	<li>
            <a href="${pageContext.request.contextPath}/addNews">
                <spring:message code="menu.addNews"/>
            </a>
        </li>
        <li>
            <a href="${pageContext.request.contextPath}/addUpdateAuthors">
                <spring:message code="menu.addUpdateAuthors"/>
            </a>
        </li>
	<li>
            <a href="${pageContext.request.contextPath}/addUpdateTags">
                <spring:message code="menu.addUpdateTags"/>
            </a>
        </li>
    </ul>
</div>
