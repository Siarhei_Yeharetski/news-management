<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div id="loginPage" onload='document.loginForm.username.focus();'>
    <div id="login-box">
        <h3><spring:message code="content.cap"/></h3>
        <c:if test="${not empty error}">
                <div class="error">${error}</div>
        </c:if>
        <c:if test="${not empty msg}">
                <div class="msg">${msg}</div>
        </c:if>
        <form name="loginForm" action="<c:url value='/loginPage' />" method="POST">
            <table>
                <tr>
                    <td><spring:message code="content.login"/></td>
                    <td><input type="text" name="login"></td>
                </tr>
                <tr>
                    <td><spring:message code="content.password"/></td>
                    <td><input type="password" name="password" /></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button id="buttonSignIn"><spring:message code="content.signin"/></button>
                    </td>
                </tr>
            </table>
            <input type="hidden" name="${_csrf.parameterName}"value="${_csrf.token}" />
	</form>
    </div>
</div>