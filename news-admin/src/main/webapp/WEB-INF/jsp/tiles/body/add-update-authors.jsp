<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<div class="addUpdateDiv">
	<table>
		<c:forEach var="authorsListItem" items="${authors}">
			<form:form action="${pageContext.request.contextPath}/updateAuthor"
				method="post" commandName="author">
				<tr>
					<td><spring:message code="content.authort" /></td>
					<td><c:choose>
							<c:when test="${activeAuthorId == authorsListItem.id}">
								<form:input path="name" value="${authorsListItem.name}"
									size="40" disabled="false" />
							</c:when>
							<c:otherwise>
								<form:input path="name" value="${authorsListItem.name}"
									size="40" disabled="true" />
							</c:otherwise>
						</c:choose> <form:hidden path="id" value="${authorsListItem.id}" /></td>
					<td><c:choose>
							<c:when test="${activeAuthorId == authorsListItem.id}">
								<input type="submit"
									value="<spring:message code="content.update"/>">
								<input type="submit"
									value="<spring:message code="content.expire"/>"
									onclick="form.action = '${pageContext.request.contextPath}/expireAuthor';">
								<%-- <input type="submit" value="<spring:message code="content.cancel"/>" onclick="history.go(-1);"> --%>
								<input type="submit"
									value="<spring:message code="content.cancel"/>"
									onclick="javascript:location.href='${pageContext.request.contextPath}/addUpdateAuthors'">
							</c:when>
							<c:otherwise>
								<a class="editClassButton"
									href="${pageContext.request.contextPath}/editAuthor/${authorsListItem.id}">
									<spring:message code="content.edit" />
								</a>
							</c:otherwise>
						</c:choose></td>
				</tr>
			</form:form>
		</c:forEach>
	</table>
	<c:if
		test="${fn:contains(requestScope['javax.servlet.forward.servlet_path'], 'pdateAuthor')}">
		<form:form action="addAuthor" method="post" commandName="author">
			<table>
				<tr>
					<td><spring:message code="content.authort" /></td>
					<td><form:input path="name" size="40" /></td>
					<td><input type="submit"
						value="<spring:message code="content.send"/>"></td>
				</tr>
			</table>
		</form:form>
	</c:if>
</div>
